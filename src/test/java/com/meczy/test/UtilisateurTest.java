package com.meczy.test;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.meczy.dao.AgenceRepository;
import com.meczy.dao.CommuneRepository;
import com.meczy.dao.DepartementRepository;
import com.meczy.dao.DossierRepository;
import com.meczy.dao.PaysRepository;
import com.meczy.dao.ProfilsRepository;
import com.meczy.dao.QuartierRepository;
import com.meczy.dao.RegionRepository;
import com.meczy.dao.UtilisateurRepository;
import com.meczy.entites.*;
import com.meczy.service.AgenceService;
import com.meczy.service.ComiteService;
import com.meczy.service.CommuneService;
import com.meczy.service.CompteService;
import com.meczy.service.DepartementService;
import com.meczy.service.DossierIndivService;
import com.meczy.service.PaysService;
import com.meczy.service.PlanificationService;
import com.meczy.service.ProfilsService;
import com.meczy.service.QuartierService;
import com.meczy.service.RegionService;
import com.meczy.service.RendezvousService;

import lombok.val;
import lombok.experimental.PackagePrivate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UtilisateurTest extends Assert {
	
	@Autowired
	private CompteService compteService ;
	@Autowired
	private PaysService paysService;
	@Autowired
	private RegionService regionService;
	@Autowired
	private DepartementService departementService;
	@Autowired
	private CommuneService communeService;
	@Autowired
	private QuartierService quartierService;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private RegionRepository regionRepository;
	@Autowired
	private DepartementRepository departementRepository;
	@Autowired
	private QuartierRepository quartierRepository;
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private AgenceRepository agenceRepository;
	@Autowired
	private AgenceService agenceService;
	
	@Autowired
	private ProfilsRepository profilsRepository;
	@Autowired
	private ProfilsService profilsService;
	@Autowired
	private RendezvousService rendezvousService;
	@Autowired
	private PlanificationService planificationService;
	@Autowired
	private ComiteService comiteService;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private DossierRepository dossierIndivRepository;
    @Autowired
   private DossierIndivService dossierIndivService;

	@Test
	public void contextLoads() {
		
	
		
		  System.out.println("creation Utilisateur"); String idhopital=""+((int)
		  (Math.random()*(9-0))) +((int) (Math.random()*(9-0))) +((int)
		  (Math.random()*(9-0))) +((int) (Math.random()*(9-0))); Integer
		  id=java.lang.Integer.valueOf(idhopital); Pays pays = new Pays();
		  pays.setIdpays(id); pays.setNompays("senegal");
		  paysService.ajouterPays(pays);
		  
		  System.out.println("creation Region");
		  
		  Region region = new Region(); region.setIdregion(id);
		  region.setNomregion("dakar"); 
		  region.setEtat(1);
		  Pays pays2 =
		  paysService.rechercheNompays("senegal");
		  
		  if (pays2!=null) { region.setPays(pays2); regionService.ajouterPays(region);
		  
		  }else { region.setPays(null); regionService.ajouterPays(region); }
		  
		  System.out.println("creation departement"); Departement departement = new
		  Departement(); departement.setIddepartement(id);
		  departement.setNomdepartement("Dakar ouest"); 
		  departement.setRegion(region);
		  Region region2 = regionService.rechercheNomRegion("dakar");
		  
		  
		  if (region2!=null) { departement.setRegion(region2);
		  departementService.ajouterDepartement(departement); }else {
		  departement.setRegion(null);
		  departementService.ajouterDepartement(departement); }
		  
		  System.out.println("creation Commune"); Commune commune = new Commune();
		  commune.setIdcommune(id); commune.setNomcommune("yoff");
		  
		  Departement departement2 =
		  departementService.rechercheNomDepartement("Dakar ouest");
		  
		  if (departement2!=null) { commune.setDepartement(departement2);
		  communeService.ajouterCommune(commune);
		  
		  }else { commune.setDepartement(null); communeService.ajouterCommune(commune);
		  }
		  
		  System.out.println("creation Quartier"); Quartier quartier = new Quartier();
		  quartier.setIdquartier(id); quartier.setNomquartier("yofflayen");
		  quartier.setCommune(commune); Commune commune2 =
		  communeService.rechercheNomCommune("yoff");
		  
		  if (commune2!=null) { quartier.setCommune(commune2);
		  quartierService.ajouterQuartier(quartier);
		  
		  }else { 
			  quartier.setCommune(null); 
			  quartierService.ajouterQuartier(quartier);
		  
		  }
			System.out.println("*************creation Comite***************");
			Comite comite = new Comite();
			comite.setIdcomite(id);
			comite.setNomcomite("comite yoff");
			comite.setNumerocomite(idhopital);
			comite.setTypecomite("Local");
			comiteService.ajouterComite(comite);
			
			Comite comite2 = new Comite();
			comite2.setIdcomite(id);
			comite2.setNomcomite("national");
			comite2.setNumerocomite("id");
			comite2.setTypecomite("National");
			comiteService.ajouterComite(comite2);
			System.out.println("*************ajouter  Comite agence***************");

		  
		  System.out.println("creation Agence"); Agence agence = new Agence();
		  agence.setAdresseagence("dakar yoff"); agence.setCodeagence("65789");
		  agence.setIdagence(id); Quartier quartier3 =
		  quartierRepository.findByNomquartier("yofflayen");
		  
		  
		  if (quartier3!=null) {
			  agence.setQuartier(quartier3); 
			  agence.setComite(null);
		      agenceService.ajouterAgence(agence);
		  
		  }else {
			  agence.setQuartier(null); 
			  agence.setComite(null);
		  agenceService.ajouterAgence(agence);
		  
		  } System.out.println("creation profils"); Profils profils = new Profils();
		  profils.setIdprofils(id); profils.setNomprofils("admin");
		  profilsService.ajouterProfils(profils);
		  
		  System.out.println("creation utilisateur 1"); Utilisateur utilisateur = new
		  Utilisateur(); utilisateur.setIdutilisateur(id); utilisateur.setNom("barry");
		  utilisateur.setPrenom("ousmane"); utilisateur.setPassword("aze");
		  utilisateur.setMatricule("145676H"); utilisateur.setTelephone(763344254);
		  utilisateur.setEmail("ousmanebarry994@gmail.com");
		  utilisateur.setEtat(0); Profils profils2 =
		  profilsRepository.findByNomprofils("admin");
		  utilisateur.setProfils(profils2);
		  Agence agence2 =agenceRepository.findByCodeagence("65789"); 
		  utilisateur.setAgence(agence2);
		  compteService.ajouterUtilisateur(utilisateur);
		  
		  System.out.println("creation utilisateur2"); Utilisateur utilisateur2 = new
		  Utilisateur(); utilisateur2.setIdutilisateur(id);
		  utilisateur2.setNom("Tall"); utilisateur2.setPrenom("Seydou");
		  utilisateur2.setPassword("aze"); utilisateur2.setMatricule("1456760");
		  utilisateur2.setTelephone(763344254);
		  utilisateur2.setEmail("ousmanebarry994@gmail.com");
		  utilisateur2.setEtat(0); utilisateur2.setProfils(profils2);
		  Comite comite4 = comiteService.rechercheComite("national");
		  if(comite4==null)
				System.out.println("--------nulllll comite--------------------------");
		 
		  List<Comite> roles = new ArrayList<Comite>();
			roles.add(comite4);
			utilisateur2.setComites(roles);
		  //utilisateur.setAgence(agence2);
		  compteService.ajouterUtilisateur(utilisateur2);
		 
			System.out.println("*************creation Planification***************");
	  	 	Planification planification = new Planification();
	  	 	planification.setIdplanification(id);
	  	 	planification.setDateplanification(new Date());
	  	 	planification.setAgence(agence2);
	  	 	planification.setMaxdossierplanification(15);
	  	 	planification.setNombredossierplanification(12);
	  	 	planificationService.ajouterPlanification(planification);
			System.out.println("*************creation Rendezvous***************");
	  	 	Rendezvous rendezvous = new Rendezvous();
	  	 	rendezvous.setDescriptionrendezvous("demande credit");
	  	 	rendezvous.setHeurerendezvous("9H30");
	  	 	rendezvous.setNumeromembre("0055");
	  	 	rendezvous.setNumerorendezvous(98777);
	  	 	rendezvous.setPlanification(planification);
	  	 	rendezvousService.ajouterRendezvous(rendezvous);
	  	 	Rendezvous rendezvous2 = new Rendezvous();
	  	 	rendezvous2.setDescriptionrendezvous("demande credit");
	  	 	rendezvous2.setHeurerendezvous("11H30");
	  	 	rendezvous2.setNumeromembre("01055");
	  	 	rendezvous2.setNumerorendezvous(918777);
	  	 	rendezvous2.setPlanification(planification);
	  	 	Utilisateur uerUtilisateur = compteService.lectureByMatricule("145676H");
	  	 	rendezvous2.setUtilisateur(uerUtilisateur);
	  	 	rendezvousService.ajouterRendezvous(rendezvous2);
		/*
		 * Dossierindividuel dossierindividuel = new Dossierindividuel();
		 * dossierindividuel.setIddossier(id);
		 * dossierindividuel.setNatureentrepreneur(1);
		 * dossierindividuel.setAgence(agence2);
		 * dossierindividuel.setNumerodemande(12345);
		 * dossierindividuel.setComptemembre(900);
		 * dossierindividuel.setRaisonsociale("samaSoft");
		 * dossierindividuel.setDatepayementfrais(new Date());
		 * dossierindividuel.setMontant(500);
		 * dossierindividuel.setNatureidentification("CIN");
		 * dossierindividuel.setNumeropiece("1630199400300");
		 * dossierindividuel.setDatenaissance(new Date());
		 * dossierindividuel.setLieudenaissance("thies");
		 * dossierindividuel.setNompere("barry"); dossierindividuel.setNommere("kane");
		 * dossierindividuel.setGenre(0); dossierindividuel.setAdresse("ouest foire ");
		 * dossierindividuel.setAdresseentreprise("sacree coeur");
		 * dossierindividuel.setEnaffaire("10");
		 * dossierindividuel.setTeldomicile(773344243);
		 * dossierindividuel.setSituationfamilial(0);
		 * dossierindividuel.setEnfantencharge(2);
		 * dossierindividuel.setConjoint("ma femme");
		 * dossierindividuel.setMontantdemande(50000000);
		 * dossierindividuel.setMontantaccorde(10000000);
		 * dossierindividuel.setTauxinteret(10); dossierindividuel.setDuree(24);
		 * dossierindividuel.setNombreecheance(6); dossierindividuel.setPeriodicite(2);
		 * dossierindividuel.setDatedeblocage(new Date());
		 * dossierindividuel.setDateecheancefinale(new Date());
		 * dossierindividuel.setButcredit("consommention");
		 * dossierindividuel.setUtilisateur(uerUtilisateur);
		 * //dossierIndivRepository.save(dossierindividuel);
		 * dossierIndivService.ajouterDossierindividuel(dossierindividuel);
		 * 
		 */
		/*
		 * System.out.println("*************creation Comite***************"); Comite
		 * comite = new Comite(); comite.setIdcomite(id);
		 * comite.setNomcomite("comite yoff"); comite.setNumerocomite(idhopital);
		 * comite.setTypecomite("locale"); comiteService.ajouterComite(comite);
		 * 
		 * Comite comite2 = new Comite(); comite2.setIdcomite(id);
		 * comite2.setNomcomite("national"); comite2.setNumerocomite("id");
		 * comite2.setTypecomite("National"); comiteService.ajouterComite(comite2);
		 * System.out.println("*************ajouter  Comite agence***************");
		 * 
		 */
		  System.out.println("*************ajouter  Comite agence***************");
		  
		  Agence agence3 = agenceRepository.findByCodeagence("65789");
		  Comite comite3 = comiteService.rechercheComite("national");
		  agence3.setComite(comite3);
		  agenceService.modificationAgence(agence3.getIdagence(), agence3);
		  
		  System.out.println("*************Fin***************");

		
	}
	
	


	
}
