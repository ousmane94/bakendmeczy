package com.meczy.test;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.meczy.dao.AgenceRepository;
import com.meczy.dao.DossierRepository;
import com.meczy.dao.UtilisateurRepository;
import com.meczy.entites.Agence;
import com.meczy.entites.Dossierindividuel;
import com.meczy.entites.Utilisateur;
import com.meczy.service.CompteService;
import com.meczy.service.DossierIndivService;



@RunWith(SpringRunner.class)
@SpringBootTest
public class DossierTest extends Assert {
	
	@Autowired
	private CompteService compteService ;
	@Autowired
	private UtilisateurRepository utilisateurRepository ;

	@Autowired
	private AgenceRepository agenceRepository;
/*	@Autowired
	private AgenceService agenceService;*/
	@Autowired
	private DossierRepository dossierIndivRepository;
    @Autowired
   private DossierIndivService dossierIndivService;
	
	@Test
	public void contextLoads() {
		
		  System.out.println("creation Dossier"); 
		  String idhopital=""+((int)
		  (Math.random()*(9-0))) +((int) (Math.random()*(9-0))) +((int)
		  (Math.random()*(9-0))) +((int) (Math.random()*(9-0))); Integer
		  id=java.lang.Integer.valueOf(idhopital); 
		  
		  Agence agence = agenceRepository.findByCodeagence("65789");
		  Utilisateur  utilisateur = utilisateurRepository.findByMatricule("145676H");
		  Dossierindividuel dossierindividuel = new Dossierindividuel();
		  dossierindividuel.setIddossier(id);
		  dossierindividuel.setNatureentrepreneur(1);
		  dossierindividuel.setAgence(agence);
		  dossierindividuel.setNumerodemande(123456);
		  dossierindividuel.setComptemembre(900);
		  dossierindividuel.setRaisonsociale("samaSoft");
		  dossierindividuel.setDatepayementfrais(new Date());
		  dossierindividuel.setMontant(500);
		  dossierindividuel.setNatureidentification("CIN");
		  dossierindividuel.setNumeropiece("1630199400300");
		  dossierindividuel.setDatenaissance(new Date());
		  dossierindividuel.setLieudenaissance("thies");
		  dossierindividuel.setNompere("barry");
		  dossierindividuel.setNommere("kane");
		  dossierindividuel.setGenre(0);
		  dossierindividuel.setAdresse("ouest foire ");
		  dossierindividuel.setAdresseentreprise("sacree coeur");
		  dossierindividuel.setEnaffaire("10");
		  dossierindividuel.setTeldomicile(773344243);
		  dossierindividuel.setSituationfamilial(0);
		  dossierindividuel.setEnfantencharge(2);
		  dossierindividuel.setConjoint("ma femme");
		  dossierindividuel.setMontantdemande(50000000);
		  dossierindividuel.setMontantaccorde(10000000);
		  dossierindividuel.setTauxinteret(10);
		  dossierindividuel.setDuree(24);
		  dossierindividuel.setNombreecheance(6);
		  dossierindividuel.setPeriodicite(2);
		  dossierindividuel.setDatedeblocage(new Date());
		  dossierindividuel.setDateecheancefinale(new Date());
		  dossierindividuel.setButcredit("consommention");
		  dossierindividuel.setUtilisateur(utilisateur);
		  //dossierIndivRepository.save(dossierindividuel);
		  dossierIndivService.ajouterDossierindividuel(dossierindividuel);
		  
		  
		  
		  
		  
		  	
	}
	
}