package com.meczy.test;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.meczy.dao.AgenceRepository;
import com.meczy.entites.Agence;
import com.meczy.entites.Planification;
import com.meczy.entites.Rendezvous;
import com.meczy.service.PlanificationService;
import com.meczy.service.RendezvousService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AgenceTest extends Assert{
	

	@Autowired
	private AgenceRepository agenceRepository;
	@Autowired
	private RendezvousService rendezvousService;
	@Autowired
	private PlanificationService planificationService;
	
	@Test
	public void contextLoads() {
		System.out.println("creation Planification");
		String  idagence=""+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)));
	    Integer id=java.lang.Integer.valueOf(idagence);
  	 	Agence agence2 = agenceRepository.findByCodeagence("65789");
  	 	Planification planification = new Planification();
  	 	planification.setIdplanification(id);
  	 	planification.setDateplanification(new Date());
  	 	planification.setAgence(agence2);
  	 	planification.setMaxdossierplanification(15);
  	 	planification.setNombredossierplanification(12);
  	 	planificationService.ajouterPlanification(planification);
  	 	Rendezvous rendezvous = new Rendezvous();
  	 	rendezvous.setDescriptionrendezvous("demande credit");
  	 	rendezvous.setHeurerendezvous("12h");
  	 	rendezvous.setNumeromembre("0055");
  	 	rendezvous.setNumerorendezvous(98777);
  	 	rendezvous.setPlanification(planification);
  	 	rendezvousService.ajouterRendezvous(rendezvous);
	    
	}

}
