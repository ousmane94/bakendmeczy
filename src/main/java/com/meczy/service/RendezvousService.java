package com.meczy.service;

import java.util.List;

import com.meczy.entites.Rendezvous;

public interface RendezvousService {
	
	
	public boolean ajouterRendezvous( Rendezvous  rendezvous);
	public Rendezvous findByNumerorendezvous(long numerorendezvous);
	public Rendezvous modificationRendezvous(int idrendezvous ,Rendezvous rendezvous);
	public boolean  supprimerRendezvous(int idrendezvous);
    public List<Rendezvous> listedesRendezvous();
    public List<Rendezvous> listeRendezvousAgence(String codeagence);
    public List<Rendezvous> listeRendezvousAgentCreditindividuel(String matricule);
    
    public List<Rendezvous> listeRendezvousAgentCreditgroupement(String matricule);

    public List<Rendezvous> listeRendezvousparPlanification(int idplanification);
    public List<Rendezvous> listeRendezvousdate(String daterendezvous , String codeagence);



   

}
