package com.meczy.service;

import java.util.List;

import com.meczy.entites. Quartier;

public interface QuartierService {
	
	public boolean ajouterQuartier( Quartier  quartier);
	public  Quartier rechercheNomQuartier(String nomquartier);
	public  Quartier modifierquartier(int idquartier , Quartier  quartier);
	public boolean supprimerquartier(int idquartier);
    public List< Quartier> listedesquartier();
    public List< Quartier> listedesquartierActive();


}
