package com.meczy.service;

import java.util.List;

import com.meczy.entites.Comite;

public interface ComiteService {
	
	
	public boolean ajouterComite(Comite comite);
	public Comite rechercheComite(String nomcomite);
	public Comite modifierComite(int idcomite ,Comite comite);
	public boolean supprimerComite(int idcomite);
    public List<Comite> listedesComites();
    public List<Comite> listedesComitesActive();



}
