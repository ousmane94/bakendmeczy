package com.meczy.service;

import java.util.List;

import com.meczy.entites.Commune;

public interface CommuneService {
	
	public boolean ajouterCommune(Commune commune);
	public Commune rechercheNomCommune(String nomcommune);
	public Commune modifierCommune(int idcommune ,Commune commune);
	public boolean supprimerCommune(int idcommune);
    public List<Commune> listedesCommunes();
    public List<Commune> listedesCommunesActive();



}
