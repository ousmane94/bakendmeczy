package com.meczy.service;

import java.util.List;

import com.meczy.entites.Profils;

public interface ProfilsService {
	
	public boolean ajouterProfils(Profils profils);
	public Profils rechercheProfils(String nomrofils);
	public Profils modifierprofils(int idprofils ,Profils profils);
	public boolean supprimerComite(int idcomite);
    public List<Profils> listedeProfils();

}
