package com.meczy.service;

import java.util.List;

import com.meczy.entites.Departement;

public interface DepartementService {
	
	public boolean ajouterDepartement(Departement departement);
	public Departement rechercheNomDepartement(String nomdepartement);
	public Departement modifierDepartement(int iddepartement ,Departement departement);
	public boolean supprimerDepartement(int iddepartement);
    public List<Departement> listedesDepartements();
    public List<Departement> listedesDepartementsActive();


}
