package com.meczy.service;

import java.util.List;

import com.meczy.entites.Activitecredit;

public interface ActivitecreditService {
	

	public boolean ajouterActivitecredit(Activitecredit activitecredit);
	public Activitecredit rechercheActivitecredit(String descriptionactivitecredit);
	public Activitecredit modifierActivitecredit(int idactivitecredit ,Activitecredit activitecredit);
	public boolean supprimerActivitecredit(int idactivitecredit);
    public List<Activitecredit> listedesActivitecredits();
	public List<Activitecredit> listedesActivitecreditcredit();




}
