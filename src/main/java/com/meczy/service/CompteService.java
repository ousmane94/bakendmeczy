 package com.meczy.service;

import java.util.List;

import com.meczy.entites.Utilisateur;

public interface CompteService {
	
	public boolean ajouterUtilisateur(com.meczy.entites.Utilisateur utilisateur);
	public Utilisateur lectureByMatricule(String matricule);
	public boolean activerutilisteur(String matricule);
	public Utilisateur modificationUtilisateur(int idutilisateur ,Utilisateur utilisateur);
	public boolean supprimerUtilisateur(int idutilisateur);
    public List<Utilisateur> listedesUtilisateurs();
    public List<Utilisateur> listeUtilisateursAgence(String codeagence);
	public Utilisateur connexion(String matricule,String password);
	public boolean ajouterdansComite(String matricule,int idcomite);
    public List<Utilisateur> listeAgentCreditAgence(String codeagence);
    public List<Utilisateur> listeSuperviseurAgence(String codeagence);
	public Utilisateur monUtilisateur(String matricule);
    public List<Utilisateur> listeAgentCredit();






   
}
