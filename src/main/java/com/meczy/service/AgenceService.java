package com.meczy.service;

import java.util.List;

import com.meczy.entites.Agence;

public interface AgenceService {
	
	
	public boolean ajouterAgence(Agence agence);
	public Agence findByCodeAgence(String codeagence);
	public Agence modificationAgence(int idagence ,Agence agence);
	public boolean  supprimerAgence(int idagence);
    public List<Agence> listedesAgences();
    public List<Agence> listedesAgencesActive();

   

}
