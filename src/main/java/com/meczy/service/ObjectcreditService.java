package com.meczy.service;
import java.util.List;
import com.meczy.entites.Objectcredit;

public interface ObjectcreditService {
	
	  public boolean ajouterObjectcredit(Objectcredit objectcredit);
		public Objectcredit rechercheObjectcredit(String nomcomite);
		public Objectcredit modifierObjectcredit(int idobjectcredit ,Objectcredit objectcredit);
		public boolean supprimerObjectcredit(int idobjectcredit);
	    public List<Objectcredit> listedesObjectcredit();
		public List<Objectcredit> listedesCommunesActive();

}
