package com.meczy.service;

import java.util.List;

import com.meczy.entites.Region;

public interface RegionService {
	public boolean ajouterPays(Region region);
	public Region rechercheNomRegion(String nomregion);
	public Region modifierRegion(int idregion ,Region region);
	public boolean supprimerRegion(int idregion);
    public List<Region> listedesRegions();
    public List<Region> listedesRegionsActive();

}
