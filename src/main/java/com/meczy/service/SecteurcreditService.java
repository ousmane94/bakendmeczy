package com.meczy.service;

import java.util.List;

import com.meczy.entites.Secteurcredit;

public interface SecteurcreditService {
	
	public boolean ajouterSecteurcredit(Secteurcredit secteurcredit);
	public Secteurcredit rechercheSecteurcredit(String descriptionsecteur);
	public Secteurcredit modifierSecteurcredit(int idsecteurcredit ,Secteurcredit secteurcredit);
	public boolean  ajouterdansObjectcredit(String description, int idsecteurcredit) ;
    public List<Secteurcredit> listedesSecteurcredit();
	public List<Secteurcredit> listedessecteurActive();
    
    
  


}
