package com.meczy.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.meczy.entites.Dossierindividuel;


public interface DossierIndivService {
	
	public boolean ajouterDossierindividuel(Dossierindividuel dossierindividuel);
	public Dossierindividuel lectureparNumeroDossier(long numerodossier);
	public Dossierindividuel lectureparNumeroDossierAndagence(long numerodossier ,String codeagence);
	public List<Dossierindividuel> lectureparNumeroDossierAndutilisateur(long numerodossier ,String matricule);
    public List<Dossierindividuel> listDossierindividuelparAgent(String matricule);
    public List<Dossierindividuel> listDossierindividuel();
	public Dossierindividuel updeateDossierindividuel(int iddossierindividuel ,Dossierindividuel dossierindividuel);
	public boolean deleteDossierindividuel(int iddossierindividuel);
    public Page<Dossierindividuel> findPaginated(Pageable pageable);

}
