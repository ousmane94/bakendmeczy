package com.meczy.service;

import java.util.Date;
import java.util.List;

import com.meczy.entites.Planification;

public interface PlanificationService {
	
	public boolean ajouterPlanification(Planification planification);
	public Planification recherchedatePlanification(String dateplanification,String codeagence);
	public  List<Planification> recherchedatePlaning(String dateplanification);
	public Planification modifierplanification(int idplanification ,Planification planification);
	public boolean supprimerPlanification(int idPlanification);
    public List<Planification> listedesPlanifications();
    public List<Planification> listedesPlanificationsActive();
    public List<Planification> listedesPlanificationAgence(String codeagence);
    public List<Planification> listedesPlanificationAgenceActive(String codeagence);



}
