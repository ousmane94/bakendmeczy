package com.meczy.service;

import java.util.List;
import com.meczy.entites.Sousactivite;

public interface SousactiviteService {
	
	public boolean ajouterActivitecredit(Sousactivite sousactivite);
	public Sousactivite rechercheActivitecredit(String descriptionactivitecredit);
	public Sousactivite modifierActivitecredit(int idactivitecredit ,Sousactivite sousactivite);
	public boolean supprimerActivitecredit(int idactivitecredit);
    public List<Sousactivite> listedesActivitecredits();
	public List<Sousactivite> listedesActivitecreditcredit();

}
