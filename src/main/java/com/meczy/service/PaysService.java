package com.meczy.service;

import java.util.List;

import com.meczy.entites.Pays;

public interface PaysService {
	
	public boolean ajouterPays(Pays pays);
	public Pays rechercheNompays(String nomPays);
	public Pays rechercheId(int idPays );
	public Pays modifierPays(int idPays ,Pays pays);
	public boolean supprimerPays(int idpays);
    public List<Pays> listedesPays();
    public List<Pays> listedesPaysActive();



}
