package com.meczy.securite;

import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.UtilisateurRepository;
import com.meczy.entites.Utilisateur;
import com.meczy.service.CompteService;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	CompteService accountService;
	
	 @Autowired
	    private UtilisateurRepository usersRepository;

	
	
	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(String matricule) throws UsernameNotFoundException {
		  try {
		Utilisateur user=usersRepository.findByMatricule(matricule);
		if(user==null) throw new UsernameNotFoundException("invalide utilisateur");
		Collection<GrantedAuthority> authorities=new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority(user.getProfils().getNomprofils()));
		return new User(user.getMatricule(),user.getPassword(),authorities);
	 } catch (NoSuchElementException e) {
         throw new UsernameNotFoundException("User " + matricule + " not found.", e);
     }
		
	}

}
