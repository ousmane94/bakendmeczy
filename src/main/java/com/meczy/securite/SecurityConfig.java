package com.meczy.securite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	

	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		//http.formLogin();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.authorizeRequests().antMatchers("/login/**","/api/photoProduct/**").permitAll();
		http.authorizeRequests().antMatchers(HttpMethod.GET,"/user/**").permitAll();
		http.authorizeRequests().antMatchers("/conte/**","/mesures/**","/constantes/**","/consultation/**","/transfert/**","/traitement/**").hasAnyAuthority("administrateur","sao","admission","chefurgence","biologiste","samu");
		http.authorizeRequests().antMatchers(HttpMethod.PUT,"/hopitals/**").hasAuthority("administrateur");
		http.authorizeRequests().antMatchers(HttpMethod.GET,"/hopitals/**","/patients","/lits/**").hasAnyAuthority("administrateur","sao","chefurgence");
		http.authorizeRequests().antMatchers(HttpMethod.POST,"/hopitals/**","/patients","/lits/**","/constantes/**").hasAnyAuthority("administrateur","sao","admission","chefurgence");
		http.authorizeRequests().antMatchers(HttpMethod.PUT,"/hopitals/**","/patients","/lits/**","/user/**","/traitement/**").hasAnyAuthority("administrateur","sao","admission","chefurgence","biologiste");
		http.authorizeRequests().antMatchers(HttpMethod.GET,"/hopitals/**","/patients/**","/services/**","/mesures/**").hasAnyAuthority("administrateur","sao","admission","chefurgence");
		//http.authorizeRequests().antMatchers(HttpMethod.GET,"/hopitals/**","/patients").hasAuthority("admin");medecin
		http.authorizeRequests().antMatchers("/user/**","/services/**","/hopitals/**","/patients/**").hasAuthority("administrateur");
		http.authorizeRequests().anyRequest().authenticated().and();
		http.addFilter(new JWTAuthenticationfilter(authenticationManager()));
		http.addFilterBefore(new JWTAuthorizationfilter(), UsernamePasswordAuthenticationFilter.class);

		
	}
	
}
