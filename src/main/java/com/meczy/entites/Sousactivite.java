package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;


@Table(name = "sousactivite", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Sousactivite implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private int idsousactivitecredit;
	private String descriptionactivitecredit;
	private int etat;


	// dossiers
	@OneToMany(mappedBy = "sousactivite" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Dossier> dossiers;

	//pays
	@ManyToOne(fetch = FetchType.LAZY , optional = false )
	@JoinColumn(name = "activitecredit" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Activitecredit activitecredit;


	@JsonCreator
	public Sousactivite(
			@JsonProperty("idsousactivitecredit") int idsousactivitecredit, 
			@JsonProperty("descriptionactivitecredit") String descriptionactivitecredit, 
			@JsonProperty("activitecredit") Activitecredit activitecredit, 
			@JsonProperty("etat") int etat)

	{
		this.idsousactivitecredit = idsousactivitecredit;
		this.descriptionactivitecredit = descriptionactivitecredit;
		this.activitecredit = activitecredit;
		this.etat = etat;

	}


	public Sousactivite() {
		super();
		// TODO Auto-generated constructor stub
	}


	


	public int getIdsousactivitecredit() {
		return idsousactivitecredit;
	}


	public void setIdsousactivitecredit(int idsousactivitecredit) {
		this.idsousactivitecredit = idsousactivitecredit;
	}


	public String getDescriptionactivitecredit() {
		return descriptionactivitecredit;
	}


	public void setDescriptionactivitecredit(String descriptionactivitecredit) {
		this.descriptionactivitecredit = descriptionactivitecredit;
	}


	public int getEtat() {
		return etat;
	}


	public void setEtat(int etat) {
		this.etat = etat;
	}


	public List<Dossier> getDossiers() {
		return dossiers;
	}


	public void setDossiers(List<Dossier> dossiers) {
		this.dossiers = dossiers;
	}


	public Activitecredit getActivitecredit() {
		return activitecredit;
	}


	public void setActivitecredit(Activitecredit activitecredit) {
		this.activitecredit = activitecredit;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
