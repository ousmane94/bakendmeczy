package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Table(name = "objectcredit", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Objectcredit implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private int idobjectcredit;
	private String description;
	private int etat;

	//utiliateur
	/*@OneToMany(mappedBy = "objectcredit" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Secteurcredit> secteurcredits;*/

	@ManyToMany(targetEntity = Secteurcredit.class, mappedBy = "objectcredits", cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Secteurcredit> secteurcredits;
	
	// dossiers
	@OneToMany(mappedBy = "objectcredit" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Dossier> dossiers;
	

	@JsonCreator
	public Objectcredit(
			@JsonProperty("idobjectcredit")int idobjectcredit, 
			@JsonProperty("description") String description, 
			@JsonProperty("etat") int etat)
			
     {
		this.idobjectcredit = idobjectcredit;
		this.description = description;
		this.etat = etat;

	}
	public Objectcredit() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Objectcredit(int idobjectcredit, String description, int etat, List<Secteurcredit> secteurcredits) {
		super();
		this.idobjectcredit = idobjectcredit;
		this.description = description;
		this.etat = etat;
		this.secteurcredits = secteurcredits;
	}
	
	
	public List<Dossier> getDossiers() {
		return dossiers;
	}
	public void setDossiers(List<Dossier> dossiers) {
		this.dossiers = dossiers;
	}
	public int getIdobjectcredit() {
		return idobjectcredit;
	}
	public void setIdobjectcredit(int idobjectcredit) {
		this.idobjectcredit = idobjectcredit;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getEtat() {
		return etat;
	}
	public void setEtat(int etat) {
		this.etat = etat;
	}
	public List<Secteurcredit> getSecteurcredits() {
		return secteurcredits;
	}
	public void setSecteurcredits(List<Secteurcredit> secteurcredits) {
		this.secteurcredits = secteurcredits;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	


}
