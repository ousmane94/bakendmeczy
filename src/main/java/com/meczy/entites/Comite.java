package com.meczy.entites;

import java.io.Serializable;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Table(name = "comite", catalog = "MeczyDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Comite implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private int idcomite;
	private String typecomite;
	private String numerocomite;
	private String nomcomite;
	private int etat;


	//guichet
	@OneToMany( mappedBy = "comite" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Agence> agences;
	
	@ManyToMany(targetEntity = Utilisateur.class, mappedBy = "comites", cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Utilisateur> utilisateurs;
	

	
	@JsonCreator
	public Comite(
			@JsonProperty("idcomite")int idcomite, 
			@JsonProperty("typecomite") String typecomite,
			@JsonProperty("nomcomite") String nomcomite,
			@JsonProperty("numerocomite") String numerocomite, 
			@JsonProperty("agences") List<Agence> agences, 
			@JsonProperty("etat")int etat, 

	        @JsonProperty("utilisateurs") List<Utilisateur> utilisateurs)
			
     {
		this.idcomite = idcomite;
		this.typecomite = typecomite;
		this.nomcomite=nomcomite;
		this.numerocomite = numerocomite;
		this.utilisateurs=utilisateurs;
		this.etat=etat;
		this.agences =agences;

	}
	
	public Comite() {}

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getIdcomite() {
		return idcomite;
	}

	public void setIdcomite(int idcomite) {
		this.idcomite = idcomite;
	}

	public String getTypecomite() {
		return typecomite;
	}

	public void setTypecomite(String typecomite) {
		this.typecomite = typecomite;
	}

	public String getNumerocomite() {
		return numerocomite;
	}

	public void setNumerocomite(String numerocomite) {
		this.numerocomite = numerocomite;
	}

	public List<Agence> getAgences() {
		return agences;
	}

	public void setAgences(List<Agence> agences) {
		this.agences = agences;
	}

	public List<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}

	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	public String getNomcomite() {
		return nomcomite;
	}

	public void setNomcomite(String nomcomite) {
		this.nomcomite = nomcomite;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agences == null) ? 0 : agences.hashCode());
		result = prime * result + etat;
		result = prime * result + idcomite;
		result = prime * result + ((nomcomite == null) ? 0 : nomcomite.hashCode());
		result = prime * result + ((numerocomite == null) ? 0 : numerocomite.hashCode());
		result = prime * result + ((typecomite == null) ? 0 : typecomite.hashCode());
		result = prime * result + ((utilisateurs == null) ? 0 : utilisateurs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comite other = (Comite) obj;
		if (agences == null) {
			if (other.agences != null)
				return false;
		} else if (!agences.equals(other.agences))
			return false;
		if (etat != other.etat)
			return false;
		if (idcomite != other.idcomite)
			return false;
		if (nomcomite == null) {
			if (other.nomcomite != null)
				return false;
		} else if (!nomcomite.equals(other.nomcomite))
			return false;
		if (numerocomite == null) {
			if (other.numerocomite != null)
				return false;
		} else if (!numerocomite.equals(other.numerocomite))
			return false;
		if (typecomite == null) {
			if (other.typecomite != null)
				return false;
		} else if (!typecomite.equals(other.typecomite))
			return false;
		if (utilisateurs == null) {
			if (other.utilisateurs != null)
				return false;
		} else if (!utilisateurs.equals(other.utilisateurs))
			return false;
		return true;
	}

	

	

	

}
