package com.meczy.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;


	
	

@Table(name = "bilanpersonnel", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Bilanpersonnel implements Serializable {

		private static final long serialVersionUID = 1L;
		@Id
		private int idbilanpersonnel;
		private Date databilan ; 
		private long epargne ; 
		private long argentenmain ; 
		private long meubles ; 
		private long appareils ; 
		private long bijoux ; 
		private long maison ; 
		private long vehicule; 
		private String autreactif ; 
		private long montantautreactif ; 
		private long emprunt ;
		private long totalactif ;
		private long totalpassif ;
		private String autrepassif ;
		private long montantautrpassif ;
		// revenu mensuel
		@ManyToOne(fetch = FetchType.LAZY , optional = true )
		@JoinColumn(name = "iddossier" , nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		private Dossierindividuel dossierindividuel;
		// justificatif
		@OneToMany(mappedBy = "bilanpersonnel" ,fetch=FetchType.LAZY)
		@JsonIgnore
		private List<Justificatifbilan> justificatifbilans;
		
		@JsonCreator
		public Bilanpersonnel(
			
						@JsonProperty("idbilanpersonnel") int idbilanpersonnel,
						@JsonProperty("databilan") Date  databilan ,
						@JsonProperty("epargne") long epargne ,
						@JsonProperty("argentenmain") long argentenmain ,
						@JsonProperty("meubles") long meubles,
				        @JsonProperty("appareils") long appareils,
				        @JsonProperty("bijoux") long bijoux,
				        @JsonProperty("maison") long maison,
				        @JsonProperty("vehicule") long vehicule,
				        @JsonProperty("autreactif") String autreactif,
				        @JsonProperty("montantautreactif") long montantautreactif,
				        @JsonProperty("emprunt") long emprunt,
				        @JsonProperty("totalactif") long totalactif,
				        @JsonProperty("totalpassif") long totalpassif,
				        @JsonProperty("autrepassif") String autrepassif,
				        @JsonProperty("dossierindividuel") Dossierindividuel dossierindividuel,
				        @JsonProperty("justificatifbilan") List<Justificatifbilan> justificatifbilans,
				        @JsonProperty("montantautrpassif") long montantautrpassif)

		
		{
			this.idbilanpersonnel = idbilanpersonnel;
			this.databilan = databilan;
			this.epargne = epargne;
			this.argentenmain = argentenmain;
			this.meubles = meubles;
			this.appareils = appareils;
			this.bijoux = bijoux;
			this.maison = maison;
			this.vehicule = vehicule;
			this.autreactif = autreactif;
			this.montantautreactif = montantautreactif;
			this.emprunt = emprunt;
			this.totalactif = totalactif;
			this.totalpassif = totalpassif;
			this.autrepassif = autrepassif;
			this.montantautrpassif = montantautrpassif;
			this.dossierindividuel=dossierindividuel;
			this.justificatifbilans=justificatifbilans;
		}
		
		


		public Bilanpersonnel() {
		
		}
	
		public int getIdbilanpersonnel() {
			return idbilanpersonnel;
		}

		public void setIdbilanpersonnel(int idbilanpersonnel) {
			this.idbilanpersonnel = idbilanpersonnel;
		}

		public Date getDatabilan() {
			return databilan;
		}


		public void setDatabilan(Date databilan) {
			this.databilan = databilan;
		}


		public long getEpargne() {
			return epargne;
		}


		public void setEpargne(long epargne) {
			this.epargne = epargne;
		}


		public long getArgentenmain() {
			return argentenmain;
		}


		public void setArgentenmain(long argentenmain) {
			this.argentenmain = argentenmain;
		}


		public long getMeubles() {
			return meubles;
		}


		public void setMeubles(long meubles) {
			this.meubles = meubles;
		}


		public long getAppareils() {
			return appareils;
		}


		public void setAppareils(long appareils) {
			this.appareils = appareils;
		}


		public long getBijoux() {
			return bijoux;
		}


		public void setBijoux(long bijoux) {
			this.bijoux = bijoux;
		}


		public long getMaison() {
			return maison;
		}


		public void setMaison(long maison) {
			this.maison = maison;
		}


		public long getVehicule() {
			return vehicule;
		}


		public void setVehicule(long vehicule) {
			this.vehicule = vehicule;
		}


		public String getAutreactif() {
			return autreactif;
		}


		public void setAutreactif(String autreactif) {
			this.autreactif = autreactif;
		}


		public long getMontantautreactif() {
			return montantautreactif;
		}


		public void setMontantautreactif(long montantautreactif) {
			this.montantautreactif = montantautreactif;
		}


		public long getEmprunt() {
			return emprunt;
		}


		public void setEmprunt(long emprunt) {
			this.emprunt = emprunt;
		}


		public long getTotalactif() {
			return totalactif;
		}


		public void setTotalactif(long totalactif) {
			this.totalactif = totalactif;
		}


		public long getTotalpassif() {
			return totalpassif;
		}


		public void setTotalpassif(long totalpassif) {
			this.totalpassif = totalpassif;
		}


		public String getAutrepassif() {
			return autrepassif;
		}


		public void setAutrepassif(String autrepassif) {
			this.autrepassif = autrepassif;
		}


		public long getMontantautrpassif() {
			return montantautrpassif;
		}


		public void setMontantautrpassif(long montantautrpassif) {
			this.montantautrpassif = montantautrpassif;
		}


		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		

		




		

}
