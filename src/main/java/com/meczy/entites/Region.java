package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "region", catalog = "MeczyDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Region implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private int idregion;
	private String nomregion;
	private int etat;

	//pays
	@ManyToOne(fetch = FetchType.LAZY , optional = false )
	@JoinColumn(name = "idpays" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    //@JsonManagedReference
	//@JsonBackReference
	private Pays pays;
	
	//region
	@OneToMany( mappedBy = "region" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Departement> departements;
	
	
	

	public Region() {
	}
	
	@JsonCreator
	public Region(
			@JsonProperty("idregion")int idregion, 
			@JsonProperty("nomregion") String nomregion,
			@JsonProperty("etat") int etat,
	        @JsonProperty("departement") List<Departement> departements)
			
     {
		this.idregion = idregion;
		this.nomregion = nomregion;
		this.etat = etat;
		this.departements = departements;

	}
	
	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public int getIdregion() {
		return idregion;
	}

	public void setIdregion(int idregion) {
		this.idregion = idregion;
	}

	public String getNomregion() {
		return nomregion;
	}

	public void setNomregion(String nomregion) {
		this.nomregion = nomregion;
	}

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}

	public List<Departement> getDepartements() {
		return departements;
	}

	public void setDepartements(List<Departement> departements) {
		this.departements = departements;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((departements == null) ? 0 : departements.hashCode());
		result = prime * result + etat;
		result = prime * result + idregion;
		result = prime * result + ((nomregion == null) ? 0 : nomregion.hashCode());
		result = prime * result + ((pays == null) ? 0 : pays.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Region other = (Region) obj;
		if (departements == null) {
			if (other.departements != null)
				return false;
		} else if (!departements.equals(other.departements))
			return false;
		if (etat != other.etat)
			return false;
		if (idregion != other.idregion)
			return false;
		if (nomregion == null) {
			if (other.nomregion != null)
				return false;
		} else if (!nomregion.equals(other.nomregion))
			return false;
		if (pays == null) {
			if (other.pays != null)
				return false;
		} else if (!pays.equals(other.pays))
			return false;
		return true;
	}



	
	
}
	
	