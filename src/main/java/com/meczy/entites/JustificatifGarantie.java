package com.meczy.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;


@Table(name = "justificatifGarantie", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class JustificatifGarantie  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private int idJustificatif;
    private String descriptionJustificatif;
    private String Justificatif;
	// garantie
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idgarantie" , nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Garantie garantie;
	
	
	public JustificatifGarantie() {
		
	}

	
	@JsonCreator
	public JustificatifGarantie(
			@JsonProperty("idJustificatif") int idJustificatif,
			@JsonProperty("descriptionJustificatif") String descriptionJustificatif ,
			@JsonProperty("justificatif") String justificatif ,
			@JsonProperty("garantie") Garantie garantie)
	
	{                                                                                                                                                                                                                                                                                                                                                         
		this.idJustificatif = idJustificatif;
		this.descriptionJustificatif = descriptionJustificatif;
		this.Justificatif =justificatif;
		this.garantie = garantie;
		
	}
	
	public int getIdJustificatif() {
		return idJustificatif;
	}
	public void setIdJustificatif(int idJustificatif) {
		this.idJustificatif = idJustificatif;
	}
	public String getDescriptionJustificatif() {
		return descriptionJustificatif;
	}
	public void setDescriptionJustificatif(String descriptionJustificatif) {
		this.descriptionJustificatif = descriptionJustificatif;
	}
	public String getJustificatif() {
		return Justificatif;
	}
	public void setJustificatif(String justificatif) {
		Justificatif = justificatif;
	}
	public Garantie getGarantie() {
		return garantie;
	}
	public void setGarantie(Garantie garantie) {
		this.garantie = garantie;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Justificatif == null) ? 0 : Justificatif.hashCode());
		result = prime * result + ((descriptionJustificatif == null) ? 0 : descriptionJustificatif.hashCode());
		result = prime * result + ((garantie == null) ? 0 : garantie.hashCode());
		result = prime * result + idJustificatif;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JustificatifGarantie other = (JustificatifGarantie) obj;
		if (Justificatif == null) {
			if (other.Justificatif != null)
				return false;
		} else if (!Justificatif.equals(other.Justificatif))
			return false;
		if (descriptionJustificatif == null) {
			if (other.descriptionJustificatif != null)
				return false;
		} else if (!descriptionJustificatif.equals(other.descriptionJustificatif))
			return false;
		if (garantie == null) {
			if (other.garantie != null)
				return false;
		} else if (!garantie.equals(other.garantie))
			return false;
		if (idJustificatif != other.idJustificatif)
			return false;
		return true;
	}
	
    
    


}
