package com.meczy.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;



@Table(name = "justificatifrevumensuel", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Justificatifrevumensuel  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private int idJustificatif;
    private String descriptionJustificatif;
    private String Justificatif;
	// garantie
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idbudget" , nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Budget budget ;
	public Justificatifrevumensuel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@JsonCreator
	public Justificatifrevumensuel(
			@JsonProperty("idJustificatif") int idJustificatif,
			@JsonProperty("descriptionJustificatif") String descriptionJustificatif ,
			@JsonProperty("justificatif") String justificatif ,
			@JsonProperty("budget") Budget budget)
	
	{                                                                                                                                                                                                                                                                                                                                                         
		this.idJustificatif = idJustificatif;
		this.descriptionJustificatif = descriptionJustificatif;
		this.Justificatif =justificatif;
		this.budget = budget;
		
	}
	
	
	public int getIdJustificatif() {
		return idJustificatif;
	}
	public void setIdJustificatif(int idJustificatif) {
		this.idJustificatif = idJustificatif;
	}
	public String getDescriptionJustificatif() {
		return descriptionJustificatif;
	}
	public void setDescriptionJustificatif(String descriptionJustificatif) {
		this.descriptionJustificatif = descriptionJustificatif;
	}
	public String getJustificatif() {
		return Justificatif;
	}
	public void setJustificatif(String justificatif) {
		Justificatif = justificatif;
	}

	public Budget getBudget() {
		return budget;
	}

	public void setBudget(Budget budget) {
		this.budget = budget;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
