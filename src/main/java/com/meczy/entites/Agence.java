package com.meczy.entites;

import java.io.Serializable;



import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "agence", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Agence implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private int idagence;
	private String codeagence;
	private String adresseagence;
	private int etat;


	//utiliateur
	@OneToMany(mappedBy = "agence" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Utilisateur> utilisateurs;
	
	//comité
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idcomite" , nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    //@JsonManagedReference
	private Comite comite;
	//comité
	@ManyToOne(fetch = FetchType.LAZY , optional = false )
	@JoinColumn(name = "idquartier" , nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	//@JsonBackReference
	private Quartier quartier;
	
	//Planification
	@OneToMany(mappedBy = "agence" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Planification> planifications;
	//utiliateur
	@OneToMany(mappedBy = "agence" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Dossierindividuel> dossierindividuel;

	@JsonCreator
	public Agence(
			@JsonProperty("idagence") int idagence,
			@JsonProperty("comite") Comite comite ,
			@JsonProperty("quartier") Quartier quartier ,
			@JsonProperty("etat") int etat,
			@JsonProperty("codeagence") String codeagence ,
			@JsonProperty("adresseagence") String adresseagence ,
			@JsonProperty("Planification") List<Planification> planifications,
			@JsonProperty("utilisateur") List<Utilisateur> utilisateurs)
	
	{                                                                                                                                                                                                                                                                                                                                                         
		this.idagence = idagence;
		this.codeagence = codeagence;
		this.adresseagence = adresseagence;
		this.utilisateurs=utilisateurs;
		this.comite = comite;
		this.etat = etat;
		this.quartier = quartier;
		this.planifications=planifications;
	
	}
	public Agence() {
		// TODO Auto-generated constructor stub
	}
	public int getIdagence() {
		return idagence;
	}
	public void setIdagence(int idagence) {
		this.idagence = idagence;
	}
	public String getCodeagence() {
		return codeagence;
	}
	public void setCodeagence(String codeagence) {
		this.codeagence = codeagence;
	}
	public String getAdresseagence() {
		return adresseagence;
	}
	public void setAdresseagence(String adresseagence) {
		this.adresseagence = adresseagence;
	}
	public List<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}
	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}
	public Comite getComite() {
		return comite;
	}
	public void setComite(Comite comite) {
		this.comite = comite;
	}
	public Quartier getQuartier() {
		return quartier;
	}
	public void setQuartier(Quartier quartier) {
		this.quartier = quartier;
	}
	public List<Planification> getPlanifications() {
		return planifications;
	}
	public void setPlanifications(List<Planification> planifications) {
		this.planifications = planifications;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getEtat() {
		return etat;
	}
	public void setEtat(int etat) {
		this.etat = etat;
	}
	
	
	public List<Dossierindividuel> getDossierindividuel() {
		return dossierindividuel;
	}
	public void setDossierindividuel(List<Dossierindividuel> dossierindividuel) {
		this.dossierindividuel = dossierindividuel;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresseagence == null) ? 0 : adresseagence.hashCode());
		result = prime * result + ((codeagence == null) ? 0 : codeagence.hashCode());
		result = prime * result + ((comite == null) ? 0 : comite.hashCode());
		result = prime * result + etat;
		result = prime * result + idagence;
		result = prime * result + ((planifications == null) ? 0 : planifications.hashCode());
		result = prime * result + ((quartier == null) ? 0 : quartier.hashCode());
		result = prime * result + ((utilisateurs == null) ? 0 : utilisateurs.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agence other = (Agence) obj;
		if (adresseagence == null) {
			if (other.adresseagence != null)
				return false;
		} else if (!adresseagence.equals(other.adresseagence))
			return false;
		if (codeagence == null) {
			if (other.codeagence != null)
				return false;
		} else if (!codeagence.equals(other.codeagence))
			return false;
		if (comite == null) {
			if (other.comite != null)
				return false;
		} else if (!comite.equals(other.comite))
			return false;
		if (etat != other.etat)
			return false;
		if (idagence != other.idagence)
			return false;
		if (planifications == null) {
			if (other.planifications != null)
				return false;
		} else if (!planifications.equals(other.planifications))
			return false;
		if (quartier == null) {
			if (other.quartier != null)
				return false;
		} else if (!quartier.equals(other.quartier))
			return false;
		if (utilisateurs == null) {
			if (other.utilisateurs != null)
				return false;
		} else if (!utilisateurs.equals(other.utilisateurs))
			return false;
		return true;
	}
	
	
}

