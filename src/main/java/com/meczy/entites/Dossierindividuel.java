package com.meczy.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

	
@Table(name = "dossierindividuel", catalog = "meczyDB"  )	
@Entity
@Data  @ToString
@DiscriminatorValue("DI")
public class Dossierindividuel  extends Dossier  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String  photo;
	private int  natureentrepreneur;
	private String  raisonsociale;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date    dateadhesion;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date    datepayementfrais;
	private long    montant;
	private String  natureidentification ;
	private String    numeropiece;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date    datenaissance;
	private String  lieudenaissance;
	private String  nompere;
	private String  nommere;
	private int     genre;
	private String  adresse;
	private String  adresseentreprise;
	private String  enaffaire;
	private long    teldomicile;
	private long    telportable;
	private long    bureau;
	private int    situationfamilial;
	private int    enfantencharge;
	private int    autrecharge;
	private String    conjoint;
	private int    emprunteur;
	
	// Budget
	// private long totalrevenus;
	// private long totaldepense ;
	// bilan entreprise
	private long epargneentreprise ; 
	private long argentenmainentreprise ; 
	private long stockentreprise ; 
	private long comptearecevoit ; 
	private long immombilisation ; 
	private String autrebilanentreprise ; 
	private long montantautrebilanentreprise ; 
	private long totalactifbilan ; 
	private long totalpassifbilan ; 
	private long emprunt ; 
	private long detteapayer ; 
	private long comptefournisseur ; 
	private String autrespassifbilan ;
	private long montantautrespassifbilan ;
	private long situationnetbilan ;
	private long totalpassif ;
	// Etat des resultats 
	private int periodiciteactivite ;
	private int periodicitprevissioneactivite ;
	
	// Analyse des besoins 
	private long besoininvestissement ;
	private long totaledepensebesoin ;
	private long apportpersonnel ;
	private long besoinreelcredit ;
	// Analyse capacités  A
	private long arevenuspersonnel ;
	private long adepensepersonnelle ;
	private int asurplusdeficit ;
	// Analyse capacités  B
	private long bbbeneficeactivite ;
	private int bsurplusdeficitpersonnel ;
	private long bdisponibiliteremboursement ;
	// Analyse capacités  C
	private long cmargedisponible ;
	private int cdisponibiliteremboursement ;
	private int cpremierremboursement ;
	private long capaciteremboursement ;
	


	//Utilisateur
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idutilisateur" , nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    //@JsonManagedReference
	private Utilisateur utilisateur;
	@OneToMany(mappedBy = "dossierindividuel" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Budget> budgets;
	
	
	// Revenumensuel
	@OneToMany(mappedBy = "dossierindividuel" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Bilanpersonnel> bilanpersonnels;
	


	




	public Dossierindividuel(int iddossier, Date datedemande, int statutdossier, int etatdossier, long montant,
			long totalegarantie, long numerodemande, long comptemembre, Agence agence, String activite, Comite comite,
			Sousactivite sousactivite, long montantaccorde, int tauxinteret, int duree, int nombreecheance,
			long montantremboursement, int periodicite, long epargneexige, String butcredit, int typrefontcredit,
			Date datedeblocage, Date dateecheance, Date dateecheancefinale, int dejacredit, String pourquoihabitude,
			String parquiHabitude, Date datehabitude, long montantHabitude, long montantdemande,
			int respectengagegements, long soldecompteepargne, int tauxinteretannuel, Date datesignature,
			String recommandationagent, int decision, Objectcredit objectcredit, String motifs, String photo,
			int natureentrepreneur, String raisonsociale, Date dateadhesion, Date datepayementfrais, long montant2,
			String natureidentification, String numeropiece, Date datenaissance, String lieudenaissance, String nompere,
			String nommere, int genre, String adresse, String adresseentreprise, String enaffaire, long teldomicile,
			long telportable, long bureau, int situationfamilial, int enfantencharge, int autrecharge, String conjoint,
			long epargneentreprise, long argentenmainentreprise, long stockentreprise, long comptearecevoit,
			long immombilisation, String autrebilanentreprise, long montantautrebilanentreprise, long totalactifbilan,
			long totalpassifbilan, long emprunt, long detteapayer, long comptefournisseur, String autrespassifbilan,
			long montantautrespassifbilan, long situationnetbilan, long totalpassif, int periodiciteactivite,
			int periodicitprevissioneactivite, long besoininvestissement, long totaledepensebesoin,
			long apportpersonnel, long besoinreelcredit, long arevenuspersonnel, long adepensepersonnelle,
			int asurplusdeficit, long bbbeneficeactivite, int bsurplusdeficitpersonnel,int emprunteur,
			long bdisponibiliteremboursement, long cmargedisponible, int cdisponibiliteremboursement,
			int cpremierremboursement, long capaciteremboursement, Utilisateur utilisateur, List<Budget> budgets,
			List<Bilanpersonnel> bilanpersonnels) {
		super(iddossier, datedemande, statutdossier, etatdossier, montant, totalegarantie, numerodemande, comptemembre,
				agence, activite, comite, sousactivite, montantaccorde, tauxinteret, duree, nombreecheance,
				montantremboursement, periodicite, epargneexige, butcredit, typrefontcredit, datedeblocage,
				dateecheance, dateecheancefinale, dejacredit, pourquoihabitude, parquiHabitude, datehabitude,
				montantHabitude, montantdemande, respectengagegements, soldecompteepargne, tauxinteretannuel,
				datesignature, recommandationagent, decision, objectcredit, motifs);
		this.photo = photo;
		this.natureentrepreneur = natureentrepreneur;
		this.raisonsociale = raisonsociale;
		this.dateadhesion = dateadhesion;
		this.datepayementfrais = datepayementfrais;
		montant = montant2;
		this.natureidentification = natureidentification;
		this.numeropiece = numeropiece;
		this.datenaissance = datenaissance;
		this.lieudenaissance = lieudenaissance;
		this.nompere = nompere;
		this.nommere = nommere;
		this.genre = genre;
		this.adresse = adresse;
		this.adresseentreprise = adresseentreprise;
		this.enaffaire = enaffaire;
		this.teldomicile = teldomicile;
		this.telportable = telportable;
		this.bureau = bureau;
		this.situationfamilial = situationfamilial;
		this.enfantencharge = enfantencharge;
		this.autrecharge = autrecharge;
		this.conjoint = conjoint;
		this.epargneentreprise = epargneentreprise;
		this.argentenmainentreprise = argentenmainentreprise;
		this.stockentreprise = stockentreprise;
		this.comptearecevoit = comptearecevoit;
		this.immombilisation = immombilisation;
		this.autrebilanentreprise = autrebilanentreprise;
		this.montantautrebilanentreprise = montantautrebilanentreprise;
		this.totalactifbilan = totalactifbilan;
		this.totalpassifbilan = totalpassifbilan;
		this.emprunt = emprunt;
		this.detteapayer = detteapayer;
		this.comptefournisseur = comptefournisseur;
		this.autrespassifbilan = autrespassifbilan;
		this.montantautrespassifbilan = montantautrespassifbilan;
		this.situationnetbilan = situationnetbilan;
		this.totalpassif = totalpassif;
		this.periodiciteactivite = periodiciteactivite;
		this.periodicitprevissioneactivite = periodicitprevissioneactivite;
		this.besoininvestissement = besoininvestissement;
		this.totaledepensebesoin = totaledepensebesoin;
		this.apportpersonnel = apportpersonnel;
		this.besoinreelcredit = besoinreelcredit;
		this.arevenuspersonnel = arevenuspersonnel;
		this.adepensepersonnelle = adepensepersonnelle;
		this.asurplusdeficit = asurplusdeficit;
		this.bbbeneficeactivite = bbbeneficeactivite;
		this.bsurplusdeficitpersonnel = bsurplusdeficitpersonnel;
		this.bdisponibiliteremboursement = bdisponibiliteremboursement;
		this.cmargedisponible = cmargedisponible;
		this.cdisponibiliteremboursement = cdisponibiliteremboursement;
		this.cpremierremboursement = cpremierremboursement;
		this.capaciteremboursement = capaciteremboursement;
		this.utilisateur = utilisateur;
		this.budgets = budgets;
		this.bilanpersonnels = bilanpersonnels;
		this.emprunteur = emprunteur;
	}




	/*
	 * @JsonCreator public Dossierindividuel(
	 * 
	 * @JsonProperty("photo")String photo,
	 * 
	 * @JsonProperty("natureentrepreneur")int natureentrepreneur,
	 * 
	 * @JsonProperty("raisonsociale")String raisonsociale,
	 * 
	 * @JsonProperty("dateadhesion")Date dateadhesion,
	 * 
	 * @JsonProperty("datepayementfrais")Date datepayementfrais,
	 * 
	 * @JsonProperty("montant")long montant,
	 * 
	 * @JsonProperty("natureidentification")String natureidentification,
	 * 
	 * @JsonProperty("numeropiece")String numeropiece,
	 * 
	 * @JsonProperty("datenaissance")Date datenaissance,
	 * 
	 * @JsonProperty("lieudenaissance")String lieudenaissance,
	 * 
	 * @JsonProperty("datenaissance")String nompere,
	 * 
	 * @JsonProperty("nompere")String nommere,
	 * 
	 * @JsonProperty("genre")int genre,
	 * 
	 * @JsonProperty("adresse")String adresse,
	 * 
	 * @JsonProperty("adresseentreprise")String adresseentreprise,
	 * 
	 * @JsonProperty("enaffaire") String enaffaire,
	 * 
	 * @JsonProperty("situationfamilial") int situationfamilial,
	 * 
	 * @JsonProperty("teldomicile")long teldomicile,
	 * 
	 * @JsonProperty("telportable")long telportable,
	 * 
	 * @JsonProperty("bureau")long bureau,
	 * 
	 * @JsonProperty("enfantencharge")int enfantencharge,
	 * 
	 * @JsonProperty("autrecharge")int autrecharge,
	 * 
	 * @JsonProperty("conjoint")String conjoint,
	 * //@JsonProperty("cituationmatrimoniale")int cituationmatrimoniale,
	 * //@JsonProperty("totalrevenus")long totalrevenus,
	 * //@JsonProperty("totaldepense")long totaldepense,
	 * 
	 * @JsonProperty("epargneentreprise")long epargneentreprise,
	 * 
	 * @JsonProperty("argentenmainentreprise")long argentenmainentreprise,
	 * 
	 * @JsonProperty("stockentreprise")long stockentreprise,
	 * 
	 * @JsonProperty("comptearecevoit")long comptearecevoit,
	 * 
	 * @JsonProperty("immombilisation")long immombilisation,
	 * 
	 * @JsonProperty("autrebilanentreprise")String autrebilanentreprise,
	 * 
	 * @JsonProperty("montantautrebilanentreprise")long montantautrebilanentreprise,
	 * 
	 * @JsonProperty("totalactifbilan")long totalactifbilan,
	 * 
	 * @JsonProperty("totalpassifbilan")long totalpassifbilan,
	 * 
	 * @JsonProperty("emprunt")long emprunt,
	 * 
	 * @JsonProperty("detteapayer")long detteapayer,
	 * 
	 * @JsonProperty("comptefournisseur")long comptefournisseur,
	 * 
	 * @JsonProperty("autrespassifbilan")String autrespassifbilan,
	 * 
	 * @JsonProperty("montantautrespassifbilan")long montantautrespassifbilan,
	 * 
	 * @JsonProperty("situationnetbilan")long situationnetbilan,
	 * 
	 * @JsonProperty("totalpassif")long totalpassif,
	 * 
	 * @JsonProperty("periodiciteactivite")int periodiciteactivite,
	 * 
	 * @JsonProperty("periodicitprevissioneactivite")int
	 * periodicitprevissioneactivite,
	 * 
	 * @JsonProperty("besoininvestissement")long besoininvestissement,
	 * 
	 * @JsonProperty("totaledepensebesoin")long totaledepensebesoin,
	 * 
	 * @JsonProperty("apportpersonnel")long apportpersonnel,
	 * 
	 * @JsonProperty("besoinreelcredit")long besoinreelcredit,
	 * 
	 * @JsonProperty("arevenuspersonnel")long arevenuspersonnel,
	 * 
	 * @JsonProperty("adepensepersonnelle")long adepensepersonnelle,
	 * 
	 * @JsonProperty("asurplusdeficit")int asurplusdeficit,
	 * 
	 * @JsonProperty("bbbeneficeactivite")long bbbeneficeactivite,
	 * 
	 * @JsonProperty("bsurplusdeficitpersonnel")int bsurplusdeficitpersonnel,
	 * 
	 * @JsonProperty("bdisponibiliteremboursement")long bdisponibiliteremboursement,
	 * 
	 * @JsonProperty("cmargedisponible")long cmargedisponible,
	 * 
	 * @JsonProperty("cdisponibiliteremboursement")int cdisponibiliteremboursement,
	 * 
	 * @JsonProperty("cpremierremboursement")int cpremierremboursement,
	 * 
	 * @JsonProperty("capaciteremboursement")long capaciteremboursement)
	 * //@JsonProperty("revenumensuel")List<Budget> budgets,
	 * //@JsonProperty("bilanpersonnel")List<Bilanpersonnel> bilanpersonnels)
	 * 
	 * { this.photo = photo; this.situationfamilial = situationfamilial;
	 * this.natureentrepreneur = natureentrepreneur; this.raisonsociale =
	 * raisonsociale; this.dateadhesion = dateadhesion; this.datepayementfrais =
	 * datepayementfrais; this.montant = montant; this.natureidentification =
	 * natureidentification; this.numeropiece = numeropiece; this.datenaissance =
	 * datenaissance; this.lieudenaissance = lieudenaissance; this.nompere =
	 * nompere; this.nommere = nommere; this.genre = genre; this.adresse = adresse;
	 * this.adresseentreprise = adresseentreprise; this.enaffaire = enaffaire;
	 * this.teldomicile = teldomicile; this.telportable = telportable; this.bureau =
	 * bureau; this.enfantencharge = enfantencharge; this.autrecharge = autrecharge;
	 * this.conjoint = conjoint; //this.cituationmatrimoniale =
	 * cituationmatrimoniale; //this.totalrevenus = totalrevenus;
	 * //this.totaldepense = totaldepense; this.epargneentreprise =
	 * epargneentreprise; this.argentenmainentreprise = argentenmainentreprise;
	 * this.stockentreprise = stockentreprise; this.comptearecevoit =
	 * comptearecevoit; this.immombilisation = immombilisation;
	 * this.autrebilanentreprise = autrebilanentreprise;
	 * this.montantautrebilanentreprise = montantautrebilanentreprise;
	 * this.totalactifbilan = totalactifbilan; this.totalpassifbilan =
	 * totalpassifbilan; this.emprunt = emprunt; this.detteapayer = detteapayer;
	 * this.comptefournisseur = comptefournisseur; this.autrespassifbilan =
	 * autrespassifbilan; this.montantautrespassifbilan = montantautrespassifbilan;
	 * this.situationnetbilan = situationnetbilan; this.totalpassif = totalpassif;
	 * this.periodiciteactivite = periodiciteactivite;
	 * this.periodicitprevissioneactivite = periodicitprevissioneactivite;
	 * this.besoininvestissement = besoininvestissement; this.totaledepensebesoin =
	 * totaledepensebesoin; this.apportpersonnel = apportpersonnel;
	 * this.besoinreelcredit = besoinreelcredit; this.arevenuspersonnel =
	 * arevenuspersonnel; this.adepensepersonnelle = adepensepersonnelle;
	 * this.asurplusdeficit = asurplusdeficit; this.bbbeneficeactivite =
	 * bbbeneficeactivite; this.bsurplusdeficitpersonnel = bsurplusdeficitpersonnel;
	 * this.bdisponibiliteremboursement = bdisponibiliteremboursement;
	 * this.cmargedisponible = cmargedisponible; this.cdisponibiliteremboursement =
	 * cdisponibiliteremboursement; this.cpremierremboursement =
	 * cpremierremboursement; this.capaciteremboursement = capaciteremboursement;
	 * //this.budgets = budgets; //this.bilanpersonnels = bilanpersonnels; }
	 */

	// revenumensuels
	
	
	

	public Dossierindividuel() {
		// TODO Auto-generated constructor stub
	}




	public int getEmprunteur() {
		return emprunteur;
	}




	public void setEmprunteur(int emprunteur) {
		this.emprunteur = emprunteur;
	}




	public Utilisateur getUtilisateur() {
		return utilisateur;
	}




	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}




	public List<Budget> getBudgets() {
		return budgets;
	}


	public void setBudgets(List<Budget> budgets) {
		this.budgets = budgets;
	}


	public int getSituationfamilial() {
		return situationfamilial;
	}


	public void setSituationfamilial(int situationfamilial) {
		this.situationfamilial = situationfamilial;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public int getNatureentrepreneur() {
		return natureentrepreneur;
	}


	public void setNatureentrepreneur(int natureentrepreneur) {
		this.natureentrepreneur = natureentrepreneur;
	}


	public String getRaisonsociale() {
		return raisonsociale;
	}


	public void setRaisonsociale(String raisonsociale) {
		this.raisonsociale = raisonsociale;
	}


	public Date getDateadhesion() {
		return dateadhesion;
	}


	public void setDateadhesion(Date dateadhesion) {
		this.dateadhesion = dateadhesion;
	}


	public Date getDatepayementfrais() {
		return datepayementfrais;
	}


	public void setDatepayementfrais(Date datepayementfrais) {
		this.datepayementfrais = datepayementfrais;
	}


	public long getMontant() {
		return montant;
	}


	public void setMontant(long montant) {
		this.montant = montant;
	}


	public String getNatureidentification() {
		return natureidentification;
	}


	public void setNatureidentification(String natureidentification) {
		this.natureidentification = natureidentification;
	}





	public String getNumeropiece() {
		return numeropiece;
	}


	public void setNumeropiece(String numeropiece) {
		this.numeropiece = numeropiece;
	}


	public Date getDatenaissance() {
		return datenaissance;
	}


	public void setDatenaissance(Date datenaissance) {
		this.datenaissance = datenaissance;
	}


	public String getLieudenaissance() {
		return lieudenaissance;
	}


	public void setLieudenaissance(String lieudenaissance) {
		this.lieudenaissance = lieudenaissance;
	}


	public String getNompere() {
		return nompere;
	}


	public void setNompere(String nompere) {
		this.nompere = nompere;
	}


	public String getNommere() {
		return nommere;
	}


	public void setNommere(String nommere) {
		this.nommere = nommere;
	}


	public int getGenre() {
		return genre;
	}


	public void setGenre(int genre) {
		this.genre = genre;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public String getAdresseentreprise() {
		return adresseentreprise;
	}


	public void setAdresseentreprise(String adresseentreprise) {
		this.adresseentreprise = adresseentreprise;
	}


	public String getEnaffaire() {
		return enaffaire;
	}


	public void setEnaffaire(String enaffaire) {
		this.enaffaire = enaffaire;
	}


	public long getTeldomicile() {
		return teldomicile;
	}


	public void setTeldomicile(long teldomicile) {
		this.teldomicile = teldomicile;
	}


	public long getTelportable() {
		return telportable;
	}


	public void setTelportable(long telportable) {
		this.telportable = telportable;
	}


	public long getBureau() {
		return bureau;
	}


	public void setBureau(long bureau) {
		this.bureau = bureau;
	}


	public int getEnfantencharge() {
		return enfantencharge;
	}


	public void setEnfantencharge(int enfantencharge) {
		this.enfantencharge = enfantencharge;
	}


	public int getAutrecharge() {
		return autrecharge;
	}


	public void setAutrecharge(int autrecharge) {
		this.autrecharge = autrecharge;
	}


	public String getConjoint() {
		return conjoint;
	}


	public void setConjoint(String conjoint) {
		this.conjoint = conjoint;
	}


	/*public int getCituationmatrimoniale() {
		return cituationmatrimoniale;
	}


	public void setCituationmatrimoniale(int cituationmatrimoniale) {
		this.cituationmatrimoniale = cituationmatrimoniale;
	}*/




	public long getEpargneentreprise() {
		return epargneentreprise;
	}


	public void setEpargneentreprise(long epargneentreprise) {
		this.epargneentreprise = epargneentreprise;
	}


	public long getArgentenmainentreprise() {
		return argentenmainentreprise;
	}


	public void setArgentenmainentreprise(long argentenmainentreprise) {
		this.argentenmainentreprise = argentenmainentreprise;
	}


	public long getStockentreprise() {
		return stockentreprise;
	}


	public void setStockentreprise(long stockentreprise) {
		this.stockentreprise = stockentreprise;
	}


	public long getComptearecevoit() {
		return comptearecevoit;
	}


	public void setComptearecevoit(long comptearecevoit) {
		this.comptearecevoit = comptearecevoit;
	}


	public long getImmombilisation() {
		return immombilisation;
	}


	public void setImmombilisation(long immombilisation) {
		this.immombilisation = immombilisation;
	}


	public String getAutrebilanentreprise() {
		return autrebilanentreprise;
	}


	public void setAutrebilanentreprise(String autrebilanentreprise) {
		this.autrebilanentreprise = autrebilanentreprise;
	}


	public long getMontantautrebilanentreprise() {
		return montantautrebilanentreprise;
	}


	public void setMontantautrebilanentreprise(long montantautrebilanentreprise) {
		this.montantautrebilanentreprise = montantautrebilanentreprise;
	}


	public long getTotalactifbilan() {
		return totalactifbilan;
	}


	public void setTotalactifbilan(long totalactifbilan) {
		this.totalactifbilan = totalactifbilan;
	}


	public long getTotalpassifbilan() {
		return totalpassifbilan;
	}


	public void setTotalpassifbilan(long totalpassifbilan) {
		this.totalpassifbilan = totalpassifbilan;
	}


	public long getEmprunt() {
		return emprunt;
	}


	public void setEmprunt(long emprunt) {
		this.emprunt = emprunt;
	}


	public long getDetteapayer() {
		return detteapayer;
	}


	public void setDetteapayer(long detteapayer) {
		this.detteapayer = detteapayer;
	}


	public long getComptefournisseur() {
		return comptefournisseur;
	}


	public void setComptefournisseur(long comptefournisseur) {
		this.comptefournisseur = comptefournisseur;
	}


	public String getAutrespassifbilan() {
		return autrespassifbilan;
	}


	public void setAutrespassifbilan(String autrespassifbilan) {
		this.autrespassifbilan = autrespassifbilan;
	}


	public long getMontantautrespassifbilan() {
		return montantautrespassifbilan;
	}


	public void setMontantautrespassifbilan(long montantautrespassifbilan) {
		this.montantautrespassifbilan = montantautrespassifbilan;
	}


	public long getSituationnetbilan() {
		return situationnetbilan;
	}


	public void setSituationnetbilan(long situationnetbilan) {
		this.situationnetbilan = situationnetbilan;
	}


	public long getTotalpassif() {
		return totalpassif;
	}


	public void setTotalpassif(long totalpassif) {
		this.totalpassif = totalpassif;
	}


	public int getPeriodiciteactivite() {
		return periodiciteactivite;
	}


	public void setPeriodiciteactivite(int periodiciteactivite) {
		this.periodiciteactivite = periodiciteactivite;
	}


	public int getPeriodicitprevissioneactivite() {
		return periodicitprevissioneactivite;
	}


	public void setPeriodicitprevissioneactivite(int periodicitprevissioneactivite) {
		this.periodicitprevissioneactivite = periodicitprevissioneactivite;
	}


	public long getBesoininvestissement() {
		return besoininvestissement;
	}


	public void setBesoininvestissement(long besoininvestissement) {
		this.besoininvestissement = besoininvestissement;
	}


	public long getTotaledepensebesoin() {
		return totaledepensebesoin;
	}


	public void setTotaledepensebesoin(long totaledepensebesoin) {
		this.totaledepensebesoin = totaledepensebesoin;
	}


	public long getApportpersonnel() {
		return apportpersonnel;
	}


	public void setApportpersonnel(long apportpersonnel) {
		this.apportpersonnel = apportpersonnel;
	}


	public long getBesoinreelcredit() {
		return besoinreelcredit;
	}


	public void setBesoinreelcredit(long besoinreelcredit) {
		this.besoinreelcredit = besoinreelcredit;
	}


	public long getArevenuspersonnel() {
		return arevenuspersonnel;
	}


	public void setArevenuspersonnel(long arevenuspersonnel) {
		this.arevenuspersonnel = arevenuspersonnel;
	}


	public long getAdepensepersonnelle() {
		return adepensepersonnelle;
	}


	public void setAdepensepersonnelle(long adepensepersonnelle) {
		this.adepensepersonnelle = adepensepersonnelle;
	}


	public int getAsurplusdeficit() {
		return asurplusdeficit;
	}


	public void setAsurplusdeficit(int asurplusdeficit) {
		this.asurplusdeficit = asurplusdeficit;
	}


	public long getBbbeneficeactivite() {
		return bbbeneficeactivite;
	}


	public void setBbbeneficeactivite(long bbbeneficeactivite) {
		this.bbbeneficeactivite = bbbeneficeactivite;
	}


	public int getBsurplusdeficitpersonnel() {
		return bsurplusdeficitpersonnel;
	}


	public void setBsurplusdeficitpersonnel(int bsurplusdeficitpersonnel) {
		this.bsurplusdeficitpersonnel = bsurplusdeficitpersonnel;
	}


	public long getBdisponibiliteremboursement() {
		return bdisponibiliteremboursement;
	}


	public void setBdisponibiliteremboursement(long bdisponibiliteremboursement) {
		this.bdisponibiliteremboursement = bdisponibiliteremboursement;
	}


	public long getCmargedisponible() {
		return cmargedisponible;
	}


	public void setCmargedisponible(long cmargedisponible) {
		this.cmargedisponible = cmargedisponible;
	}


	public int getCdisponibiliteremboursement() {
		return cdisponibiliteremboursement;
	}


	public void setCdisponibiliteremboursement(int cdisponibiliteremboursement) {
		this.cdisponibiliteremboursement = cdisponibiliteremboursement;
	}


	public int getCpremierremboursement() {
		return cpremierremboursement;
	}


	public void setCpremierremboursement(int cpremierremboursement) {
		this.cpremierremboursement = cpremierremboursement;
	}


	public long getCapaciteremboursement() {
		return capaciteremboursement;
	}


	public void setCapaciteremboursement(long capaciteremboursement) {
		this.capaciteremboursement = capaciteremboursement;
	}




	public List<Bilanpersonnel> getBilanpersonnels() {
		return bilanpersonnels;
	}


	public void setBilanpersonnels(List<Bilanpersonnel> bilanpersonnels) {
		this.bilanpersonnels = bilanpersonnels;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

	


	

	

	


}
