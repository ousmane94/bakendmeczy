
package com.meczy.entites;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "utilisateur", catalog = "MeczyDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Utilisateur implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	private int idutilisateur;
	private String prenom;
	private String nom;
	private String email;
	@Column(unique =true )
	private String matricule;
	private int etat;
	private int telephone;
	@Column(unique =true )
	//@JsonProperty(access = Access.WRITE_ONLY)
	private String password;
	//private String profils;

	/*
	 * @JsonIgnore
	 * 
	 * @ManyToMany(cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	 * 
	 * @JoinTable(name = "UTILISATEUR_COMITES", joinColumns={
	 * 
	 * @JoinColumn(name = "UTILISATEUR_IDUTILISATEUR", referencedColumnName =
	 * "idUtilisateur") }, inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "COMITE_IDCOMITE", referencedColumnName = "comite") })
	 */
	 //@ManyToMany(targetEntity = Comite.class,cascade = CascadeType.ALL  , fetch = FetchType.LAZY)
     @JsonIgnore
 	@ManyToMany(cascade = CascadeType.ALL)
 	@JoinTable(name = "UTILISATEUR_COMITES", joinColumns={
 			@JoinColumn(name = "UTILISATEUR_IDUTILISATEUR", referencedColumnName = "idutilisateur") }, inverseJoinColumns = {
 					@JoinColumn(name = "COMITES_IDCOMITE", referencedColumnName = "idcomite") })
	 private List<Comite> comites;
	// Guichet
	//@ManyToOne(fetch = FetchType.LAZY)
	//@JoinColumn(name = "idguichet")
	//@ManyToOne(fetch = FetchType.LAZY , optional = false )
	
	@ManyToOne(cascade = CascadeType.ALL  , fetch = FetchType.LAZY,optional = true)
	@JoinColumn(name = "idagence" , nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
   // @JsonBackReference
    //@JsonManagedReference
	private Agence agence;
	
	@ManyToOne(cascade = CascadeType.ALL  , fetch = FetchType.LAZY)
	@JoinColumn(name = "idprofils" , nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
    // @JsonBackReference //cest pour bloquer 
    //@JsonManagedReference
	private Profils profils;

	//Rendezvous
	@OneToMany( mappedBy = "utilisateur" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Rendezvous> rendezvous;
	//Rendezvous
	@OneToMany( mappedBy = "utilisateur" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Dossierindividuel> dossierindividuel;
	//Rendezvous
	@OneToMany( mappedBy = "utilisateur" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Recommandation> recommandations;

	
	@JsonCreator
	public Utilisateur(
			@JsonProperty("idutilisateur")int idutilisateur, 
			@JsonProperty("prenom") String prenom,
			@JsonProperty("nom") String nom, 
			@JsonProperty("email") String email,
			@JsonProperty("telephone") int telephone,
			@JsonProperty("matricule")String matricule,
			@JsonProperty("password") String password,
			@JsonProperty("etat") int etat,
			@JsonProperty("profils")Profils profils,
	        @JsonProperty("agence") Agence agence)
	       // @JsonProperty("dossier") List<Dossier> dossiers,
	       // @JsonProperty("recommandation") List<Recommandation> recommandations,
	       // @JsonProperty("comite") List<Comite> comites)
			
 {
		//this.recommandations = recommandations;
		this.idutilisateur = idutilisateur;
		this.prenom = prenom;
		this.nom = nom;
		this.matricule = matricule;
		this.email = email;
		this.telephone = telephone;
		this.password = password;
		this.etat = etat;
		this.profils = profils;
		this.agence = agence;
		// this.comites=comites;
		//this.dossiers=dossiers;


	}
	
	public Utilisateur() {}

	public int getIdutilisateur() {
		return idutilisateur;
	}

	public void setIdutilisateur(int idUtilisateur) {
		this.idutilisateur = idUtilisateur;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Comite> getComites() {
		return comites;
	}

	public void setComites(List<Comite> comites) {
		this.comites = comites;
	}

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	public Profils getProfils() {
		return profils;
	}

	public void setProfils(Profils profils) {
		this.profils = profils;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Rendezvous> getRendezvous() {
		return rendezvous;
	}

	public void setRendezvous(List<Rendezvous> rendezvous) {
		this.rendezvous = rendezvous;
	}

	public List<Dossierindividuel> getDossierindividuel() {
		return dossierindividuel;
	}

	public void setDossierindividuel(List<Dossierindividuel> dossierindividuel) {
		this.dossierindividuel = dossierindividuel;
	}

	public List<Recommandation> getRecommandations() {
		return recommandations;
	}

	public void setRecommandations(List<Recommandation> recommandations) {
		this.recommandations = recommandations;
	}


	
	

	

	

	

}
