package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;


@Table(name = "activitecredit", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Activitecredit implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	private int idactivitecredit;
	private String descriptionactivitecredit;
	private int etat;

	// dossiers
	@OneToMany(mappedBy = "activitecredit" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Sousactivite> sousactivite;
	
	//secteurcredit
	@ManyToOne(fetch = FetchType.LAZY , optional = false )
	@JoinColumn(name = "idsecteurcredit" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Secteurcredit secteurcredit;
	
	
	@JsonCreator
	public Activitecredit(
			@JsonProperty("idactivitecredit") int idactivitecredit, 
			@JsonProperty("descriptionactivitecredit") String descriptionactivitecredit, 
			@JsonProperty("secteurcredit") Secteurcredit secteurcredit, 
			@JsonProperty("etat") int etat)
			
     {
		this.idactivitecredit = idactivitecredit;
		this.descriptionactivitecredit = descriptionactivitecredit;
		this.secteurcredit = secteurcredit;
		this.etat = etat;

	}


	public Activitecredit() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdactivitecredit() {
		return idactivitecredit;
	}

	public void setIdactivitecredit(int idactivitecredit) {
		this.idactivitecredit = idactivitecredit;
	}

	public String getDescriptionactivitecredit() {
		return descriptionactivitecredit;
	}

	public void setDescriptionactivitecredit(String descriptionactivitecredit) {
		this.descriptionactivitecredit = descriptionactivitecredit;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	

	public List<Sousactivite> getSousactivite() {
		return sousactivite;
	}


	public void setSousactivite(List<Sousactivite> sousactivite) {
		this.sousactivite = sousactivite;
	}


	public Secteurcredit getSecteurcredit() {
		return secteurcredit;
	}

	public void setSecteurcredit(Secteurcredit secteurcredit) {
		this.secteurcredit = secteurcredit;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
