package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Usercomite implements Serializable {

		private static final long serialVersionUID = 1L;
		private int idcomite;
		private String matricule;
		
		@JsonCreator
		public Usercomite(
				@JsonProperty("idcomite")int idcomite, 
				@JsonProperty("matricule") String matricule)
	     {
			this.idcomite = idcomite;
			this.matricule = matricule;
		}

		public int getIdcomite() {
			return idcomite;
		}
		public void setIdcomite(int idcomite) {
			this.idcomite = idcomite;
		}
		public String getMatricule() {
			return matricule;
		}
		public void setMatricule(String matricule) {
			this.matricule = matricule;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		
		


}
