package com.meczy.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "rendezvous", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Rendezvous implements Serializable {


	private static final long serialVersionUID = 1L;
	@Id
	private int idrendezvous;
	private long numerorendezvous;
	private long montant;
	private String heurerendezvous;
	private String numeromembre;
	private int etat;
	private int fraisRendezvous;	
	private int naturedossier;


	private String descriptionrendezvous;
	
	//Planification
		
	@ManyToOne(fetch = FetchType.LAZY , optional = false )
	@JoinColumn(name = "idplanification" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	//@JsonBackReference
	//@JsonManagedReference
	private Planification  planification;
	
	//Utilisateur
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idutilisateur" , nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    //@JsonManagedReference
	private Utilisateur utilisateur;

	public Rendezvous() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Rendezvous(int idrendezvous, long numerorendezvous, String heurerendezvous, String numeromembre,
			String descriptionrendezvous,long montant,int fraisRendezvous,Planification planification,int etat,Utilisateur utilisateur) {
		super();
		this.idrendezvous = idrendezvous;
		this.numerorendezvous = numerorendezvous;
		this.heurerendezvous = heurerendezvous;
		this.numeromembre = numeromembre;
		this.descriptionrendezvous = descriptionrendezvous;
		this.planification = planification;
		this.etat = etat;
		this.fraisRendezvous = fraisRendezvous;
		this.montant=montant;
		this.utilisateur=utilisateur;

	}
	
	@JsonCreator
	public Rendezvous(
			@JsonProperty("idrendezvous") int idrendezvous,
			@JsonProperty("numerorendezvous") long numerorendezvous ,
			@JsonProperty("montant") long montant ,
			@JsonProperty("planification") Planification planification ,
			@JsonProperty("numeromembre") String numeromembre ,
			@JsonProperty("etat") int etat,
			@JsonProperty("naturedossier") int naturedossier,
			@JsonProperty("fraisRendezvous") int fraisRendezvous,
			@JsonProperty("utilisateur") Utilisateur utilisateur ,
			@JsonProperty("descriptionrendezvous") String descriptionrendezvous,
			@JsonProperty("heurerendezvous") String  heurerendezvous)
	
	{                                                                                                                                                                                                                                                                                                                                                         
		this.idrendezvous = idrendezvous;
		this.numerorendezvous = numerorendezvous;
		this.descriptionrendezvous = descriptionrendezvous;
		this.numeromembre=numeromembre;
		this.planification = planification;
		this.heurerendezvous=heurerendezvous;
		this.etat=etat;
		this.naturedossier = naturedossier;
		this.fraisRendezvous=fraisRendezvous;
		this.montant=montant;
		this.utilisateur=utilisateur;
	
	}
	
	

	public int getNaturedossier() {
		return naturedossier;
	}

	public void setNaturedossier(int naturedossier) {
		this.naturedossier = naturedossier;
	}

	public int getFraisRendezvous() {
		return fraisRendezvous;
	}

	public void setFraisRendezvous(int fraisRendezvous) {
		this.fraisRendezvous = fraisRendezvous;
	}

	public int getIdrendezvous() {
		return idrendezvous;
	}

	public void setIdrendezvous(int idrendezvous) {
		this.idrendezvous = idrendezvous;
	}

	public long getNumerorendezvous() {
		return numerorendezvous;
	}

	public void setNumerorendezvous(long numerorendezvous) {
		this.numerorendezvous = numerorendezvous;
	}

	public String getHeurerendezvous() {
		return heurerendezvous;
	}

	public void setHeurerendezvous(String heurerendezvous) {
		this.heurerendezvous = heurerendezvous;
	}

	public String getNumeromembre() {
		return numeromembre;
	}

	public void setNumeromembre(String numeromembre) {
		this.numeromembre = numeromembre;
	}

	public String getDescriptionrendezvous() {
		return descriptionrendezvous;
	}

	public void setDescriptionrendezvous(String descriptionrendezvous) {
		this.descriptionrendezvous = descriptionrendezvous;
	}

	public Planification getPlanification() {
		return planification;
	}

	public void setPlanification(Planification planification) {
		this.planification = planification;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	
	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}
	
	

	public long getMontant() {
		return montant;
	}

	public void setMontant(long montant) {
		this.montant = montant;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rendezvous other = (Rendezvous) obj;
		if (descriptionrendezvous == null) {
			if (other.descriptionrendezvous != null)
				return false;
		} else if (!descriptionrendezvous.equals(other.descriptionrendezvous))
			return false;
		if (etat != other.etat)
			return false;
		if (heurerendezvous == null) {
			if (other.heurerendezvous != null)
				return false;
		} else if (!heurerendezvous.equals(other.heurerendezvous))
			return false;
		if (idrendezvous != other.idrendezvous)
			return false;
		if (numeromembre == null) {
			if (other.numeromembre != null)
				return false;
		} else if (!numeromembre.equals(other.numeromembre))
			return false;
		if (numerorendezvous != other.numerorendezvous)
			return false;
		if (planification == null) {
			if (other.planification != null)
				return false;
		} else if (!planification.equals(other.planification))
			return false;
		if (utilisateur == null) {
			if (other.utilisateur != null)
				return false;
		} else if (!utilisateur.equals(other.utilisateur))
			return false;
		return true;
	}



	
	
	
	
	

}
