package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "pays", catalog = "MeczyDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Pays implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private int idpays;
	private String nompays;
	private int etat;

	//region
	@OneToMany( mappedBy = "pays" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Region> regions;
	
	
	
	public Pays() {
	}
	
	

	@JsonCreator
	public Pays(
			@JsonProperty("idpays")int idpays, 
			@JsonProperty("nompays") String nompays, 
			@JsonProperty("etat") int etat,
	        @JsonProperty("regions") List<Region> regions)
			
     {
		this.idpays = idpays;
		this.nompays = nompays;
		this.regions = regions;
		this.etat = etat;


	}
	
	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public int getIdpays() {
		return idpays;
	}
	public void setIdpays(int idpays) {
		this.idpays = idpays;
	}
	public String getNompays() {
		return nompays;
	}
	public void setNompays(String nompays) {
		this.nompays = nompays;
	}
	public List<Region> getRegions() {
		return regions;
	}
	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idpays;
		result = prime * result + ((nompays == null) ? 0 : nompays.hashCode());
		result = prime * result + ((regions == null) ? 0 : regions.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pays other = (Pays) obj;
		if (idpays != other.idpays)
			return false;
		if (nompays == null) {
			if (other.nompays != null)
				return false;
		} else if (!nompays.equals(other.nompays))
			return false;
		if (regions == null) {
			if (other.regions != null)
				return false;
		} else if (!regions.equals(other.regions))
			return false;
		return true;
	}
	
	
	


}
