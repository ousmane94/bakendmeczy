package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;


@Table(name = "budget", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Budget  implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idbudget;
	private long salairenet;
	private long beneficenet;
	private long totalrevenus;
	private String autresrevenus;
	private long montantautrerevenus;
	private long loyer;
	private long nourriture;
	private long electricite;
	private long eau;
	private long telephone;
	private long scolarite;
	private long transport;
	private String autreecheances;
	private long echeances;
	private long autresdepense;
	private long totaledepense;
    // revenu mensuel
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "iddossier" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Dossierindividuel dossierindividuel;
	@OneToMany(mappedBy = "budget" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Justificatifrevumensuel> justificatifrevumensuels;
	
	public   Budget() {

	}
	
	@JsonCreator
	public Budget(
			@JsonProperty("idbudget") int idbudget,
			@JsonProperty("salairenet") long  salairenet ,
			@JsonProperty("beneficenet") long beneficenet ,
			@JsonProperty("dossierindividuel") Dossierindividuel dossierindividuel ,
			@JsonProperty("totalrevenus") long totalrevenus,
	        @JsonProperty("justificatifrevumensuel") List<Justificatifrevumensuel> justificatifrevumensuels,
	        @JsonProperty("autresrevenus") String autresrevenus,
	        @JsonProperty("montantautrerevenus") long montantautrerevenus,
	        @JsonProperty("loyer") long loyer,
	        @JsonProperty("nourriture") long nourriture,
	        @JsonProperty("electricite") long electricite,
	        @JsonProperty("eau") long eau,
	        @JsonProperty("telephone") long telephone,
	        @JsonProperty("scolarite") long scolarite,
	        @JsonProperty("transport") long transport,
	        @JsonProperty("echeances") long echeances,
	        @JsonProperty("autreecheances") String autreecheances,

	        @JsonProperty("autresdepense") long autresdepense,
	        @JsonProperty("totaledepense") long totaledepense)
	  
	   {                                                                                                                                                                                                                                                                                                                                                         
		this.idbudget = idbudget;
		this.salairenet = salairenet;
		this.beneficenet =beneficenet;
		this.dossierindividuel = dossierindividuel;
		this.totalrevenus = totalrevenus;
		this.justificatifrevumensuels = justificatifrevumensuels;
		this.autresrevenus = autresrevenus;
		this.montantautrerevenus = montantautrerevenus;
		this.loyer = loyer;
		this.nourriture = nourriture;
		this.electricite = electricite;
		this.eau = eau;
		this.telephone = telephone;
		this.scolarite = scolarite;
		this.transport = transport;
		this.echeances = echeances;
		this.autresdepense = autresdepense;
		this.totaledepense = totaledepense;
		this.autreecheances=autreecheances;

	}
	
	
	
	public String getAutresrevenus() {
		return autresrevenus;
	}

	public void setAutresrevenus(String autresrevenus) {
		this.autresrevenus = autresrevenus;
	}

	public long getMontantautrerevenus() {
		return montantautrerevenus;
	}

	public void setMontantautrerevenus(long montantautrerevenus) {
		this.montantautrerevenus = montantautrerevenus;
	}

	public long getLoyer() {
		return loyer;
	}

	public void setLoyer(long loyer) {
		this.loyer = loyer;
	}

	public long getNourriture() {
		return nourriture;
	}

	public void setNourriture(long nourriture) {
		this.nourriture = nourriture;
	}

	public long getElectricite() {
		return electricite;
	}

	public void setElectricite(long electricite) {
		this.electricite = electricite;
	}

	public long getEau() {
		return eau;
	}

	public void setEau(long eau) {
		this.eau = eau;
	}

	public long getTelephone() {
		return telephone;
	}

	public void setTelephone(long telephone) {
		this.telephone = telephone;
	}

	public long getScolarite() {
		return scolarite;
	}

	public void setScolarite(long scolarite) {
		this.scolarite = scolarite;
	}

	public long getTransport() {
		return transport;
	}

	public void setTransport(long transport) {
		this.transport = transport;
	}

	public long getEcheances() {
		return echeances;
	}

	public void setEcheances(long echeances) {
		this.echeances = echeances;
	}

	public long getAutresdepense() {
		return autresdepense;
	}

	public void setAutresdepense(long autresdepense) {
		this.autresdepense = autresdepense;
	}

	public long getTotaledepense() {
		return totaledepense;
	}

	public void setTotaledepense(long totaledepense) {
		this.totaledepense = totaledepense;
	}

	public List<Justificatifrevumensuel> getJustificatifrevumensuels() {
		return justificatifrevumensuels;
	}

	public void setJustificatifrevumensuels(List<Justificatifrevumensuel> justificatifrevumensuels) {
		this.justificatifrevumensuels = justificatifrevumensuels;
	}


	public int getIdbudget() {
		return idbudget;
	}

	public void setIdbudget(int idbudget) {
		this.idbudget = idbudget;
	}

	public long getSalairenet() {
		return salairenet;
	}
	public void setSalairenet(long salairenet) {
		this.salairenet = salairenet;
	}
	public long getBeneficenet() {
		return beneficenet;
	}
	public void setBeneficenet(long beneficenet) {
		this.beneficenet = beneficenet;
	}
	public long getTotalrevenus() {
		return totalrevenus;
	}
	public void setTotalrevenus(long totalrevenus) {
		this.totalrevenus = totalrevenus;
	}
	
	public Dossierindividuel getDossierindividuel() {
		return dossierindividuel;
	}

	public void setDossierindividuel(Dossierindividuel dossierindividuel) {
		this.dossierindividuel = dossierindividuel;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAutreecheances() {
		return autreecheances;
	}

	public void setAutreecheances(String autreecheances) {
		this.autreecheances = autreecheances;
	}



}
