package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "profils", catalog = "MeczyDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Profils  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private int idprofils;
	private String nomprofils;
	//utilisateur
	@OneToMany( mappedBy = "profils" ,fetch=FetchType.LAZY)
    @JsonIgnore
	private List<Utilisateur> utilisateurs;
	
	public Profils() {
	}
	
	

	@JsonCreator
	public Profils(
			@JsonProperty("idprofils")int idprofils, 
			@JsonProperty("nomprofils") String nomprofils, 
	        @JsonProperty("utilisateurs") List<Utilisateur> utilisateurs)
			
     {
		this.idprofils = idprofils;
		this.nomprofils = nomprofils;
		this.utilisateurs = utilisateurs;

	}



	public int getIdprofils() {
		return idprofils;
	}



	public void setIdprofils(int idprofils) {
		this.idprofils = idprofils;
	}



	public String getNomprofils() {
		return nomprofils;
	}



	public void setNomprofils(String nomprofils) {
		this.nomprofils = nomprofils;
	}



	public List<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}



	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idprofils;
		result = prime * result + ((nomprofils == null) ? 0 : nomprofils.hashCode());
		result = prime * result + ((utilisateurs == null) ? 0 : utilisateurs.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profils other = (Profils) obj;
		if (idprofils != other.idprofils)
			return false;
		if (nomprofils == null) {
			if (other.nomprofils != null)
				return false;
		} else if (!nomprofils.equals(other.nomprofils))
			return false;
		if (utilisateurs == null) {
			if (other.utilisateurs != null)
				return false;
		} else if (!utilisateurs.equals(other.utilisateurs))
			return false;
		return true;
	}
	
	
	

}
