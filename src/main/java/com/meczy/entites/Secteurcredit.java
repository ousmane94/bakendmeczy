package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@Table(name = "secteurcredit", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Secteurcredit implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	@Id
	private int idsecteurcredit;
	private String descriptionsecteur;
	private int etat;
	
	//Activité
	@OneToMany(mappedBy = "secteurcredit" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Activitecredit> activitecredits;
	
	/*Dossier
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idobjectcredit" , nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private Objectcredit objectcredit;*/
	
    @JsonIgnore
 	@ManyToMany(cascade = CascadeType.ALL)
 	@JoinTable(name = "SECTEURCREDIT_OBJECTCREDITS", joinColumns={
 			@JoinColumn(name = "SECTEURCREDIT_IDSECTEURCREDIT", referencedColumnName = "idsecteurcredit") }, inverseJoinColumns = {
 					@JoinColumn(name = "OBJECTCREDITS_IDOBJECTCREDIT", referencedColumnName = "idobjectcredit") })
	 private List<Objectcredit> objectcredits;
	
	@JsonCreator
	public Secteurcredit(
			@JsonProperty("idsecteurcredit") int idsecteurcredit, 
			@JsonProperty("descriptionsecteur") String descriptionsecteur, 
			@JsonProperty("etat") int etat)
			
     {
		this.idsecteurcredit = idsecteurcredit;
		this.descriptionsecteur = descriptionsecteur;
		this.etat = etat;

	}
	

	public Secteurcredit() {
		super();
		// TODO Auto-generated constructor stub
	}


	


	public int getIdsecteurcredit() {
		return idsecteurcredit;
	}


	public void setIdsecteurcredit(int idsecteurcredit) {
		this.idsecteurcredit = idsecteurcredit;
	}


	public List<Objectcredit> getObjectcredits() {
		return objectcredits;
	}


	public void setObjectcredits(List<Objectcredit> objectcredits) {
		this.objectcredits = objectcredits;
	}


	public String getDescriptionsecteur() {
		return descriptionsecteur;
	}


	public void setDescriptionsecteur(String descriptionsecteur) {
		this.descriptionsecteur = descriptionsecteur;
	}


	public int getEtat() {
		return etat;
	}


	public void setEtat(int etat) {
		this.etat = etat;
	}


	public List<Activitecredit> getActivitecredits() {
		return activitecredits;
	}


	public void setActivitecredits(List<Activitecredit> activitecredits) {
		this.activitecredits = activitecredits;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	

}
