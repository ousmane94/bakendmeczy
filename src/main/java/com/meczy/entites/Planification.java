package com.meczy.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "planification", catalog = "MeczyDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Planification implements Serializable {


	private static final long serialVersionUID = 1L;
	@Id
	private int idplanification;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date dateplanification;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date dateFinaleplanification;
	private int nombredossierplanification;
	private int maxdossierplanification;
	private int etat;

	//Guichet
	//@ManyToOne(fetch = FetchType.LAZY , optional = false )
	//@JoinColumn(name = "idguichet" , nullable = false)
	//@OnDelete(action = OnDeleteAction.CASCADE)
	//private Guichet  guichet;
	
	  //hpital
	 	@ManyToOne(fetch = FetchType.LAZY , optional = false)
	 	@JoinColumn(name = "idagence",nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		//@JsonManagedReference
	 	private Agence agence;
	//guichet
	@OneToMany( mappedBy = "planification" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Rendezvous> rendezvous;
	
	public Planification() {
	}

	@JsonCreator
	public Planification(
			@JsonProperty("idplanification")int idplanification, 
			@JsonProperty("dateplanification") Date dateplanification,
			@JsonProperty("dateFinaleplanification") Date dateFinaleplanification,

			@JsonProperty("nombredossierplanification") int nombredossierplanification, 
			@JsonProperty("maxdossierplanification") int maxdossierplanification,
			@JsonProperty("rendezvous") List<Rendezvous> rendezvous, 
			@JsonProperty("etat") int etat,
	        @JsonProperty("agence") Agence agence)
			
     {
		this.idplanification = idplanification;
		this.dateFinaleplanification=dateFinaleplanification;
		this.dateplanification = dateplanification;
		this.nombredossierplanification = nombredossierplanification;
		this.maxdossierplanification=maxdossierplanification;
		this.rendezvous =rendezvous;
		this.etat = etat;
		this.agence=agence;

	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public int getIdplanification() {
		return idplanification;
	}

	public void setIdplanification(int idplanification) {
		this.idplanification = idplanification;
	}

	public Date getDateplanification() {
		return dateplanification;
	}

	public void setDateplanification(Date dateplanification) {
		this.dateplanification = dateplanification;
	}

	public int getNombredossierplanification() {
		return nombredossierplanification;
	}

	public void setNombredossierplanification(int nombredossierplanification) {
		this.nombredossierplanification = nombredossierplanification;
	}

	public int getMaxdossierplanification() {
		return maxdossierplanification;
	}

	public void setMaxdossierplanification(int maxdossierplanification) {
		this.maxdossierplanification = maxdossierplanification;
	}

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	public List<Rendezvous> getRendezvous() {
		return rendezvous;
	}

	public void setRendezvous(List<Rendezvous> rendezvous) {
		this.rendezvous = rendezvous;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agence == null) ? 0 : agence.hashCode());
		result = prime * result + ((dateplanification == null) ? 0 : dateplanification.hashCode());
		result = prime * result + idplanification;
		result = prime * result + maxdossierplanification;
		result = prime * result + nombredossierplanification;
		result = prime * result + ((rendezvous == null) ? 0 : rendezvous.hashCode());
		return result;
	}

	public Date getDateFinaleplanification() {
		return dateFinaleplanification;
	}

	public void setDateFinaleplanification(Date dateFinaleplanification) {
		this.dateFinaleplanification = dateFinaleplanification;
	}



}
