package com.meczy.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Data;
import lombok.ToString;

@Table(name = "depensemensuel", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Depensemensuel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private int  iddepensemensuel;
	private long  loyer;
	private long  nourriture;
	private long  electricite;
	private long  eau;
	private long  telephone;
	private long  scolarite;
	private long  transport;
	private long  echeancecredit;
	private long  autresdepense;	
	
	// Dossierindividuel
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "iddossier" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Dossierindividuel dossierindividuel;
	



}
