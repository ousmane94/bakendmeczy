package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "departement", catalog = "MeczyDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Departement implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private int iddepartement;
	private String nomdepartement;
	private int etat;

	//region
	@ManyToOne(fetch = FetchType.LAZY , optional = false )
	@JoinColumn(name = "idregion" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    //@JsonManagedReference
	private Region region;
	//commune
	@OneToMany( mappedBy = "departement" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Commune> communes;
	

	public Departement() {
	}

	@JsonCreator
	public Departement (
			@JsonProperty("iddepartement")int iddepartement, 
			@JsonProperty("nomdepartement") String nomdepartement, 
			@JsonProperty("region") Region region, 
			@JsonProperty("etat") int etat,
			@JsonProperty("commune") List<Commune> communes)

	{
		this.iddepartement = iddepartement;
		this.nomdepartement = nomdepartement;
		this.communes = communes;
		this.region=region;
		this.etat = etat;


	}
	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}


	public int getIddepartement() {
		return iddepartement;
	}

	public void setIddepartement(int iddepartement) {
		this.iddepartement = iddepartement;
	}

	public String getNomdepartement() {
		return nomdepartement;
	}

	public void setNomdepartement(String nomdepartement) {
		this.nomdepartement = nomdepartement;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public List<Commune> getCommunes() {
		return communes;
	}

	public void setCommunes(List<Commune> communes) {
		this.communes = communes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((communes == null) ? 0 : communes.hashCode());
		result = prime * result + iddepartement;
		result = prime * result + ((nomdepartement == null) ? 0 : nomdepartement.hashCode());
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Departement other = (Departement) obj;
		if (communes == null) {
			if (other.communes != null)
				return false;
		} else if (!communes.equals(other.communes))
			return false;
		if (iddepartement != other.iddepartement)
			return false;
		if (nomdepartement == null) {
			if (other.nomdepartement != null)
				return false;
		} else if (!nomdepartement.equals(other.nomdepartement))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		return true;
	}
	
	

	
}