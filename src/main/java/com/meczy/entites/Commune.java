package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "commune", catalog = "MeczyDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Commune implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private int idcommune;
	private String nomcommune;
	@JsonProperty("etat") int etat;
	//departement
	@ManyToOne(fetch = FetchType.LAZY , optional = false )
	@JoinColumn(name = "iddepartement" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	//@JsonBackReference
   // @JsonManagedReference
	private Departement departement;
	//quartier
	@OneToMany( mappedBy = "commune" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Quartier> quartiers;
	

	public Commune() {
	}
	
	@JsonCreator
	public Commune(
			@JsonProperty("idcommune")int idcommune, 
			@JsonProperty("nomcommune") String nomcommune, 
			@JsonProperty("etat") int etat,
			@JsonProperty("departement") Departement departement, 
	        @JsonProperty("quartier") List<Quartier> quartiers)
			
     {
		this.idcommune = idcommune;
		this.nomcommune = nomcommune;
		this.departement=departement;
		this.etat = etat;
		this.quartiers = quartiers;


	}
	
	
	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public int getIdcommune() {
		return idcommune;
	}
	public void setIdcommune(int idcommune) {
		this.idcommune = idcommune;
	}
	public String getNomcommune() {
		return nomcommune;
	}
	public void setNomcommune(String nomcommune) {
		this.nomcommune = nomcommune;
	}
	public Departement getDepartement() {
		return departement;
	}
	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	public List<Quartier> getQuartiers() {
		return quartiers;
	}
	public void setQuartiers(List<Quartier> quartiers) {
		this.quartiers = quartiers;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((departement == null) ? 0 : departement.hashCode());
		result = prime * result + idcommune;
		result = prime * result + ((nomcommune == null) ? 0 : nomcommune.hashCode());
		result = prime * result + ((quartiers == null) ? 0 : quartiers.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commune other = (Commune) obj;
		if (departement == null) {
			if (other.departement != null)
				return false;
		} else if (!departement.equals(other.departement))
			return false;
		if (idcommune != other.idcommune)
			return false;
		if (nomcommune == null) {
			if (other.nomcommune != null)
				return false;
		} else if (!nomcommune.equals(other.nomcommune))
			return false;
		if (quartiers == null) {
			if (other.quartiers != null)
				return false;
		} else if (!quartiers.equals(other.quartiers))
			return false;
		return true;
	}
	
	

	
		
}