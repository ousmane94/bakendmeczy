package com.meczy.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;


@Table(name = "recommandation", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Recommandation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private int idrecommandation;
	private String motif;
	private Date datedecision;
	private int decision ;


	// Utilisateur
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idutilisateur" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Utilisateur utilisateur;

	// Dossier
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "iddossier" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Dossier dossier;
	
	
			
			
	@JsonCreator
	public Recommandation(
			@JsonProperty("idrecommandation")int idrecommandation, 
			@JsonProperty("motif")String motif, 
			@JsonProperty("datedecision")Date datedecision, 
			@JsonProperty("decision")int decision, 
			@JsonProperty("utilisateur")Utilisateur utilisateur,
			@JsonProperty("dossier")Dossier dossier) 
	{
		this.idrecommandation = idrecommandation;
		this.motif = motif;
		this.datedecision = datedecision;
		this.decision = decision;
		this.utilisateur = utilisateur;
		this.dossier = dossier;
	}




	public Recommandation() {
		// TODO Auto-generated constructor stub
	}




	public int getIdrecommandation() {
		return idrecommandation;
	}




	public void setIdrecommandation(int idrecommandation) {
		this.idrecommandation = idrecommandation;
	}




	public String getMotif() {
		return motif;
	}




	public void setMotif(String motif) {
		this.motif = motif;
	}




	public Date getDatedecision() {
		return datedecision;
	}




	public void setDatedecision(Date datedecision) {
		this.datedecision = datedecision;
	}




	public int getDecision() {
		return decision;
	}




	public void setDecision(int decision) {
		this.decision = decision;
	}




	public Utilisateur getUtilisateur() {
		return utilisateur;
	}




	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}




	public Dossier getDossier() {
		return dossier;
	}




	public void setDossier(Dossier dossier) {
		this.dossier = dossier;
	}




	public static long getSerialversionuid() {
		return serialVersionUID;
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datedecision == null) ? 0 : datedecision.hashCode());
		result = prime * result + decision;
		result = prime * result + ((dossier == null) ? 0 : dossier.hashCode());
		result = prime * result + idrecommandation;
		result = prime * result + ((motif == null) ? 0 : motif.hashCode());
		result = prime * result + ((utilisateur == null) ? 0 : utilisateur.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Recommandation other = (Recommandation) obj;
		if (datedecision == null) {
			if (other.datedecision != null)
				return false;
		} else if (!datedecision.equals(other.datedecision))
			return false;
		if (decision != other.decision)
			return false;
		if (dossier == null) {
			if (other.dossier != null)
				return false;
		} else if (!dossier.equals(other.dossier))
			return false;
		if (idrecommandation != other.idrecommandation)
			return false;
		if (motif == null) {
			if (other.motif != null)
				return false;
		} else if (!motif.equals(other.motif))
			return false;
		if (utilisateur == null) {
			if (other.utilisateur != null)
				return false;
		} else if (!utilisateur.equals(other.utilisateur))
			return false;
		return true;
	}
	
	
	


}
