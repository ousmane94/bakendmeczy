package com.meczy.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Table(name = "dossiergroupement", catalog = "meczyDB"  )	
@Entity
@Data  @ToString
@DiscriminatorValue("DG")
public class Dossiergroupement  extends Dossier  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  String nomgroupe;
	private  int membredepuis;
	private  int     nombremembre;
	private  int    nombrefemme;
	private  int    membreHomme;
	private  Date datecreation;
	private  String lieuderencontre;
	private  String frequencereunion;
	private  String responsable1;
	private  String responsable2;
	private  String responsable3;
	private  String responsable4;
	private  String responsable5;
	
	private  String domiciletelphone1;
	private  String domiciletelphone2;
	private  String domiciletelphone3;
	private  String domiciletelphone4;
	private  String domiciletelphone5;
	
	




	



	

}
