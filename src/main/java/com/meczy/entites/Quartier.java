package com.meczy.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "quartier", catalog = "MeczyDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Quartier implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private int idquartier;
	private String nomquartier;
	private int etat;

	//departement
	//pays
	@ManyToOne(fetch = FetchType.LAZY , optional = false )
	@JoinColumn(name = "idcommune" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
  // @JsonManagedReference
	private Commune commune;
	//Agence
	@OneToMany( mappedBy = "quartier" ,fetch=FetchType.LAZY)
    @JsonIgnore
	private List<Agence> agences;
	
	public Quartier() {
	}
	
	@JsonCreator
	public Quartier(
			@JsonProperty("idquartier")int idquartier, 
			@JsonProperty("nomquartier") String nomquartier, 
			@JsonProperty("commune") Commune commune, 
			@JsonProperty("etat") int etat,
	        @JsonProperty("agence") List<Agence> agences)
			
     {
		this.idquartier = idquartier;
		this.nomquartier = nomquartier;
		this.commune=commune;
		this.etat = etat;
		this.agences = agences;

	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public int getIdquartier() {
		return idquartier;
	}

	public void setIdquartier(int idquartier) {
		this.idquartier = idquartier;
	}

	public String getNomquartier() {
		return nomquartier;
	}

	public void setNomquartier(String nomquartier) {
		this.nomquartier = nomquartier;
	}

	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}

	public List<Agence> getAgences() {
		return agences;
	}

	public void setAgences(List<Agence> agences) {
		this.agences = agences;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agences == null) ? 0 : agences.hashCode());
		result = prime * result + ((commune == null) ? 0 : commune.hashCode());
		result = prime * result + idquartier;
		result = prime * result + ((nomquartier == null) ? 0 : nomquartier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quartier other = (Quartier) obj;
		if (agences == null) {
			if (other.agences != null)
				return false;
		} else if (!agences.equals(other.agences))
			return false;
		if (commune == null) {
			if (other.commune != null)
				return false;
		} else if (!commune.equals(other.commune))
			return false;
		if (idquartier != other.idquartier)
			return false;
		if (nomquartier == null) {
			if (other.nomquartier != null)
				return false;
		} else if (!nomquartier.equals(other.nomquartier))
			return false;
		return true;
	}
	

		
}
