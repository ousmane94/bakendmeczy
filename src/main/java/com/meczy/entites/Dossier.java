package com.meczy.entites;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@Table(name = "dossier", catalog = "meczyDB"  )
@Data  @ToString
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE) 
@DiscriminatorColumn(name="TYPE_DOC",discriminatorType=DiscriminatorType.STRING,length=2)
public class Dossier implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id  
	private int  iddossier;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date datedemande;
	private long  comptemembre;
	//@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long  numerodemande;
	private int  statutdossier;
	private int  etatdossier;
	// private String naturedossier;
	// private long totalegarantie;
	private String  activite;
	private long montantdemande ;
	private long montantaccorde ;
	private int  tauxinteret ;
	private int  duree ;
	private int  nombreecheance ;
	private long montantremboursement ;
	private int  periodicite ;
	private long epargneexige ;
	private String butcredit ;
	private int typrefontcredit ;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date datedeblocage ;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date dateecheance ;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date dateecheancefinale ;
	// ajout habitude
	private int dejacredit ;
	private String pourquoihabitude ;
	private String parquiHabitude ;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date datehabitude ;
	private long montantHabitude ;
	private int respectengagegements ;
	private long soldecompteepargne ;
	private int tauxinteretannuel ;
	@Temporal(TemporalType.DATE )
    @JsonFormat(pattern = "yyyy-MM-dd")
	private Date datesignature ;
	private String recommandationagent ;
	private int decision ;
	private String motifs ;
	
	//sous secteurcredit
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idsousactivitecredit" , nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private Sousactivite sousactivite;
	
	//sous objet
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idobjectcredit" , nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private Objectcredit objectcredit ;
	
	

	// montant garantie

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "dossier")
	private List<Garantie> garanties;

	//comité
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idcomite" , nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Comite comite;


	// agence
	@ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "idagence" , nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Agence agence;


	// recommandation
	@OneToMany(mappedBy = "dossier" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Recommandation> recommandations;


	public Dossier() {
		// TODO Auto-generated constructor stub
	}


	@JsonCreator
	public Dossier(
			@JsonProperty("iddossier") int iddossier ,
			@JsonProperty("datedemande") Date datedemande ,
			@JsonProperty("statutdossier") int statutdossier,
			@JsonProperty("etatdossier") int etatdossier ,
			@JsonProperty("montant") long montant ,
			@JsonProperty("totalegarantie") long totalegarantie,
			@JsonProperty("numerodemande") long numerodemande,
			@JsonProperty("comptemembre") long comptemembre,
			@JsonProperty("agence") Agence agence,
			@JsonProperty("activite") String activite,
			@JsonProperty("comite") Comite comite,
			@JsonProperty("sousactivite") Sousactivite sousactivite,
			@JsonProperty("montantaccorde") long montantaccorde,
			@JsonProperty("tauxinteret") int tauxinteret,
			@JsonProperty("duree") int duree,
			@JsonProperty("nombreecheance") int nombreecheance,
			@JsonProperty("montantremboursement") long montantremboursement,
			@JsonProperty("periodicite") int periodicite,
			@JsonProperty("epargneexige") long epargneexige,
			@JsonProperty("butcredit") String butcredit,
			@JsonProperty("typrefontcredit") int typrefontcredit,
			@JsonProperty("datedeblocage") Date datedeblocage,
			@JsonProperty("dateecheance") Date dateecheance,
			@JsonProperty("dateecheancefinale") Date dateecheancefinale,
			@JsonProperty("dejacredit") int dejacredit,
			@JsonProperty("pourquoihabitude") String pourquoihabitude,
			@JsonProperty("parquiHabitude") String parquiHabitude,
			@JsonProperty("datehabitude") Date datehabitude,
			@JsonProperty("montantHabitude") long montantHabitude,
			@JsonProperty("montantdemande") long montantdemande,
			@JsonProperty("respectengagegements") int respectengagegements,
			@JsonProperty("soldecompteepargne") long soldecompteepargne,
			@JsonProperty("tauxinteretannuel") int tauxinteretannuel,
			@JsonProperty("datesignature") Date datesignature,
			@JsonProperty("recommandationagent") String recommandationagent,
			@JsonProperty("decision") int decision,
			@JsonProperty("objectcredit") Objectcredit objectcredit,
			@JsonProperty("motifs") String motifs)
			//@JsonProperty("naturepret") List<Naturepret> natureprets)
			
			


	{      
		//this.garanties = garanties;
		this.objectcredit = objectcredit;
		this.motifs = motifs;
		this.decision = decision;
		this.datesignature = datesignature;
		this.tauxinteretannuel = tauxinteretannuel;
		this.soldecompteepargne = soldecompteepargne;
		this.respectengagegements = respectengagegements;
		this.montantHabitude = montantHabitude;
		this.datehabitude = datehabitude;
		this.parquiHabitude = parquiHabitude;
		this.pourquoihabitude = pourquoihabitude;
		this.dateecheancefinale = dateecheancefinale;
		this.dateecheance = dateecheance;

		this.datedeblocage = datedeblocage;
		this.typrefontcredit = typrefontcredit;
		this.butcredit = butcredit;
		this.epargneexige = epargneexige;
		this.periodicite = periodicite;
		this.montantremboursement = montantremboursement;
		this.nombreecheance = nombreecheance;
		this.duree = duree;
		this.tauxinteret = tauxinteret;
		this.montantaccorde = montantaccorde;
		this.datedemande = datedemande;
		this.statutdossier = statutdossier;
		this.etatdossier = etatdossier;
		//this.totalegarantie = totalegarantie;
		this.montantdemande = montantdemande; 
		this.activite = activite;
		this.comptemembre = comptemembre;
		this.numerodemande = numerodemande;
		this.comite = comite;
		this.sousactivite = sousactivite;
		this.agence = agence;
		//this.recommandations = recommandations;

	}


	public long getComptemembre() {
		return comptemembre;
	}


	public Sousactivite getSousactivite() {
		return sousactivite;
	}


	public void setSousactivite(Sousactivite sousactivite) {
		this.sousactivite = sousactivite;
	}


	public Objectcredit getObjectcredit() {
		return objectcredit;
	}


	public void setObjectcredit(Objectcredit objectcredit) {
		this.objectcredit = objectcredit;
	}


	public void setComptemembre(long comptemembre) {
		this.comptemembre = comptemembre;
	}


	public long getNumerodemande() {
		return numerodemande;
	}


	public void setNumerodemande(long numerodemande) {
		this.numerodemande = numerodemande;
	}


	public int getIddossier() {
		return iddossier;
	}

	public void setIddossier(int iddossier) {
		this.iddossier = iddossier;
	}

	public Date getDatedemande() {
		return datedemande;
	}

	public void setDatedemande(Date datedemande) {
		this.datedemande = datedemande;
	}

	public int getStatutdossier() {
		return statutdossier;
	}

	public void setStatutdossier(int statutdossier) {
		this.statutdossier = statutdossier;
	}

	public int getEtatdossier() {
		return etatdossier;
	}

	public void setEtatdossier(int etatdossier) {
		this.etatdossier = etatdossier;
	}



	

	public long getMontantdemande() {
		return montantdemande;
	}


	public void setMontantdemande(long montantdemande) {
		this.montantdemande = montantdemande;
	}


	public String getActivite() {
		return activite;
	}

	public void setActivite(String activite) {
		this.activite = activite;
	}

	public Comite getComite() {
		return comite;
	}

	public void setComite(Comite comite) {
		this.comite = comite;
	}

/*	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}*/

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	public List<Recommandation> getRecommandations() {
		return recommandations;
	}

	public void setRecommandations(List<Recommandation> recommandations) {
		this.recommandations = recommandations;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public long getMontantaccorde() {
		return montantaccorde;
	}


	public void setMontantaccorde(long montantaccorde) {
		this.montantaccorde = montantaccorde;
	}


	public int getTauxinteret() {
		return tauxinteret;
	}


	public void setTauxinteret(int tauxinteret) {
		this.tauxinteret = tauxinteret;
	}


	public int getDuree() {
		return duree;
	}


	public void setDuree(int duree) {
		this.duree = duree;
	}


	public int getNombreecheance() {
		return nombreecheance;
	}


	public void setNombreecheance(int nombreecheance) {
		this.nombreecheance = nombreecheance;
	}


	public long getMontantremboursement() {
		return montantremboursement;
	}


	public void setMontantremboursement(long montantremboursement) {
		this.montantremboursement = montantremboursement;
	}


	public int getPeriodicite() {
		return periodicite;
	}


	public void setPeriodicite(int periodicite) {
		this.periodicite = periodicite;
	}


	public long getEpargneexige() {
		return epargneexige;
	}


	public void setEpargneexige(long epargneexige) {
		this.epargneexige = epargneexige;
	}


	public String getButcredit() {
		return butcredit;
	}


	public void setButcredit(String butcredit) {
		this.butcredit = butcredit;
	}


	public int getTyprefontcredit() {
		return typrefontcredit;
	}


	public void setTyprefontcredit(int typrefontcredit) {
		this.typrefontcredit = typrefontcredit;
	}


	public Date getDatedeblocage() {
		return datedeblocage;
	}


	public void setDatedeblocage(Date datedeblocage) {
		this.datedeblocage = datedeblocage;
	}


	public Date getDateecheance() {
		return dateecheance;
	}


	public void setDateecheance(Date dateecheance) {
		this.dateecheance = dateecheance;
	}


	public Date getDateecheancefinale() {
		return dateecheancefinale;
	}


	public void setDateecheancefinale(Date dateecheancefinale) {
		this.dateecheancefinale = dateecheancefinale;
	}


	public int getDejacredit() {
		return dejacredit;
	}


	public void setDejacredit(int dejacredit) {
		this.dejacredit = dejacredit;
	}


	public String getPourquoihabitude() {
		return pourquoihabitude;
	}


	public void setPourquoihabitude(String pourquoihabitude) {
		this.pourquoihabitude = pourquoihabitude;
	}


	public String getParquiHabitude() {
		return parquiHabitude;
	}


	public void setParquiHabitude(String parquiHabitude) {
		this.parquiHabitude = parquiHabitude;
	}


	public Date getDatehabitude() {
		return datehabitude;
	}


	public void setDatehabitude(Date datehabitude) {
		this.datehabitude = datehabitude;
	}


	public long getMontantHabitude() {
		return montantHabitude;
	}


	public void setMontantHabitude(long montantHabitude) {
		this.montantHabitude = montantHabitude;
	}


	public int getRespectengagegements() {
		return respectengagegements;
	}


	public void setRespectengagegements(int respectengagegements) {
		this.respectengagegements = respectengagegements;
	}


	public long getSoldecompteepargne() {
		return soldecompteepargne;
	}


	public void setSoldecompteepargne(long soldecompteepargne) {
		this.soldecompteepargne = soldecompteepargne;
	}


	public int getTauxinteretannuel() {
		return tauxinteretannuel;
	}


	public void setTauxinteretannuel(int tauxinteretannuel) {
		this.tauxinteretannuel = tauxinteretannuel;
	}


	public Date getDatesignature() {
		return datesignature;
	}


	public void setDatesignature(Date datesignature) {
		this.datesignature = datesignature;
	}


	public String getRecommandationagent() {
		return recommandationagent;
	}


	public void setRecommandationagent(String recommandationagent) {
		this.recommandationagent = recommandationagent;
	}


	public int getDecision() {
		return decision;
	}


	public void setDecision(int decision) {
		this.decision = decision;
	}


	public String getMotifs() {
		return motifs;
	}


	public void setMotifs(String motifs) {
		this.motifs = motifs;
	}


	
	  public List<Garantie> getGaranties() { return garanties; }
	  
	  
	  public void setGaranties(List<Garantie> garanties) { this.garanties =
	  garanties; }
	 







}
