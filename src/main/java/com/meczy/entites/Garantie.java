package com.meczy.entites;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

	
	
@Table(name = "garantie", catalog = "MeczyDB")
@Entity
@Data  @ToString
public class Garantie implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private int idgarantie;
	private String  naturegarantie;
	private String description;
	private long valeurs;
	
	
	 // Dossier 
    @ManyToOne(fetch = FetchType.LAZY , optional = true )
	@JoinColumn(name = "iddossier" , nullable = true) 
    @OnDelete(action =  OnDeleteAction.CASCADE) 
    private Dossier dossier;
    
	 	//justification
	@OneToMany(mappedBy = "garantie" ,fetch=FetchType.LAZY)
	@JsonIgnore
	private List<JustificatifGarantie> justificatifGaranties;
	
	
	@JsonCreator
	public Garantie(
			@JsonProperty("idgarantie") int idgarantie,
			@JsonProperty("naturegarantie") String naturegarantie ,
			@JsonProperty("discription") String discription ,
			@JsonProperty("dossier") Dossier dossier ,
			@JsonProperty("description") String description ,
			@JsonProperty("justificatifGarantie") List<JustificatifGarantie> justificatifGaranties,
			@JsonProperty("valeurs") long valeurs)
	
	{                                                                                                                                                                                                                                                                                                                                                         
		this.idgarantie = idgarantie;
		this.naturegarantie = naturegarantie;
		this.description = description;
		this.valeurs = valeurs;
		this.dossier = dossier;
		this.justificatifGaranties = justificatifGaranties;
		
	}
	
	
	public Garantie() {
		// TODO Auto-generated constructor stub
	}
	
	public int getIdgarantie() {
		return idgarantie;
	}
	public void setIdgarantie(int idgarantie) {
		this.idgarantie = idgarantie;
	}
	public String getNaturegarantie() {
		return naturegarantie;
	}
	public void setNaturegarantie(String naturegarantie) {
		this.naturegarantie = naturegarantie;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getValeurs() {
		return valeurs;
	}
	public void setValeurs(long valeurs) {
		this.valeurs = valeurs;
	}

	

	 
	public Dossier getDossier() {
		return dossier;
	}


	public void setDossier(Dossier dossier) {
		this.dossier = dossier;
	}


	public List<JustificatifGarantie> getJustificatifGaranties() {
		return justificatifGaranties;
	}
	public void setJustificatifGaranties(List<JustificatifGarantie> justificatifGaranties) {
		this.justificatifGaranties = justificatifGaranties;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	


	
	
	
	

}
