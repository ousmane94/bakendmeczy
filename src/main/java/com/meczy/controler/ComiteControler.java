package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Comite;
import com.meczy.service.ComiteService;



@RestController
@CrossOrigin("*")
public class ComiteControler {
	@Autowired
	private ComiteService comiteService;

	// ajouter comite
	@PostMapping("meczy/comite/create")
	public Boolean ajouteragence(@RequestBody Comite comite) {
		try {  
			System.out.println(" ajouter  comite controle ");

			if (comiteService.ajouterComite(comite)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	// modifier comite
	@PutMapping("meczy/comite/update/{idcomite}")
	public ResponseEntity<?> modifiercomite(@PathVariable (value = "idcomite") Integer idcomite, @RequestBody Comite comite) {
		try {

			System.out.println("modification comite controle");	
			Comite lecomite = comiteService.modifierComite(idcomite, comite);
			if(lecomite==null) {
				throw new ResourceNotFoundException("comite " + idcomite+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	//suppression comite
	@DeleteMapping("/meczy/comite/delete/{idcomite}")
	public ResponseEntity<String> supprimercomite(@PathVariable("idcomite") Integer idcomite) {
		try
		{
			System.out.println(" supression  comite controle ");
			if (comiteService.supprimerComite(idcomite)) {
				return new ResponseEntity<>("comite est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression comite", HttpStatus.EXPECTATION_FAILED);

			}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("Echec suppression comite", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//liste comite 
	@GetMapping("/meczy/comite/liste")
	public List<Comite> getcomiteRecherche(){
		try
		{
			System.out.println(" liste  comite controle ");
			List<Comite> mescomite= comiteService.listedesComites();
			if (mescomite!=null) {

				return mescomite;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste comite 
		@GetMapping("/meczy/comite/active/liste")
		public List<Comite> getcomiteActive(){
			try
			{
				System.out.println(" liste  comite controle ");
				List<Comite> mescomite= comiteService.listedesComitesActive();
				if (mescomite!=null) {

					return mescomite;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}

}
