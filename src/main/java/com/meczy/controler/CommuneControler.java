package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Commune;
import com.meczy.service.CommuneService;

@RestController
@CrossOrigin("*")
public class CommuneControler {
	
	@Autowired
	private CommuneService communeService;
	

	    // ajouter Commune
		@PostMapping("meczy/commune/create")
		public Boolean ajouterregion(@RequestBody Commune commune) {
			try {  
				System.out.println(" ajouter  Commune controle ");

				if (communeService.ajouterCommune(commune)){
					return true;
				}
				return false;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return false;
			}
		}


		// modifier Commune
		@PutMapping("meczy/commune/update/{idcommune}")
		public ResponseEntity<?> modifiercommune(@PathVariable (value = "idcommune") Integer idcommune, @RequestBody Commune commune) {
			try {

				System.out.println("modification Commune controle");	
				Commune maCommune = communeService.modifierCommune(idcommune, commune);
				if(maCommune==null) {
					throw new ResourceNotFoundException("Commune " + idcommune+" non trouve");
				}
				else {

					return ResponseEntity.ok().build();			
				}	
			} catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}


		//suppression Commune
		@DeleteMapping("/meczy/commune/delete/{idcommune}")
		public ResponseEntity<String> supprimercommune(@PathVariable("idcommune") Integer idcommune) {
			try
			{
				System.out.println(" supression  Commune controle ");
				if (communeService.supprimerCommune(idcommune)) {
					return new ResponseEntity<>("commune est bien  supprime!", HttpStatus.OK);

				}else {
					return new ResponseEntity<>("Echec suppression commune", HttpStatus.EXPECTATION_FAILED);

				}
			}
			catch(Exception e)
			{
				return new ResponseEntity<>("Echec suppression commune", HttpStatus.EXPECTATION_FAILED);
			}
		}
		//liste commune 
		@GetMapping("/meczy/commune/liste")
		public List<Commune> getcommuneRecherche(){
			try
			{
				System.out.println(" liste  commune controle ");
				List<Commune> mescommune= communeService.listedesCommunes();
				if (mescommune!=null) {

					return mescommune;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
		
		//liste commune active
				@GetMapping("/meczy/commune/active/liste")
				public List<Commune> getcommuneActive(){
					try
					{
						System.out.println(" liste  commune controle ");
						List<Commune> mescommune= communeService.listedesCommunesActive();
						if (mescommune!=null) {

							return mescommune;  
						}
						return null;
					}

					catch(Exception e)
					{
						e.printStackTrace();
						return null;
					}
				}

}
