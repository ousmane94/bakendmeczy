package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Rendezvous;
import com.meczy.service.RendezvousService;

@RestController
@CrossOrigin("*")
public class RendezvousControler {

	@Autowired
	private RendezvousService rendezvousService;

	// ajouter rendezvous
	@PostMapping("meczy/rendezvous/create")
	public Boolean ajouterrendezvous(@RequestBody Rendezvous rendezvous) {
		try {  
			System.out.println(" ajouter  rendezvous controle ");

			if (rendezvousService.ajouterRendezvous(rendezvous)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	// modifier rendezvous
	@PutMapping("meczy/rendezvous/update/{idrendezvous}")
	public ResponseEntity<?> modifierrendezvous(@PathVariable (value = "idrendezvous") Integer idrendezvous, @RequestBody Rendezvous rendezvous) {
		try {

			// System.out.println(rendezvous.getUtilisateur().getIdutilisateur());	
			Rendezvous monrendezvous = rendezvousService.modificationRendezvous(idrendezvous, rendezvous);
			if(monrendezvous==null) {
				throw new ResourceNotFoundException("Commune " + idrendezvous+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	//suppression rendezvous
	@DeleteMapping("/meczy/rendezvous/delete/{idrendezvous}")
	public ResponseEntity<String> supprimeragence(@PathVariable("idrendezvous") Integer idrendezvous) {
		try
		{
			System.out.println(" supression  rendezvous controle ");
			if (rendezvousService.supprimerRendezvous(idrendezvous)) {
				return new ResponseEntity<>("rendezvous est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression rendezvous", HttpStatus.EXPECTATION_FAILED);

			}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("Echec suppression rendezvous", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//liste rendezvous 
	@GetMapping("/meczy/rendezvous/liste")
	public List<Rendezvous> rendezvousListe(){
		try
		{
			System.out.println(" liste  rendezvous controle ");
			List<Rendezvous> mesrendezvous= rendezvousService.listedesRendezvous();
			if (mesrendezvous!=null) {

				return mesrendezvous;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	//liste rendezvous  par agence  listeRendezvousAgentCredit
	@GetMapping("/meczy/rendezvous/liste/agence/{codeagence}")
	public List<Rendezvous> rendezvouspaagence(@PathVariable("codeagence") String codeagence){
		try
		{
			System.out.println(" liste  rendezvouspar agence controle ");
			List<Rendezvous> mesrendezvous= rendezvousService.listeRendezvousAgence(codeagence);
			if (mesrendezvous!=null) {

				return mesrendezvous;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste rendezvous  par agence  
		@GetMapping("/meczy/rendezvous/liste/credit/indiv/{matricule}")
		public List<Rendezvous> rendezvouspaagentCreditindiv(@PathVariable("matricule") String matricule){
			try
			{
				System.out.println(" liste  rendezvouspar  agent credit controle ");
				List<Rendezvous> mesrendezvous= rendezvousService.listeRendezvousAgentCreditindividuel(matricule);
				if (mesrendezvous!=null) {

					return mesrendezvous;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
		
		//liste rendezvous  par agence  
		@GetMapping("/meczy/rendezvous/liste/credit/groupe/{matricule}")
		public List<Rendezvous> rendezvouspaagentCreditgrouper(@PathVariable("matricule") String matricule){
			try
			{
				System.out.println(" liste  rendezvouspar  agent groupement controle ");
				List<Rendezvous> mesrendezvous= rendezvousService.listeRendezvousAgentCreditgroupement(matricule);
				if (mesrendezvous!=null) {

					return mesrendezvous;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}

		
	    //liste rendezvous  par planification
		@GetMapping("/meczy/rendezvous/liste/planification/{idplanification}")
		public List<Rendezvous> rendezvousplanification(@PathVariable("idplanification") Integer idplanification){
			try
			{
				System.out.println(" liste  rendezvous par idplanification controle ");
				List<Rendezvous> mesrendezvous= rendezvousService.listeRendezvousparPlanification(idplanification);
			
				if (mesrendezvous!=null) {

					return mesrendezvous;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
		
		 //liste rendezvous  par par date et par agence
		@GetMapping("/meczy/rendezvous/liste/date/agence/{date}/{codeagence}")
		public List<Rendezvous> rendezvousplanification(@PathVariable("date") String date ,@PathVariable("codeagence") String codeagence ){
			try
			{
				System.out.println(" liste  rendezvous par date et agence controle ");
				List<Rendezvous> mesrendezvous= rendezvousService.listeRendezvousdate(date, codeagence);
			
				if (mesrendezvous!=null) {

					return mesrendezvous;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}




}
