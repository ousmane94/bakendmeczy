package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Planification;
import com.meczy.service.PlanificationService;

@RestController
@CrossOrigin("*")
public class PlanificationControler {

	@Autowired
	private PlanificationService planificationService;

	// ajouter planificationService
	@PostMapping("meczy/planification/create")
	public Boolean ajouterplanification(@RequestBody Planification planification) {
		try {  
			System.out.println(" ajouter  planification controle ");

			if (planificationService.ajouterPlanification(planification)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	// modifier planificationService
	@PutMapping("meczy/planification/update/{idplanification}")
	public ResponseEntity<?> modifierPlanification(@PathVariable (value = "idplanification") Integer idplanification, @RequestBody Planification planification) {
		try {

			System.out.println("modification agence controle");	
			Planification moplanification = planificationService.modifierplanification(idplanification, planification);
			if(moplanification==null) {
				throw new ResourceNotFoundException("Commune " + idplanification+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	//suppression planification
	@DeleteMapping("/meczy/planification/delete/{idplanification}")
	public ResponseEntity<String> supprimerplanification(@PathVariable("idplanification") Integer idplanification) {
		try
		{
			System.out.println(" supression  planification controle ");
			if (planificationService.supprimerPlanification(idplanification)) {
				return new ResponseEntity<>("idplanification est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression idplanification", HttpStatus.EXPECTATION_FAILED);

			}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("Echec suppression idplanification", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//liste agence 
	@GetMapping("/meczy/planification/liste")
	public List<Planification> getidplanificationRecherche(){
		try
		{
			System.out.println(" liste  planification controle ");

			List<Planification> mescommune= planificationService.listedesPlanifications();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste agence 
		@GetMapping("/meczy/planification/active/liste")
		public List<Planification> getplaningeActive(){
			try
			{
				System.out.println(" liste  planification actve controle ");

				List<Planification> mescommune= planificationService.listedesPlanificationsActive();
				if (mescommune!=null) {

					return mescommune;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
	
	//liste planification par agence  
	@GetMapping("/meczy/planification/liste/agence/{codeagence}")
	public List<Planification> getplanificationAgence(@PathVariable("codeagence")String codeagence){
		try
		{
			System.out.println(" liste  planification controle ");

			List<Planification> mescommune= planificationService.listedesPlanificationAgence(codeagence);
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste planification par agence  active
		@GetMapping("/meczy/planification/active/liste/agence/{codeagence}")
		public List<Planification> getplanificationAgenceActive(@PathVariable("codeagence")String codeagence){
			try
			{
				System.out.println(" liste  planification controle ");

				List<Planification> mescommune= planificationService.listedesPlanificationAgenceActive(codeagence);
				if (mescommune!=null) {

					return mescommune;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
	//liste planification par agence et date 
	@GetMapping("/meczy/planification/liste/agence/{codeagence}/{date}")
	public Planification rechercheplanificationAgence(@PathVariable("codagence")String codeagence, @PathVariable("date") String date){
		try
		{
			System.out.println(" liste  planification controle ");

			Planification mescommune= planificationService.recherchedatePlanification(date, codeagence);
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
