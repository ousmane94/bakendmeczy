package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.meczy.entites.Dossierindividuel;
import com.meczy.entites.Utilisateur;
import com.meczy.service.DossierIndivService;

@RestController
@CrossOrigin("*")
public class DossierindivController {
	
	@Autowired
	private DossierIndivService dossierIndivService;
	@PostMapping("meczy/dossier/indiv/create")
	public Boolean ajouterdossierindividuel(@RequestBody Dossierindividuel dossierindividuel) {
		try {  
			System.out.println(" ajouter  dossierindividuel controle ");

			if (dossierIndivService.ajouterDossierindividuel(dossierindividuel)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	// modifier planificationService
	@PutMapping("meczy/dossier/indiv/update/{iddossierindividuel}")
	public ResponseEntity<?> modifierdossierindividuel(@PathVariable (value = "iddossierindividuel") Integer iddossierindividuel, @RequestBody Dossierindividuel dossierindividuel) {
		try {

			System.out.println("Modification dossierindividuel  controle");	
			Dossierindividuel monDossierindividuel = dossierIndivService.updeateDossierindividuel(iddossierindividuel, dossierindividuel);
			if(monDossierindividuel==null) {
				throw new ResourceNotFoundException("Dossier " + iddossierindividuel+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	//suppression iddossierindividuel
	@DeleteMapping("/meczy/dossier/indiv/delete/{iddossierindividuel}")
	public ResponseEntity<String> supprimerdossierindividuel(@PathVariable("iddossierindividuel") Integer iddossierindividuel) {
		try
		{
			System.out.println(" supression  dossierindividuel controle ");
			if (dossierIndivService.deleteDossierindividuel(iddossierindividuel)) {
				return new ResponseEntity<>("iddossierindividuel est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression iddossierindividuel", HttpStatus.EXPECTATION_FAILED);

			}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("Echec suppression iddossierindividuel", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//liste dossierindividuel 
	@GetMapping("/meczy/dossier/indiv/list")
	public List<Dossierindividuel> listdossierindividuel(){
		try
		{
			System.out.println(" liste  dossierindividuel controle ");

			List<Dossierindividuel> mescommune= dossierIndivService.listDossierindividuel();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste dossierindividuel 
	@GetMapping("/meczy/dossier/indiv/list/{matricule}")
	public List<Dossierindividuel> listdossierindividuelMatricule(@PathVariable("matricule") String matricule){
		try
		{
			System.out.println(" liste  dossierindividuel matricule controle ");

			List<Dossierindividuel> mescommune= dossierIndivService.listDossierindividuelparAgent(matricule);
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste dossier  par numero 
	@GetMapping("/meczy/dossier/indiv/{matricule}/{numerodemande}")
	public List<Dossierindividuel> getautilisateuragence(@PathVariable("matricule") String matricule,@PathVariable("numerodemande") long numerodemande){
		try
		{
			System.out.println(" recherche  Dossier par matricule  ");

			List<Dossierindividuel> mesDossierindividuel= dossierIndivService.lectureparNumeroDossierAndutilisateur(numerodemande,matricule);
			if (mesDossierindividuel!=null) {
				return mesDossierindividuel;  
			}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
