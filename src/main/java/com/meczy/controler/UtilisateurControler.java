package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Usercomite;
import com.meczy.entites.Utilisateur;
import com.meczy.service.CompteService;

@RestController
@CrossOrigin("*")
public class UtilisateurControler {


	@Autowired
	private CompteService compteService;
	

	// ajouter utilisateur
	@PostMapping("meczy/utilisateur/create")
	public Boolean ajouteutilisateur(@RequestBody Utilisateur utilisateur) {
		try {  
			System.out.println(" ajouter  utilisateur controle ");

			if (compteService.ajouterUtilisateur(utilisateur)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	// modifier utilisateur
	@PutMapping("meczy/utilisateur/update/{idutilisateur}")
	public ResponseEntity<?> modifierutilisateur(@PathVariable (value = "idutilisateur") Integer idutilisateur, @RequestBody Utilisateur utilisateur) {
		try {

			System.out.println("modification utilisateur controle");
			Utilisateur monutilisateur = compteService.modificationUtilisateur(idutilisateur, utilisateur);
			if(monutilisateur==null) {
				throw new ResourceNotFoundException("Commune " + idutilisateur+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	//suppression utilisateur
	@DeleteMapping("/meczy/utilisateur/delete/{idutilisateur}")
	public ResponseEntity<String> supprimerutilisateur(@PathVariable("idutilisateur") Integer idutilisateur) {
		try
		{
			System.out.println(" supression  utilisateur controle ");
			if (compteService.supprimerUtilisateur(idutilisateur)) {
				return new ResponseEntity<>("utilisateur est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression utilisateur", HttpStatus.EXPECTATION_FAILED);

			}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("Echec suppression utilisateur", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//liste utilisateur 
	@GetMapping("/meczy/utilisateur/liste")
	public List<Utilisateur> getautilisateurListe(){
		try
		{
			System.out.println(" liste  utilisateur controle ");

			List<Utilisateur> mesUtilisateur= compteService.listedesUtilisateurs();
			if (mesUtilisateur!=null) {

				return mesUtilisateur;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	// connexion utilisateur
	@GetMapping("/meczy/utilisateur/{matricule}/{password}")
	public Utilisateur getconnexionUtilisateur(@PathVariable("matricule") String matricule , @PathVariable("password") String password) {
		try {  
			System.out.println(" connexion  utilisateur controle ");
			Utilisateur userUtilisateur = compteService.connexion(matricule, password);

			if (userUtilisateur==null){
				return null;
			}
			return userUtilisateur;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	//liste utilisateur  agence
	@GetMapping("/meczy/utilisateur/agence/{codeagence}")
	public List<Utilisateur> getautilisateuragence(@PathVariable("codeagence") String codeagence){
		try
		{
			System.out.println(" liste  utilisateur par agence controle ");

			List<Utilisateur> mesUtilisateur= compteService.listeUtilisateursAgence(codeagence);
			if (mesUtilisateur!=null) {
				return mesUtilisateur;  
			}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	
	// ajouter comité a  utilisateur
	@PutMapping("/meczy/utilisateur/comite/ajout")
	public boolean ajoutercomiteUtilisateur(@RequestBody Usercomite utilisateur) {
		try {  
			System.out.println(" ajouter comite  utilisateur controle "+utilisateur.getMatricule()+"---"+utilisateur.getIdcomite());
			//Utilisateur userUtilisateur = compteService.connexion(matricule, password);

			if (compteService.ajouterdansComite(utilisateur.getMatricule(), utilisateur.getIdcomite())){
				return true;
			}else {
				return false;

			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	//liste agent de credit par  agence
	@GetMapping("/meczy/utilisateur/agence/credit/{codeagence}")
	public List<Utilisateur> getagentcreditragence(@PathVariable("codeagence") String codeagence){
		try
		{
			System.out.println(" liste  agent de credit agence controle ");

			List<Utilisateur> mesUtilisateur= compteService.listeAgentCreditAgence(codeagence);
			if (mesUtilisateur!=null) {

				return mesUtilisateur;  
			}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	

	//liste agent de credit par  agence
	@GetMapping("/meczy/utilisateur/agent/credit")
	public List<Utilisateur> getagentcredit(){
		try
		{
			System.out.println(" liste  agent de credit agence controle ");

			List<Utilisateur> mesUtilisateur= compteService.listeAgentCredit();
			if (mesUtilisateur!=null) {

				return mesUtilisateur;  
			}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	//liste superviseur par  agence
	@GetMapping("/meczy/utilisateur/agence/superviseur/{codeagence}")
	public List<Utilisateur> getsuperviseuragence(@PathVariable("codeagence") String codeagence){
		try
		{
			System.out.println(" liste superviseur par agence controle ");

			List<Utilisateur> mesUtilisateur= compteService.listeSuperviseurAgence(codeagence);
			if (mesUtilisateur!=null) {

				return mesUtilisateur;  
			}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	// utilisateur  connecté 
	@GetMapping("/meczy/utilisateur/connecte/{matricule}")
	public  Utilisateur getautilisateurconnecte(@PathVariable("matricule") String matricule){
		try
		{
			System.out.println("   utilisateur par matricule controle ");

		  Utilisateur mesUtilisateur= compteService.monUtilisateur(matricule);
			if (mesUtilisateur!=null) {

				return mesUtilisateur;  
			}
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
