package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Activitecredit;
import com.meczy.entites.Objectcredit;
import com.meczy.entites.Secteurcredit;
import com.meczy.entites.Sousactivite;
import com.meczy.entites.Usercomite;
import com.meczy.service.ActivitecreditService;
import com.meczy.service.ObjectcreditService;
import com.meczy.service.SecteurcreditService;
import com.meczy.service.SousactiviteService;

@RestController
@CrossOrigin("*")
public class Butcreditcontroller {

	@Autowired
	private ActivitecreditService activitecreditService;

	@Autowired
	private SecteurcreditService secteurcreditService;
	@Autowired
	private ObjectcreditService objectcreditService;
	@Autowired
	private  SousactiviteService sousactiviteService;


	// ajouter objet
	@PostMapping("meczy/objet/create")
	public Boolean ajouterobjetcredit(@RequestBody Objectcredit objectcredit) {
		try {  
			System.out.println(" ajouter  Objectcredit controle ");

			if (objectcreditService.ajouterObjectcredit(objectcredit)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	// modifier objet
	@PutMapping("meczy/objet/update/{idobjectcredit}")
	public ResponseEntity<?> modifierObjectcredit(@PathVariable (value = "idobjectcredit") Integer idobjectcredit, @RequestBody Objectcredit objectcredit) {
		try {

			System.out.println("modification Objectcredit controle");	
			Objectcredit maCommune = objectcreditService.modifierObjectcredit(idobjectcredit, objectcredit);
			if(maCommune==null) {
				throw new ResourceNotFoundException("idobjectcredit " + idobjectcredit+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	//liste objet active
	@GetMapping("/meczy/objet/active/liste")
	public List<Objectcredit> getObjectcreditActive(){
		try
		{
			System.out.println(" liste  Objectcredit controle ");
			List<Objectcredit> mescommune= objectcreditService.listedesCommunesActive();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}


	//liste objt 
	@GetMapping("/meczy/objet/liste")
	public List<Objectcredit> getObjectcreditRecherche(){
		try
		{
			System.out.println(" liste  objet controle ");
			List<Objectcredit> mescommune= objectcreditService.listedesObjectcredit();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	// ajouter secteur
	@PostMapping("meczy/secteur/create")
	public Boolean ajouterSecteurcredit(@RequestBody Secteurcredit secteurcredit) {
		try {  
			System.out.println(" ajouter  Objectcredit controle ");

			if (secteurcreditService.ajouterSecteurcredit(secteurcredit)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	// modifier idsecteurcredit
	@PutMapping("meczy/secteur/update/{idsecteurcredit}")
	public ResponseEntity<?> modifierSecteurcredit(@PathVariable (value = "idsecteurcredit") Integer idsecteurcredit, @RequestBody Secteurcredit secteurcredit) {
		try {

			System.out.println("modification secteurcredit controle");	
			Secteurcredit maCommune = secteurcreditService.modifierSecteurcredit(idsecteurcredit, secteurcredit);
			if(maCommune==null) {
				throw new ResourceNotFoundException("idsecteurcredit " + idsecteurcredit+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	//liste objet active
	@GetMapping("/meczy/secteur/active/liste")
	public List<Secteurcredit> getSecteurcreditActive(){
		try
		{
			System.out.println(" liste  Objectcredit controle ");
			List<Secteurcredit> mescommune= secteurcreditService.listedessecteurActive();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}


	//liste secteur recherche 
	@GetMapping("/meczy/secteur/liste")
	public List<Secteurcredit> getSecteurcredit(){
		try
		{
			System.out.println(" liste  objet controle ");
			List<Secteurcredit> mescommune= secteurcreditService.listedesSecteurcredit();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	// ajouter Secteur a  objet
	@PutMapping("/meczy/secteur/objet/ajout")
	public boolean ajoutercomiteUtilisateur(@RequestBody Usercomite secteurcredit) {
		try {  
			System.out.println(" secteurcredit objrt   controle "+secteurcredit.getMatricule()+"---"+secteurcredit.getIdcomite());

			if (secteurcreditService.ajouterdansObjectcredit(secteurcredit.getMatricule(), secteurcredit.getIdcomite())){
				return true;
			}else {
				return false;

			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	// ajouter activite
	@PostMapping("meczy/activite/create")
	public Boolean ajouterobj(@RequestBody Activitecredit activitecredit) {
		try {  
			System.out.println(" ajouter  Activitecredit controle ");

			if (activitecreditService.ajouterActivitecredit(activitecredit)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	// modifier activite
	@PutMapping("meczy/activite/update/{idactivitecredit}")
	public ResponseEntity<?> modifierActivitecredit(@PathVariable (value = "idactivitecredit") Integer idactivitecredit, @RequestBody Activitecredit activitecredit) {
		try {

			System.out.println("modification Activitecredit controle");	
			Activitecredit maCommune = activitecreditService.modifierActivitecredit(idactivitecredit, activitecredit);
			if(maCommune==null) {
				throw new ResourceNotFoundException("activitecredit " + idactivitecredit+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	//liste objet activite
	@GetMapping("/meczy/activite/active/liste")
	public List<Activitecredit> getactivitecreditActive(){
		try
		{
			System.out.println(" liste  Activitecredit controle ");
			List<Activitecredit> mescommune= activitecreditService.listedesActivitecreditcredit();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	//liste activite 
	@GetMapping("/meczy/activite/liste")
	public List<Activitecredit> getSecteurcreditRecherche(){
		try
		{
			System.out.println(" liste  activite controle ");
			List<Activitecredit> mescommune= activitecreditService.listedesActivitecredits();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	// ajouter sous activite
	@PostMapping("meczy/sousactivite/create")
	public Boolean ajoutersous(@RequestBody Sousactivite activitecredit) {
		try {  
			System.out.println(" ajouter  sousactivite controle ");

			if (sousactiviteService.ajouterActivitecredit(activitecredit)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	// modifier Sousactivite
	@PutMapping("meczy/sousactivite/update/{idactivitecredit}")
	public ResponseEntity<?> modifiersousActivitecredit(@PathVariable (value = "idactivitecredit") Integer idactivitecredit, @RequestBody Sousactivite activitecredit) {
		try {

			System.out.println("modification Sousactivite controle");	
			Sousactivite maCommune = sousactiviteService.modifierActivitecredit(idactivitecredit, activitecredit);
			if(maCommune==null) {
				throw new ResourceNotFoundException("Sousactivite " + idactivitecredit+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	//liste objet Sousactivite
	@GetMapping("/meczy/sousactivite/active/liste")
	public List<Sousactivite> getsousactivitecreditActive(){
		try
		{
			System.out.println(" liste  Sousactivite controle ");
			List<Sousactivite> mescommune= sousactiviteService.listedesActivitecredits();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	//liste Sousactivite 
	@GetMapping("/meczy/sousactivite/liste")
	public List<Sousactivite> getsousSecteurcreditRecherche(){
		try
		{
			System.out.println(" liste  Sousactivite controle ");
			List<Sousactivite> mescommune= sousactiviteService.listedesActivitecredits();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
