package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Agence;
import com.meczy.service.AgenceService;

@RestController
@CrossOrigin("*")
public class AgenceControler {


	@Autowired
	private AgenceService agenceService;


	// ajouter Commune
	@PostMapping("meczy/agence/create")
	public Boolean ajouteragence(@RequestBody Agence agence) {
		try {  
			System.out.println(" ajouter  agence controle ");

			if (agenceService.ajouterAgence(agence)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	// modifier agence
	@PutMapping("meczy/agence/update/{idagence}")
	public ResponseEntity<?> modifieragence(@PathVariable (value = "idagence") Integer idagence, @RequestBody Agence agence) {
		try {

			System.out.println("modification agence controle");	
			Agence monagence = agenceService.modificationAgence(idagence, agence);
			if(monagence==null) {
				throw new ResourceNotFoundException("Commune " + idagence+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	//suppression agence
	@DeleteMapping("/meczy/agence/delete/{idagence}")
	public ResponseEntity<String> supprimeragence(@PathVariable("idagence") Integer idagence) {
		try
		{
			System.out.println(" supression  agence controle ");
			if (agenceService.supprimerAgence(idagence)) {
				return new ResponseEntity<>("commune est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression agence", HttpStatus.EXPECTATION_FAILED);

			}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("Echec suppression agence", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//liste agence 
	@GetMapping("/meczy/agence/liste")
	public List<Agence> getagenceRecherche(){
		try
		{
			System.out.println(" liste  agence controle ");
			List<Agence> mescommune= agenceService.listedesAgences();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste agence 
	@GetMapping("/meczy/agence/active/liste")
	public List<Agence> getagenceActive(){
		try
		{
			System.out.println(" liste  agence active controle ");
			List<Agence> mescommune= agenceService.listedesAgencesActive();
			if (mescommune!=null) {

				return mescommune;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}



}
