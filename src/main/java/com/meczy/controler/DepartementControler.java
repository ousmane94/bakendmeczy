package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Departement;
import com.meczy.service.DepartementService;

@RestController
@CrossOrigin("*")
public class DepartementControler {
	
	@Autowired
	private DepartementService departementService;
	
	    //ajouter Departement
		@PostMapping("meczy/departement/create")
		public Boolean ajouterregion(@RequestBody Departement departement) {
			try {  
				System.out.println(" ajouter  Departement controle ");

				if (departementService.ajouterDepartement(departement)){
					return true;
				}
				return false;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return false;
			}
		}


		// modifier Departement
		@PutMapping("meczy/departement/update/{iddepartement}")
		public ResponseEntity<?> modifierregion(@PathVariable (value = "iddepartement") Integer iddepartement, @RequestBody Departement departement) {
			try {

				System.out.println("modification region controle");	
				Departement maDepartement = departementService.modifierDepartement(iddepartement, departement);
				if(maDepartement==null) {
					throw new ResourceNotFoundException("Departement " + iddepartement+" non trouve");
				}
				else {

					return ResponseEntity.ok().build();			
				}	
			} catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}


		//suppression Departement
		@DeleteMapping("/meczy/departement/delete/{iddepartement}")
		public ResponseEntity<String> supprimerRegion(@PathVariable("iddepartement") Integer iddepartement) {
			try
			{
				System.out.println(" supression  Departement controle ");
				if (departementService.supprimerDepartement(iddepartement)) {
					return new ResponseEntity<>("Departement est bien  supprime!", HttpStatus.OK);

				}else {
					return new ResponseEntity<>("Echec suppression Departement", HttpStatus.EXPECTATION_FAILED);

				}
			}
			catch(Exception e)
			{
				return new ResponseEntity<>("Echec suppression Departement", HttpStatus.EXPECTATION_FAILED);
			}
		}
		//liste Departement 
		@GetMapping("/meczy/departement/liste")
		public List<Departement> getRegioneRecherche(){
			try
			{
				System.out.println(" liste  Departement controle ");
				List<Departement> mesRegion= departementService.listedesDepartements();
				if (mesRegion!=null) {

					return mesRegion;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
		//liste Departement  active
				@GetMapping("/meczy/departement/active/liste")
				public List<Departement> getDepatementActive(){
					try
					{
						System.out.println(" liste  Departement active controle ");
						List<Departement> mesRegion= departementService.listedesDepartementsActive();
						if (mesRegion!=null) {

							return mesRegion;  
						}
						return null;
					}

					catch(Exception e)
					{
						e.printStackTrace();
						return null;
					}
				}
}
