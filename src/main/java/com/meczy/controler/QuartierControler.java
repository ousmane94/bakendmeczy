package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Quartier;
import com.meczy.service.QuartierService;

@RestController
@CrossOrigin("*")
public class QuartierControler {
	

	@Autowired
	private QuartierService quartierService;


	// ajouter quartier
	@PostMapping("meczy/quartier/create")
	public Boolean ajouterquartier(@RequestBody Quartier quartier) {
		try {  
			System.out.println(" ajouter  quartier controle ");

			if (quartierService.ajouterQuartier(quartier)){
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	// modifier quartier
	@PutMapping("meczy/quartier/update/{idquartier}")
	public ResponseEntity<?> modifierquartier(@PathVariable (value = "idquartier") Integer idquartier, @RequestBody Quartier quartier) {
		try {

			System.out.println("modification quartier controle");	
			Quartier maquartier = quartierService.modifierquartier(idquartier, quartier);
			if(maquartier==null) {
				throw new ResourceNotFoundException("quartier " + idquartier+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	//suppression quartier
	@DeleteMapping("/meczy/quartier/delete/{idquartier}")
	public ResponseEntity<String> supprimerquartier(@PathVariable("idquartier") Integer idquartier) {
		try
		{
			System.out.println(" supression  quartier controle ");
			if (quartierService.supprimerquartier(idquartier)) {
				return new ResponseEntity<>("quartier est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression quartier", HttpStatus.EXPECTATION_FAILED);

			}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("Echec suppression quartier", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//liste quartier  
	@GetMapping("/meczy/quartier/liste")
	public List<Quartier> getquartierRecherche(){
		try
		{
			System.out.println(" liste  quartier controle ");
			List<Quartier> mesquartier= quartierService.listedesquartier();
			if (mesquartier!=null) {

				return mesquartier;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	//liste quartier Active
		@GetMapping("/meczy/quartier/active/liste")
		public List<Quartier> getquartierActive(){
			try
			{
				System.out.println(" liste  quartier controle ");
				List<Quartier> mesquartier= quartierService.listedesquartierActive();
				if (mesquartier!=null) {

					return mesquartier;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}

}
