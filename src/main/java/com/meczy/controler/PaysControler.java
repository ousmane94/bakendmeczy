package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Pays;
import com.meczy.service.PaysService;



@RestController
@CrossOrigin("*")
public class PaysControler {

	@Autowired
	private PaysService paysService;


	//ajouter pays
	@PostMapping("meczy/pays/create")
	public Boolean ajouterpays(@RequestBody Pays pays) {
		try {  
			System.out.println(" ajouter  pays controle ");

			if (paysService.ajouterPays(pays)) {
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	// modifier pays
	@PutMapping("meczy/pays/update/{idPays}")
	public ResponseEntity<?> modifierpays(@PathVariable (value = "idPays") Integer idPays, @RequestBody Pays pays) {
		try {

			System.out.println("modification pays controle");		
			Pays monpays= paysService.modifierPays(idPays, pays);
			if(monpays==null) {
				throw new ResourceNotFoundException("pays " + idPays+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	//suppression pays
	@DeleteMapping("/meczy/pays/delete/{idpays}")
	public ResponseEntity<String> supprimerPays(@PathVariable("idpays") Integer idpays) {
		try
		{
			System.out.println(" supression  pays controle ");
			if (paysService.supprimerPays(idpays)) {
				return new ResponseEntity<>("Salle est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression salle", HttpStatus.EXPECTATION_FAILED);

			}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("Echec suppression salle", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//liste pays 
	@GetMapping("/meczy/pays/liste")
	public List<Pays> getPaysRecherche(){
		try
		{
			System.out.println(" liste  pays controle ");
			List<Pays> mespays= paysService.listedesPays();
			if (mespays!=null) {

				return mespays;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	@GetMapping("/meczy/pays/active/liste")
	public List<Pays> getPaysActive(){
		try
		{
			System.out.println(" liste  pays Active controle ");
			List<Pays> mespays= paysService.listedesPaysActive();
			if (mespays!=null) {

				return mespays;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste pays 
		@GetMapping("/meczy/pays/id/{idpays}")
		public Pays getpaysid(@PathVariable("idpays") Integer idpays){
			try
			{
				System.out.println("   pays controle  idpays");
				Pays mespays= paysService.rechercheId(idpays);
				if (mespays!=null) {

					return mespays;  
				}
				return null;
			}

			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}

	//
	//

}
