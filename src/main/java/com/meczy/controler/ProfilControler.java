package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Profils;
import com.meczy.entites.Utilisateur;
import com.meczy.service.ProfilsService;

@RestController
@CrossOrigin("*")
public class ProfilControler {
	
	
	@Autowired
	private ProfilsService ProfilsService;
	
	//liste utilisateur 
	@GetMapping("/meczy/profils/liste")
	public List<Profils> getprofilsListe(){
		try
		{
			System.out.println(" liste  profils controle ");

			List<Profils> mesUtilisateur= ProfilsService.listedeProfils();
			if (mesUtilisateur!=null) {

				return mesUtilisateur;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}


}
