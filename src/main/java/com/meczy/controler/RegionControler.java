package com.meczy.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meczy.entites.Region;
import com.meczy.service.RegionService;



@RestController
@CrossOrigin("*")
public class RegionControler {
	
	@Autowired
	private RegionService regionService;


	//ajouter region
	@PostMapping("meczy/region/create")
	public Boolean ajouterregion(@RequestBody Region region) {
		try {  
			
			System.out.println(" ajouter  Region controle ");

			if (regionService.ajouterPays(region)) {
				return true;
			}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	// modifier region
	@PutMapping("meczy/region/update/{idregion}")
	public ResponseEntity<?> modifierregion(@PathVariable (value = "idregion") Integer idregion, @RequestBody Region region) {
		try {

			System.out.println("modification region controle");	
			Region maRegion = regionService.modifierRegion(idregion, region);
			if(maRegion==null) {
				throw new ResourceNotFoundException("pays " + idregion+" non trouve");
			}
			else {

				return ResponseEntity.ok().build();			
			}	
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	//suppression region
	@DeleteMapping("/meczy/region/delete/{idregion}")
	public ResponseEntity<String> supprimerRegion(@PathVariable("idregion") Integer idregion) {
		try
		{
			System.out.println(" supression  region controle ");
			if (regionService.supprimerRegion(idregion)) {
				return new ResponseEntity<>("region est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression region", HttpStatus.EXPECTATION_FAILED);

			}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("Echec suppression region", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//liste region 
	@GetMapping("/meczy/region/liste")
	public List<Region> getRegioneRecherche(){
		try
		{
			System.out.println(" liste  region controle ");
			List<Region> mesRegion= regionService.listedesRegions();
			if (mesRegion!=null) {

				return mesRegion;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste region 
	@GetMapping("/meczy/region/active/liste")
	public List<Region> getRegioneActive(){
		try
		{
			System.out.println(" liste  region controle ");
			List<Region> mesRegion= regionService.listedesRegionsActive();
			if (mesRegion!=null) {

				return mesRegion;  
			}
			return null;
		}

		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}



}
