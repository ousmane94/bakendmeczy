package com.meczy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.meczy.entites.Activitecredit;
import com.meczy.entites.Agence;
import com.meczy.entites.Comite;
import com.meczy.entites.Commune;
import com.meczy.entites.Departement;
import com.meczy.entites.Dossier;
import com.meczy.entites.Dossierindividuel;
import com.meczy.entites.Objectcredit;
import com.meczy.entites.Pays;
import com.meczy.entites.Planification;
import com.meczy.entites.Profils;
import com.meczy.entites.Quartier;
import com.meczy.entites.Region;
import com.meczy.entites.Rendezvous;
import com.meczy.entites.Secteurcredit;
import com.meczy.entites.Utilisateur;

@SpringBootApplication
public class MeczysafApplication implements CommandLineRunner  {

	@Autowired
	private RepositoryRestConfiguration restConfiguration;
	
	
	@Bean
	public BCryptPasswordEncoder getBCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(MeczysafApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		// affiche de l'id dans json
		 restConfiguration.exposeIdsFor(
				 Utilisateur.class,
				 Comite.class,
				 Agence.class,
				 Commune.class,
				 Departement.class,
				 Pays.class,
				 Planification.class,
				 Profils.class,
				 Quartier.class,
				 Region.class,
				 Dossier.class,
				 Dossierindividuel.class,
				 Rendezvous.class,
				 Objectcredit.class,
				 Secteurcredit.class,
				 Activitecredit.class
				 );	
		
	}

}
