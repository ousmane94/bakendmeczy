package com.meczy.serviceImpl;

import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.AgenceRepository;
import com.meczy.dao.ComiteRepository;
import com.meczy.dao.ProfilsRepository;
import com.meczy.dao.UtilisateurRepository;
import com.meczy.entites.Agence;
import com.meczy.entites.Comite;
import com.meczy.entites.Profils;
import com.meczy.entites.Utilisateur;
import com.meczy.service.CompteService;

@Service
@Transactional
public class CompteServiceImpl implements CompteService {
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private BCryptPasswordEncoder  bCryptPasswordEncoder;
	@Autowired
	private ProfilsRepository profilsRepository;
	@Autowired
	private AgenceRepository agenceRepository;
	@Autowired
	private  ComiteRepository comiteRepository;
	

	@Override
	public boolean ajouterUtilisateur(Utilisateur utilisateur) {
		try {
			
		Utilisateur user1 = utilisateurRepository.findByMatricule(utilisateur.getMatricule());
		if(user1!=null) 
			return false;
		Utilisateur user2 = new Utilisateur();
		String  iduser1=""+((int) (Math.random()*(9-1)))
				+((int) (Math.random()*(9-1)));
        Integer uss1=java.lang.Integer.valueOf(iduser1);
		user2.setPrenom(utilisateur.getPrenom());
		user2.setEmail(utilisateur.getEmail());
		user2.setNom(utilisateur.getNom());
		user2.setPassword(bCryptPasswordEncoder.encode(utilisateur.getPassword()));
		user2.setTelephone(utilisateur.getTelephone());
		user2.setProfils(utilisateur.getProfils());
		user2.setProfils(utilisateur.getProfils());
		user2.setAgence(utilisateur.getAgence());
		user2.setMatricule(utilisateur.getMatricule());
		user2.setComites(utilisateur.getComites());
		
		user2.setIdutilisateur(uss1);
		user2.setProfils(utilisateur.getProfils());
		user2.setEtat(1); //desactivé
		utilisateurRepository.save(user2);
		return true;
		
	}catch(Exception e)
	{
		e.printStackTrace();
		return false;
	}
	}
	@Override
	public Utilisateur lectureByMatricule(String matricule) {
		try {
			System.out.println("recherche  Utilisateur service...");
			Utilisateur listQuartier = utilisateurRepository.findByMatricule(matricule);
			if(listQuartier != null) 
			  return listQuartier;
			return null;		  

		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public boolean activerutilisteur(String matricule) {
		try {
			System.out.println("activation   Utilisateur  service...");
			Utilisateur monUtilisateur = utilisateurRepository.findByMatricule(matricule);
			if (monUtilisateur.getEtat()==1) {
				monUtilisateur.setEtat(0);
				utilisateurRepository.save(monUtilisateur);
				return false;

				
			}else {
				monUtilisateur.setEtat(1);
				utilisateurRepository.save(monUtilisateur);
				return true;

			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	@Override
	public Utilisateur modificationUtilisateur(int idutilisateur, Utilisateur utilisateur) {
		try {
			System.out.println("modifier   Utilisateur  service...");
			Utilisateur monUtilisateur = utilisateurRepository.findByIdutilisateur(idutilisateur);
			monUtilisateur.setAgence(utilisateur.getAgence());
			monUtilisateur.setEmail(utilisateur.getEmail());
			monUtilisateur.setEtat(utilisateur.getEtat());
			monUtilisateur.setMatricule(utilisateur.getMatricule());
			monUtilisateur.setNom(utilisateur.getNom());
			if (utilisateur.getPassword()==null) {
				monUtilisateur.setPrenom(utilisateur.getPrenom());
				monUtilisateur.setProfils(utilisateur.getProfils());
				monUtilisateur.setTelephone(utilisateur.getTelephone());
				return utilisateurRepository.save(monUtilisateur);
			}else {
				monUtilisateur.setPassword(bCryptPasswordEncoder.encode(utilisateur.getPassword()));
				monUtilisateur.setPrenom(utilisateur.getPrenom());
				monUtilisateur.setProfils(utilisateur.getProfils());
				monUtilisateur.setTelephone(utilisateur.getTelephone());
				return utilisateurRepository.save(monUtilisateur);
			}

			

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public boolean supprimerUtilisateur(int idutilisateur) {
		try {
			Utilisateur utilisateur = utilisateurRepository.findByIdutilisateur(idutilisateur);
			
			if (utilisateur!=null) {
			
				utilisateurRepository.deleteById(idutilisateur);
				return  true;

			}else {
				return  false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}		
		
	}
	@Override
	public List<Utilisateur> listedesUtilisateurs() {
		try {
			System.out.println("liste   Utilisateur  service...");
			List<Utilisateur> listQuartier= new ArrayList<Utilisateur>();
			List<Utilisateur> listereturnlistQuartier=new ArrayList<Utilisateur>();
			utilisateurRepository.mesUtilisateurparordre().forEach(listQuartier::add);
			Iterator<Utilisateur> it=listQuartier.iterator();
			while(it.hasNext())
			{
				Utilisateur patQuartier=it.next();
				Utilisateur monQuartier=new Utilisateur();
				monQuartier.setIdutilisateur(patQuartier.getIdutilisateur());
				monQuartier.setEtat(patQuartier.getEtat());
				monQuartier.setEmail(patQuartier.getEmail());
				monQuartier.setMatricule(patQuartier.getMatricule());
				monQuartier.setPrenom(patQuartier.getPrenom());
				monQuartier.setNom(patQuartier.getNom());
				//monQuartier.setPassword(bCryptPasswordEncoder.));
				monQuartier.setTelephone(patQuartier.getTelephone());
				monQuartier.setComites(null);
				if (patQuartier.getAgence()==null) {
					monQuartier.setAgence(null);
				}
					else {
						
						Agence  utilisateurAgence   =  agenceRepository.findByCodeagence(patQuartier.getAgence().getCodeagence());
						if (utilisateurAgence!=null) {
							utilisateurAgence.setUtilisateurs(null);
							utilisateurAgence.setPlanifications(null);
							utilisateurAgence.setComite(null);
							monQuartier.setAgence(utilisateurAgence);
						}else {
							monQuartier.setAgence(null);

						}
					}
				
				Profils profils = profilsRepository.findByIdprofils(patQuartier.getProfils().getIdprofils());
					if (profils!=null) {
						profils.setUtilisateurs(null);
						monQuartier.setProfils(profils);
										
					}
				listereturnlistQuartier.add(monQuartier);
			}

			return listereturnlistQuartier;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public List<Utilisateur> listeUtilisateursAgence(String codeagence) {
		try {
			System.out.println("liste   Utilisateur agence service...");
			Agence agence = agenceRepository.findByCodeagence(codeagence);
			List<Utilisateur> listereturnlistQuartier=new ArrayList<Utilisateur>();

			if (agence==null)
				return null;
			List<Utilisateur> listQuartier = utilisateurRepository.findByAgence(agence);
			Iterator<Utilisateur> it=listQuartier.iterator();
			while(it.hasNext())
			{
				Utilisateur patQuartier=it.next();
				Utilisateur monQuartier=patQuartier;
				monQuartier.setComites(null);
				listereturnlistQuartier.add(monQuartier);
			}

			return listereturnlistQuartier;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Utilisateur monUtilisateur(String matricule) {
		try {
			System.out.println(" Utilisateur matricule service...");
			Utilisateur listQuartier = utilisateurRepository.findByMatricule(matricule);
			if(listQuartier==null) 
				return null;
			Utilisateur monQuartier=new Utilisateur();
			monQuartier.setEtat(listQuartier.getEtat());
			monQuartier.setEmail(listQuartier.getEmail());
			monQuartier.setMatricule(listQuartier.getMatricule());
			monQuartier.setPrenom(listQuartier.getPrenom());
			monQuartier.setIdutilisateur(listQuartier.getIdutilisateur());
			monQuartier.setNom(listQuartier.getNom());
			monQuartier.setTelephone(listQuartier.getTelephone());
			monQuartier.setComites(null);
			if (listQuartier.getAgence()==null) {
				monQuartier.setAgence(null);
			}
				else {
					
					Agence  utilisateurAgence   =  agenceRepository.findByCodeagence(listQuartier.getAgence().getCodeagence());
					if (utilisateurAgence!=null) {
						utilisateurAgence.setUtilisateurs(null);
						utilisateurAgence.setPlanifications(null);
						utilisateurAgence.setComite(null);
					    //utilisateurAgence.setQuartier(null);
						monQuartier.setAgence(utilisateurAgence);
					}else {
						monQuartier.setAgence(null);

					}
				}
			
			Profils profils = profilsRepository.findByIdprofils(listQuartier.getProfils().getIdprofils());
				if (profils!=null) {
					profils.setUtilisateurs(null);
					monQuartier.setProfils(profils);
									
				}
			return monQuartier;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	
	@Override
	public Utilisateur connexion(String matricule, String password) {

		Utilisateur utilisateur  = utilisateurRepository.findByMatricule(matricule);
		if (matricule.equalsIgnoreCase(utilisateur.getMatricule()) && bCryptPasswordEncoder.matches(password, utilisateur.getPassword())) {
			System.out.println("utilisateur bien identifier ");
			utilisateur.setComites(null);
			/// utilisateur.setAgence(null);
			return utilisateur;
		}else {
			return null;
		}
	}
	@Override
	public boolean  ajouterdansComite(String matricule, int idcomite) {
		try {
			System.out.println(" controle  utilisateur controle "+matricule);
			Utilisateur utilisateur = utilisateurRepository.findByMatricule(matricule);
			Comite usercomite = comiteRepository.findByIdcomite(idcomite);
			System.out.println(" controle  utilisateur controle "+idcomite);


			if (utilisateur !=null || usercomite !=null ) {
				System.out.println(" ajouter bon   utilisateur comite -bon--");
				List<Comite> mescomites = new ArrayList<>();
				mescomites.add(usercomite);
				mescomites.addAll(utilisateur.getComites());
				utilisateur.setComites(mescomites);
				System.out.println(" savegarde  -bon--");

				utilisateurRepository.save(utilisateur);
				return true;	

			}else {

			return false;
			}

		} catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	@Override
	public List<Utilisateur> listeAgentCreditAgence(String codeagence) {
		try {
			System.out.println("liste   Utilisateur agent de credit agence service...");
			List<Utilisateur> listereturnlistQuartier=new ArrayList<Utilisateur>();	
			List<Utilisateur> listQuartier = utilisateurRepository.listetilisateurAgentcreditAgence(codeagence);
			if (listQuartier==null)
				return null;
			Iterator<Utilisateur> it=listQuartier.iterator();
			while(it.hasNext())
			{
				Utilisateur patQuartier=it.next();
				Utilisateur monQuartier=patQuartier;
				monQuartier.setComites(null);
				listereturnlistQuartier.add(monQuartier);
			}
			return listereturnlistQuartier;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Utilisateur> listeAgentCredit() {
		try {
			System.out.println("liste   Utilisateur agent de credit service...");
			List<Utilisateur> listereturnlistQuartier=new ArrayList<Utilisateur>();	
			List<Utilisateur> listQuartier = utilisateurRepository.listetilisateurAgentcredit();
			if (listQuartier==null)
				return null;
			Iterator<Utilisateur> it=listQuartier.iterator();
			while(it.hasNext())
			{
				Utilisateur patQuartier=it.next();
				Utilisateur monQuartier=patQuartier;
				monQuartier.setComites(null);
				listereturnlistQuartier.add(monQuartier);
			}
			return listereturnlistQuartier;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public List<Utilisateur> listeSuperviseurAgence(String codeagence) {
		try {
			System.out.println("liste   Utilisateur agence Superviseur service...");
			List<Utilisateur> listereturnlistQuartier=new ArrayList<Utilisateur>();	
			List<Utilisateur> listQuartier = utilisateurRepository.listetilisateurSuperviseurAgence(codeagence);
			if (listQuartier==null)
				return null;
			Iterator<Utilisateur> it=listQuartier.iterator();
			while(it.hasNext())
			{
				Utilisateur patQuartier=it.next();
				Utilisateur monQuartier=patQuartier;
				monQuartier.setComites(null);
				listereturnlistQuartier.add(monQuartier);
			}
			return listereturnlistQuartier;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}

