package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.CommuneRepository;
import com.meczy.dao.DepartementRepository;
import com.meczy.dao.PaysRepository;
import com.meczy.dao.QuartierRepository;
import com.meczy.entites.Commune;
import com.meczy.entites.Departement;
import com.meczy.entites.Pays;
import com.meczy.entites.Quartier;
import com.meczy.entites.Region;
import com.meczy.service.CommuneService;
import com.meczy.service.QuartierService;

@Service
@Transactional
public class CommuneServiceImpl implements CommuneService {
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private DepartementRepository departementRepository;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private QuartierRepository quartierRepository;
	@Autowired 
	private QuartierService quartierService;

	@Override
	public boolean ajouterCommune(Commune commune) {
	
		try {

			System.out.println("creation  Commune service...");
			String  idcommune=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idcommune);
			commune.setIdcommune(id);
			Commune communerecherche = communeRepository.findCommuneDepatement(commune.getNomcommune(),commune.getDepartement().getNomdepartement());
			if (communerecherche==null) {
				communeRepository.save(commune);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Commune rechercheNomCommune(String nomcommune) {
		try {
			System.out.println("recherche  Commune service...");
			Commune commune = communeRepository.findByNomcommune(nomcommune);
			if(commune==null) throw new RuntimeException("Commune introuvable ");
			return commune;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Commune modifierCommune(int idcommune, Commune commune) {
		try {
			System.out.println("modifier   Commune  service...");
			Commune maCommune = communeRepository.findByIdcommune(idcommune);
			maCommune.setNomcommune(commune.getNomcommune());
			maCommune.setDepartement(commune.getDepartement());
			maCommune.setEtat(commune.getEtat());
			return communeRepository.save(maCommune);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerCommune(int idcommune) {
		try {
			Commune commune =communeRepository.findByIdcommune(idcommune);

			if (commune!=null) {
				//liste des service
				System.out.println("modification des agences  supression   commune .....");
				List<Quartier>  mesQuartiersliste = quartierRepository.findByCommune(commune);
				Iterator<Quartier> it=mesQuartiersliste.iterator();
				while(it.hasNext())
				{
					Quartier delquartier=it.next();
					quartierService.supprimerquartier(delquartier.getIdquartier());
				}

				communeRepository.deleteById(idcommune);
				return  true;

			}else {
				return  false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public List<Commune> listedesCommunes() {
		try {
			System.out.println("liste   Commune  service...");
			List<Commune> listCommune= new ArrayList<Commune>();
			List<Commune> listereturnCommune=new ArrayList<Commune>();
			communeRepository.mesCommuneparordre().forEach(listCommune::add);
			Iterator<Commune> it=listCommune.iterator();
			while(it.hasNext())
			{
				Commune patCommune=it.next();
				Commune maCommune=new Commune();
				maCommune.setIdcommune(patCommune.getIdcommune());
				maCommune.setNomcommune(patCommune.getNomcommune());
				maCommune.setEtat(patCommune.getEtat());
				maCommune.setQuartiers(null);
				//maCommune.setDepartement(patCommune.getDepartement());
				Departement regionPays   = departementRepository.findByIddepartement(patCommune.getDepartement().getIddepartement());
				if (regionPays!=null) {
					regionPays.setCommunes(null);
					maCommune.setDepartement(regionPays);
				}
			

				listereturnCommune.add(maCommune);
			}

			return listereturnCommune;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Commune> listedesCommunesActive() {
		try {
			System.out.println("liste   Commune  service...");
			List<Commune> listCommune= new ArrayList<Commune>();
			List<Commune> listereturnCommune=new ArrayList<Commune>();
			communeRepository.mesCommuneparordreActive().forEach(listCommune::add);
			Iterator<Commune> it=listCommune.iterator();
			while(it.hasNext())
			{
				Commune patCommune=it.next();
				Commune maCommune=new Commune();
				maCommune.setIdcommune(patCommune.getIdcommune());
				maCommune.setNomcommune(patCommune.getNomcommune());
				maCommune.setEtat(patCommune.getEtat());
				maCommune.setQuartiers(null);
				maCommune.setDepartement(patCommune.getDepartement());

				listereturnCommune.add(maCommune);
			}

			return listereturnCommune;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
