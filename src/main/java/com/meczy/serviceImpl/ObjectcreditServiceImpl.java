package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.ObjectcreditRepository;
import com.meczy.entites.Objectcredit;
import com.meczy.service.ObjectcreditService;

@Service
@Transactional
public class ObjectcreditServiceImpl implements ObjectcreditService {
	
	@Autowired
	private ObjectcreditRepository objectcreditRepository;


	@Override
	public boolean ajouterObjectcredit(Objectcredit objectcredit) {
		try {

			System.out.println("creation  objectcredit service...");
			String  idcomite=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idcomite);
			objectcredit.setIdobjectcredit(id);
			Objectcredit monobjectcredit = objectcreditRepository.findByDescription(objectcredit.getDescription());
			if (monobjectcredit==null) {
				objectcreditRepository.save(objectcredit);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}



	@Override
	public Objectcredit rechercheObjectcredit(String nomcomite) {
		try {
			System.out.println("recherche  Description Objectcredit service...");
			Objectcredit comite = objectcreditRepository.findByDescription(nomcomite);
			
			if(comite==null) throw new RuntimeException("Objectcredit introuvable ");
			return comite;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Objectcredit modifierObjectcredit(int idobjectcredit, Objectcredit objectcredit) {
		try {
			System.out.println("modifier   objectcredit  service...");


			Objectcredit monobjectcredit = objectcreditRepository.findByIdobjectcredit(idobjectcredit);
			if(monobjectcredit==null) 
				return null;
			monobjectcredit.setDescription(objectcredit.getDescription());
			monobjectcredit.setEtat(objectcredit.getEtat());
			return objectcreditRepository.save(monobjectcredit);


		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerObjectcredit(int idobjectcredit) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Objectcredit> listedesObjectcredit() {
		try {
			System.out.println("liste   Comite  service...");
			List<Objectcredit> listComite= new ArrayList<Objectcredit>();
			List<Objectcredit> listereturnlistComite=new ArrayList<Objectcredit>();
			objectcreditRepository.findAll().forEach(listComite::add);
			Iterator<Objectcredit> it=listComite.iterator();
			while(it.hasNext())
			{
				Objectcredit patComite=it.next();
				patComite.setSecteurcredits(null);
				patComite.setDossiers(null);
			    listereturnlistComite.add(patComite);
			}

			return listereturnlistComite;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}



	@Override
	public List<Objectcredit> listedesCommunesActive() {
		try {
			System.out.println("liste   Comite  service...");
			List<Objectcredit> listComite= new ArrayList<Objectcredit>();
			List<Objectcredit> listereturnlistComite=new ArrayList<Objectcredit>();
			objectcreditRepository.getObjectcreditActive().forEach(listComite::add);
			Iterator<Objectcredit> it=listComite.iterator();
			while(it.hasNext())
			{
				Objectcredit patComite=it.next();
				patComite.setSecteurcredits(null);
				patComite.setDossiers(null);
			    listereturnlistComite.add(patComite);
			}

			return listereturnlistComite;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
