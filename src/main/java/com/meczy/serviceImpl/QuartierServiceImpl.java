package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.AgenceRepository;
import com.meczy.dao.CommuneRepository;
import com.meczy.dao.DepartementRepository;
import com.meczy.dao.PaysRepository;
import com.meczy.dao.QuartierRepository;
import com.meczy.entites.Agence;
import com.meczy.entites.Commune;
import com.meczy.entites.Departement;
import com.meczy.entites.Pays;
import com.meczy.entites.Quartier;
import com.meczy.entites.Region;
import com.meczy.service.QuartierService;


@Service
@Transactional
public class QuartierServiceImpl implements QuartierService{
	
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private DepartementRepository departementRepository;
	@Autowired 
	private QuartierRepository quartierRepository;
	@Autowired
	private PaysRepository paysRepository;
	@Autowired
	private AgenceRepository agenceRepository;
	

	@Override
	public boolean ajouterQuartier(Quartier quartier) {
		try {

			System.out.println("creation  Quartier service...");
			String  idQuartier=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idQuartier);
			quartier.setIdquartier(id);
			Quartier quartierrecherche = quartierRepository.findCommuneQuartier(quartier.getNomquartier(), quartier.getCommune().getNomcommune());
			if (quartierrecherche==null) {
				quartierRepository.save(quartier);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Quartier rechercheNomQuartier(String nomquartier) {
		try {
			System.out.println("recherche  Quartier service...");
			Quartier quartier = quartierRepository.findByNomquartier(nomquartier);
			if(quartier==null) throw new RuntimeException("Quartier introuvable ");
			return quartier;		  
			}catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
	}

	@Override
	public Quartier modifierquartier(int idquartier, Quartier quartier) {
		try {
			System.out.println("modifier   Quartier  service...");
			Quartier maQuartier = quartierRepository.findByIdquartier(idquartier);
			maQuartier.setNomquartier(quartier.getNomquartier());
			maQuartier.setCommune(quartier.getCommune());
			maQuartier.setEtat(quartier.getEtat());
			return quartierRepository.save(maQuartier);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerquartier(int idquartier) {
		try {
			Quartier quartier =quartierRepository.findByIdquartier(idquartier);

			if (quartier!=null) {
				//liste des service
				System.out.println("modification des agences  supression   quartier .....");
				List<Agence>  mesagencesList = agenceRepository.findByQuartier(quartier);
				Iterator<Agence> it=mesagencesList.iterator();
				while(it.hasNext())
				{
					Agence delagence=it.next();
					delagence.setQuartier(null);
					agenceRepository.save(delagence);
				}

				quartierRepository.deleteById(idquartier);
				return  true;

			}else {
				return  false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Quartier> listedesquartier() {
		try {
			System.out.println("liste   Quartier  service...");
			List<Quartier> listQuartier= new ArrayList<Quartier>();
			List<Quartier> listereturnlistQuartier=new ArrayList<Quartier>();
			quartierRepository.mesQuartierparordre().forEach(listQuartier::add);
			Iterator<Quartier> it=listQuartier.iterator();
			while(it.hasNext())
			{
				Quartier patQuartier=it.next();
				Quartier monQuartier=new Quartier();
				monQuartier.setIdquartier(patQuartier.getIdquartier());
				monQuartier.setNomquartier(patQuartier.getNomquartier());
				monQuartier.setAgences(null);
				monQuartier.setEtat(patQuartier.getEtat());
				monQuartier.setCommune(patQuartier.getCommune());
				
				listereturnlistQuartier.add(monQuartier);
			}

			return listereturnlistQuartier;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Quartier> listedesquartierActive() {
		try {
			System.out.println("liste   Quartier  service...");
			List<Quartier> listQuartier= new ArrayList<Quartier>();
			List<Quartier> listereturnlistQuartier=new ArrayList<Quartier>();
			quartierRepository.mesQuartierparordreActive().forEach(listQuartier::add);
			Iterator<Quartier> it=listQuartier.iterator();
			while(it.hasNext())
			{
				Quartier patQuartier=it.next();
				Quartier monQuartier=new Quartier();
				monQuartier.setIdquartier(patQuartier.getIdquartier());
				monQuartier.setNomquartier(patQuartier.getNomquartier());
				monQuartier.setAgences(null);
				monQuartier.setEtat(patQuartier.getEtat());
				monQuartier.setCommune(patQuartier.getCommune());
				
				listereturnlistQuartier.add(monQuartier);
			}

			return listereturnlistQuartier;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
