package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.DepartementRepository;
import com.meczy.dao.PaysRepository;
import com.meczy.dao.RegionRepository;
import com.meczy.entites.Commune;
import com.meczy.entites.Departement;
import com.meczy.entites.Pays;
import com.meczy.entites.Region;
import com.meczy.service.DepartementService;
import com.meczy.service.PaysService;
import com.meczy.service.RegionService;

@Service
@Transactional
public class RegionServiceImpl  implements RegionService{

	
	@Autowired
	private RegionRepository regionRepository;
	@Autowired 
	private PaysRepository paysRepository;
	@Autowired 
	private PaysService paysService;
	@Autowired
	private DepartementService departementService;
	@Autowired
	private DepartementRepository departementRepository;
	
	// ajouter region 
	@Override
	public boolean ajouterPays(Region region) {

		try {

			System.out.println("creation  Region service...");
			String  idpays=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idpays);
			region.setIdregion(id);
			System.out.println("etat   Region"+region.getEtat());
	

			Region regionrecherche = regionRepository.findByNomregion(region.getNomregion());
			if (regionrecherche==null) {
				regionRepository.save(region);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	// Recherche nom region 
	@Override
	public Region rechercheNomRegion(String nomregion) {
		try {
			System.out.println("recherche  Region service...");
			Region region = regionRepository.findByNomregion(nomregion);
			if(region==null) throw new RuntimeException("Region introuvable ");
			return region;		  
			}catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
	}
    // modifier région
	@Override
	public Region modifierRegion(int idregion, Region region) {
	try {
			System.out.println("modifier   Région  service...");
			Region monRegion = regionRepository.findByIdregion(idregion);
			monRegion.setNomregion(region.getNomregion());
			monRegion.setEtat(region.getEtat());
			monRegion.setPays(region.getPays());
			return regionRepository.save(monRegion);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
     // supprimer région 
	@Override
	public boolean supprimerRegion(int idregion) {
		try {
			Region region =regionRepository.findByIdregion(idregion);

			if (region!=null) {
				//liste des service
				System.out.println("modification des departement  supression   region .....");
				List<Departement>  mesDepartementliste = departementRepository.findByRegion(region);
				Iterator<Departement> it=mesDepartementliste.iterator();
				while(it.hasNext())
				{
					Departement deepartement=it.next();
					departementService.supprimerDepartement(deepartement.getIddepartement());
				}

				regionRepository.deleteById(idregion);
				return  true;

			}else {
				return  false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

	// liste des region ordre pays 
	@Override
	public List<Region> listedesRegions() {
		try {
			System.out.println("liste   Region  service...");
			List<Region> listRegion = new ArrayList<Region>();
			List<Region> listereturnRegion=new ArrayList<Region>();
			regionRepository.mesregionparordre().forEach(listRegion::add);
			Iterator<Region> it=listRegion.iterator();
			while(it.hasNext())
			{
				Region patregion=it.next();
				Region monRegion=new Region();
				monRegion.setIdregion(patregion.getIdregion());
				monRegion.setNomregion(patregion.getNomregion());
				monRegion.setEtat(patregion.getEtat());
				Pays regionPays   = paysRepository.findByIdpays(patregion.getPays().getIdpays());
				if (regionPays!=null) {
					regionPays.setRegions(null);
					monRegion.setPays(regionPays);
				}
				listereturnRegion.add(monRegion);
			}

			return listereturnRegion;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	// liste des region ordre pays  active
		@Override
		public List<Region> listedesRegionsActive() {
			try {
				System.out.println("liste   Region  active service...");
				List<Region> listRegion = new ArrayList<Region>();
				List<Region> listereturnRegion=new ArrayList<Region>();
				regionRepository.findlisteActive().forEach(listRegion::add);
				Iterator<Region> it=listRegion.iterator();
				while(it.hasNext())
				{
					Region patregion=it.next();
					Region monRegion=new Region();
					monRegion.setIdregion(patregion.getIdregion());
					monRegion.setNomregion(patregion.getNomregion());
					monRegion.setEtat(patregion.getEtat());
					Pays regionPays   = paysRepository.findByIdpays(patregion.getPays().getIdpays());
					if (regionPays!=null) {
						regionPays.setRegions(null);
						monRegion.setPays(regionPays);
					}
					listereturnRegion.add(monRegion);
				}

				return listereturnRegion;
			} catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}




}
