package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.PaysRepository;
import com.meczy.dao.RegionRepository;
import com.meczy.entites.Pays;
import com.meczy.entites.Region;
import com.meczy.service.PaysService;
import com.meczy.service.RegionService;


@Service
@Transactional
public class PaysServiceImpl implements PaysService{
	
	@Autowired
	private PaysRepository paysRepository;
	@Autowired 
	private RegionRepository regionRepository;
	@Autowired
	private RegionService regionService;
	// ajouter pays
	@Override
	public boolean ajouterPays(Pays pays) {
		
	try {
        //Pays nouveauPays = new  Pays();
			/*
			 * newHopital.setIdHopital(id);
			 * newHopital.setNomHopital(hopital.getNomHopital());
			 * newHopital.setCapacite(hopital.getCapacite());
			 * newHopital.setAdresse(hopital.getAdresse()); newHopital.setServices(null);
			 * newHopital.setUtilisateurs(null); hopitalService.ajouterHopital(newHopital);
			 */
         System.out.println("creation  Pays service...");
		 String  idpays=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
         Integer id=java.lang.Integer.valueOf(idpays);
         pays.setIdpays(id);
		 Pays paysrecherche = paysRepository.findByNompays(pays.getNompays());
		 if (paysrecherche==null) {
	         System.out.println("creation  Pays. etat."+pays.getEtat());

			paysRepository.save(pays);
			return true;
		}
			return false;
			  
		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
    // Recherche par nom pays
	@Override
	public Pays rechercheNompays(String nomPays) {
		
		try {
			
		System.out.println("recherche   pays par nom service...");
		Pays pays = paysRepository.findByNompays(nomPays);
		if(pays==null) throw new RuntimeException("pays introuvable ");
		pays.setRegions(null);
		return pays;
		
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		
	}
	
	@Override
	public Pays rechercheId(int idpays) {
		
		try {
			
		System.out.println("recherche   pays par nom service...");
		Pays pays = paysRepository.findByIdpays(idpays);
		if(pays==null) throw new RuntimeException("pays introuvable ");
		pays.setRegions(null);
		return pays;
		
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		
	}

	//modifier pays
	@Override
	public Pays modifierPays(int idPays, Pays pays) {

		try {

			System.out.println("modifier   pays  service...");
			Pays monpays = paysRepository.findByIdpays(idPays);
			monpays.setNompays(pays.getNompays());
			monpays.setEtat(pays.getEtat());
			return paysRepository.save(monpays);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
  
	//liste des pays     
	@Override
	public List<Pays> listedesPays() {
		try {
			System.out.println("liste   pays  service...");
			List<Pays> listpays = new ArrayList<Pays>();
			List<Pays> listereturnpays=new ArrayList<Pays>();
			paysRepository.findAll().forEach(listpays::add);
			Iterator<Pays> it=listpays.iterator();
			while(it.hasNext())
			{
				Pays patpays=it.next();
				Pays monPays=new Pays();
				monPays.setIdpays(patpays.getIdpays());
				monPays.setNompays(patpays.getNompays());
				monPays.setEtat(patpays.getEtat());
				monPays.setRegions(null);
				listereturnpays.add(monPays);

			}

			return listereturnpays;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//liste des pays     
		@Override
		public List<Pays> listedesPaysActive() {
			try {
				System.out.println("liste   pays  service...");
				List<Pays> listpays = new ArrayList<Pays>();
				List<Pays> listereturnpays=new ArrayList<Pays>();
				paysRepository.findlisteActive().forEach(listpays::add);
				Iterator<Pays> it=listpays.iterator();
				while(it.hasNext())
				{
					Pays patpays=it.next();
					Pays monPays=new Pays();
					monPays.setIdpays(patpays.getIdpays());
					monPays.setNompays(patpays.getNompays());
					monPays.setEtat(patpays.getEtat());
					monPays.setRegions(null);
					listereturnpays.add(monPays);

				}

				return listereturnpays;
			} catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}


	  // suppression d'un pays 
		@Override
		public boolean supprimerPays(int idpays) {
			try {
				Pays pays =paysRepository.findByIdpays(idpays);

				if (pays!=null) {
					//liste des service
					System.out.println("modification des Region  supression   pays .....");
					List<Region>  mesRegionliste = regionRepository.findByPays(pays);
					Iterator<Region> it=mesRegionliste.iterator();
					while(it.hasNext())
					{
						Region derRegion=it.next();
						regionService.supprimerRegion(derRegion.getIdregion());
					}

					paysRepository.deleteById(idpays);
					return  true;

				}else {
					return  false;
				}

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			
		}

}
