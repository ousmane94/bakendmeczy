package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.ObjectcreditRepository;
import com.meczy.dao.SecteurcreditRepository;
import com.meczy.entites.Objectcredit;
import com.meczy.entites.Secteurcredit;
import com.meczy.service.SecteurcreditService;

@Service
@Transactional
public class SecteurcreditServiceImpl implements SecteurcreditService{

	@Autowired
	private ObjectcreditRepository objectcreditRepository;
	@Autowired
	private SecteurcreditRepository secteurcreditRepository;
	@Override
	public boolean ajouterSecteurcredit(Secteurcredit secteurcredit) {

		  try {
		  
		  System.out.println("creation  secteurcredit service..."); String
		  idcomite=""+((int) (Math.random()*(9-1))) +((int) (Math.random()*(9-1)));
		  Integer id=java.lang.Integer.valueOf(idcomite);
		  secteurcredit.setIdsecteurcredit(id); Secteurcredit monobjectcredit =
		  secteurcreditRepository.findByDescriptionsecteur(secteurcredit.
		  getDescriptionsecteur()); 
		  if (monobjectcredit==null) {
		  secteurcreditRepository.save(secteurcredit); 
		  return true; 
		  } return false;
		  
		  }catch(Exception e) { e.printStackTrace(); return false; } 
	}
	@Override
	public Secteurcredit rechercheSecteurcredit(String descriptionsecteur) {
		{ try {
			  System.out.println("recherche  Description Secteurcredit service...");
			  Secteurcredit comite =
			  secteurcreditRepository.findByDescriptionsecteur(descriptionsecteur);
			  
			  if(comite==null) throw new RuntimeException("Secteurcredit introuvable ");
			  return comite; }catch(Exception e) { 
				  e.printStackTrace(); 
			  return null; 
			  }
		}
	}
	@Override
	public Secteurcredit modifierSecteurcredit(int idsecteurcredit, Secteurcredit secteurcredit) {
		 { try {
			  System.out.println("modifier   Secteurcredit  service...");
			  
			  
			  Secteurcredit monobjectcredit =
			  secteurcreditRepository.findByIdsecteurcredit(idsecteurcredit);
			  if(monobjectcredit==null) 
				  return null;
			  monobjectcredit.setDescriptionsecteur(secteurcredit.getDescriptionsecteur());
			  monobjectcredit.setEtat(secteurcredit.getEtat()); 
			  return secteurcreditRepository.save(monobjectcredit);
			  
			  
			  } catch (Exception e) { 
				  e.printStackTrace(); return null; 
			  } 
			}
	}
	@Override
	public boolean ajouterdansObjectcredit(String description, int idsecteurcredit) {
		try {
			  System.out.println(" controle  Secteurcredit controle "+description);
			  Objectcredit utilisateur =
			  objectcreditRepository.findByDescription(description); Secteurcredit
			  usercomite = secteurcreditRepository.findByIdsecteurcredit(idsecteurcredit);
			  System.out.println(" controle  utilisateur controle "+idsecteurcredit);
			  
			  if (utilisateur !=null || usercomite !=null ) {
			  System.out.println(" ajouter bon   Secteurcredit Secteurcredit -bon--");
			  List<Secteurcredit> mescomites = new ArrayList<>();
			  mescomites.add(usercomite);
			  mescomites.addAll(utilisateur.getSecteurcredits());
			  utilisateur.setSecteurcredits(mescomites);
			  System.out.println(" savegarde  -bon--");
			  
			  objectcreditRepository.save(utilisateur); return true;
			  
			  }else {
			  
			  return false; }
			  
			  } catch(Exception e) { e.printStackTrace(); return false; 
			}
	}
	@Override
	public List<Secteurcredit> listedesSecteurcredit() {
		 try {
			  System.out.println("liste   Secteurcredit  service..."); List<Secteurcredit>
			  listComite= new ArrayList<Secteurcredit>(); List<Secteurcredit>
			  listereturnlistComite=new ArrayList<Secteurcredit>();
			  secteurcreditRepository.findAll().forEach(listComite::add);
			  Iterator<Secteurcredit> it=listComite.iterator(); 
			  while(it.hasNext()) {
			  Secteurcredit patComite=it.next(); 
			 // patComite.setObjectcredits(null);
			  listereturnlistComite.add(patComite); 
			  }
			  
			  return listereturnlistComite; } 
		 catch(Exception e) { e.printStackTrace();
			  return null; } 
	}
	@Override
	public List<Secteurcredit> listedessecteurActive() {
		try {
			  System.out.println("liste   Secteurcredit active service...");
			  List<Secteurcredit> listComite= new ArrayList<Secteurcredit>();
			  List<Secteurcredit> listereturnlistComite=new ArrayList<Secteurcredit>();
			  secteurcreditRepository.getSecteurcreditActive().forEach(listComite::add);
			  Iterator<Secteurcredit> it=listComite.iterator(); while(it.hasNext()) {
			  Secteurcredit patComite=it.next(); patComite.setObjectcredits(null);
			  listereturnlistComite.add(patComite); }

			  return listereturnlistComite; } catch(Exception e) { e.printStackTrace();
			  return null; 
	   } 
	}

	
	/*

	 * 

	 * 
	 * @Override public List<Secteurcredit> listedessecteurActive() { try {
	 * System.out.println("liste   Secteurcredit active service...");
	 * List<Secteurcredit> listComite= new ArrayList<Secteurcredit>();
	 * List<Secteurcredit> listereturnlistComite=new ArrayList<Secteurcredit>();
	 * secteurcreditRepository.getSecteurcreditActive().forEach(listComite::add);
	 * Iterator<Secteurcredit> it=listComite.iterator(); while(it.hasNext()) {
	 * Secteurcredit patComite=it.next(); patComite.setObjectcredits(null);
	 * listereturnlistComite.add(patComite); }
	 * 
	 * return listereturnlistComite; } catch(Exception e) { e.printStackTrace();
	 * return null; } }
	 */
	 

}
