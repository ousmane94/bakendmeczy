package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.meczy.dao.ActivitecreditRepository;
import com.meczy.dao.SousactiviteRepository;
import com.meczy.entites.Activitecredit;
import com.meczy.entites.Sousactivite;
import com.meczy.service.SousactiviteService;



@Service
@Transactional
public class SousactiviteServiceImpl implements SousactiviteService {


	@Autowired
	private SousactiviteRepository sousactiviteRepository;

	@Autowired
	private ActivitecreditRepository activitecreditRepository;

	@Override
	public boolean ajouterActivitecredit(Sousactivite sousactivite) {
		try {

			System.out.println("creation  sousactivite service...");
			String  idcomite=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idcomite);
			sousactivite.setIdsousactivitecredit(id);
			Sousactivite monobjectcredit = sousactiviteRepository.findByDescriptionactivitecredit(sousactivite.getDescriptionactivitecredit());
			System.out.println(sousactivite.getActivitecredit().getIdactivitecredit());
			if (monobjectcredit==null) {
				sousactiviteRepository.save(sousactivite);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}





	@Override
	public Sousactivite rechercheActivitecredit(String descriptionactivitecredit) {
		try {
			System.out.println("recherche  Description Sousactivite service...");
			Sousactivite comite = sousactiviteRepository.findByDescriptionactivitecredit(descriptionactivitecredit);

			if(comite==null) throw new RuntimeException("Sousactivite introuvable ");
			return comite;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Sousactivite modifierActivitecredit(int idactivitecredit, Sousactivite sousactivite) {
		try {
			System.out.println("modifier   Sousactivite  service...");


			Activitecredit monobjectcredit = activitecreditRepository.findByIdactivitecredit(sousactivite.getActivitecredit().getIdactivitecredit());
			if(monobjectcredit==null) 
				return null;
			Sousactivite activitecredit2 = sousactiviteRepository.findByIdsousactivitecredit(idactivitecredit);
			activitecredit2.setDescriptionactivitecredit(sousactivite.getDescriptionactivitecredit());
			activitecredit2.setActivitecredit(monobjectcredit);
			activitecredit2.setEtat(activitecredit2.getEtat());
			return sousactiviteRepository.save(activitecredit2);


		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerActivitecredit(int idactivitecredit) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Sousactivite> listedesActivitecredits() {
		try {
			System.out.println("liste  sous Activitecredit  service...");
			List<Sousactivite> listComite= new ArrayList<Sousactivite>();
			List<Sousactivite> listereturnlistComite=new ArrayList<Sousactivite>();
			sousactiviteRepository.findAll().forEach(listComite::add);
			Iterator<Sousactivite> it=listComite.iterator();
			while(it.hasNext())
			{
				Sousactivite patComite=it.next();
				patComite.setDossiers(null);
				listereturnlistComite.add(patComite);
			}

			return listereturnlistComite;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Sousactivite> listedesActivitecreditcredit() {
		try {
			System.out.println("liste   sous Activitecredit  service...");
			List<Sousactivite> listComite= new ArrayList<Sousactivite>();
			List<Sousactivite> listereturnlistComite=new ArrayList<Sousactivite>();
			sousactiviteRepository.getSousactiviteActive().forEach(listComite::add);
			Iterator<Sousactivite> it=listComite.iterator();
			while(it.hasNext())
			{
				Sousactivite patComite=it.next();
				patComite.setDossiers(null);
				listereturnlistComite.add(patComite);
			}

			return listereturnlistComite;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
