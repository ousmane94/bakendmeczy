package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.ProfilsRepository;
import com.meczy.entites.Comite;
import com.meczy.entites.Pays;
import com.meczy.entites.Profils;
import com.meczy.service.ProfilsService;

@Service
@Transactional
public class ProfilsServiceImpl  implements ProfilsService{
	
	@Autowired
	private ProfilsRepository profilsRepository;

	@Override
	public boolean ajouterProfils(Profils profils) {
		try {

			System.out.println("creation  Profils service...");
			String  idProfils=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idProfils);
			//profils.setIdprofils(id);
			Profils monProfils =profilsRepository.findByNomprofils(profils.getNomprofils());
			if (monProfils==null) {
				profilsRepository.save(profils);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Profils rechercheProfils(String nomrofils) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Profils modifierprofils(int idprofils, Profils profils) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean supprimerComite(int idcomite) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Profils> listedeProfils() {
			try {
				System.out.println("liste   Profils  service...");
				List<Profils> listpays = new ArrayList<Profils>();
				List<Profils> listereturnpays=new ArrayList<Profils>();
				profilsRepository.findAll().forEach(listpays::add);
				Iterator<Profils> it=listpays.iterator();
				while(it.hasNext())
				{
					Profils patpays=it.next();
					Profils monPays=new Profils();
					monPays.setIdprofils(patpays.getIdprofils());
					monPays.setNomprofils(patpays.getNomprofils());
					monPays.setUtilisateurs(null);
					listereturnpays.add(monPays);

				}

				return listereturnpays;
			} catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
	}

}
