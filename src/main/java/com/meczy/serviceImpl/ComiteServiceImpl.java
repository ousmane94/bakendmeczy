package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.AgenceRepository;
import com.meczy.dao.ComiteRepository;
import com.meczy.dao.UtilisateurRepository;
import com.meczy.entites.Agence;
import com.meczy.entites.Comite;
import com.meczy.service.ComiteService;

@Service
@Transactional
public class ComiteServiceImpl  implements ComiteService{
	
	@Autowired
	private ComiteRepository comiteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private AgenceRepository agenceRepository;
	

	@Override
	public boolean ajouterComite(Comite comite) {
		try {

			System.out.println("creation  Comite service...");
			String  idcomite=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idcomite);
			comite.setIdcomite(id);
			Comite moncomite = comiteRepository.findByNomcomite(comite.getNomcomite());
			if (moncomite==null) {
				comiteRepository.save(comite);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Comite rechercheComite(String nomcomite) {
		try {
			System.out.println("recherche  Agence service...");
			Comite comite = comiteRepository.findByNomcomite(nomcomite);
			
			if(comite==null) throw new RuntimeException("comite introuvable ");
			return comite;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Comite modifierComite(int idcomite, Comite comite) {
		
		try {
			System.out.println("modifier   Comite  service..."+comite.getIdcomite());
			System.out.println("modifier   Comite  service..."+comite.getNomcomite());
			System.out.println("modifier   Comite  service..."+comite.getNomcomite());
			System.out.println("modifier   Comite  service..."+comite.getEtat());

			Comite monComite = comiteRepository.findByIdcomite(idcomite);
			if(monComite==null) 
				return null;
			monComite.setNomcomite(comite.getNomcomite());
			monComite.setNumerocomite(comite.getNumerocomite());
			monComite.setTypecomite(comite.getTypecomite());
			monComite.setEtat(comite.getEtat());
			return comiteRepository.save(monComite);


		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerComite(int idcomite) {
		try {
			Comite comite = comiteRepository.findByIdcomite(idcomite);

			if (comite!=null) {
				//liste des agence
				System.out.println("modification des user  supression   Comite .....");
				List<Agence>  mesagencesList = agenceRepository.findByComite(comite);
				Iterator<Agence> it=mesagencesList.iterator();
				while(it.hasNext())
				{
					Agence delagence=it.next();
					delagence.setComite(null);
					agenceRepository.save(delagence);
				}
		
				comiteRepository.deleteById(idcomite);
				return  true;

			}else {
				return  false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Comite> listedesComites() {
		try {
			System.out.println("liste   Comite  service...");
			List<Comite> listComite= new ArrayList<Comite>();
			List<Comite> listereturnlistComite=new ArrayList<Comite>();
			comiteRepository.findAll().forEach(listComite::add);
			Iterator<Comite> it=listComite.iterator();
			while(it.hasNext())
			{
				Comite patComite=it.next();
				Comite monComite=new Comite();
				monComite.setIdcomite(patComite.getIdcomite());
				monComite.setNomcomite(patComite.getNomcomite());
				monComite.setNumerocomite(patComite.getNumerocomite());
				monComite.setEtat(patComite.getEtat());
				monComite.setTypecomite(patComite.getTypecomite());
				monComite.setAgences(null);
				monComite.setUtilisateurs(null);
				listereturnlistComite.add(monComite);
			}

			return listereturnlistComite;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	public List<Comite> listedesComitesActive() {
		try {
			System.out.println("liste   Comite active service...");
			List<Comite> listComite= new ArrayList<Comite>();
			List<Comite> listereturnlistComite=new ArrayList<Comite>();
			comiteRepository.getcomiteActive().forEach(listComite::add);
			Iterator<Comite> it=listComite.iterator();
			while(it.hasNext())
			{
				Comite patComite=it.next();
				Comite monComite=new Comite();
				monComite.setIdcomite(patComite.getIdcomite());
				monComite.setNomcomite(patComite.getNomcomite());
				monComite.setNumerocomite(patComite.getNumerocomite());
				monComite.setEtat(patComite.getEtat());
				monComite.setTypecomite(patComite.getTypecomite());
				monComite.setAgences(null);
				monComite.setUtilisateurs(null);
				listereturnlistComite.add(monComite);
			}

			return listereturnlistComite;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
