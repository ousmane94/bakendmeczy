package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.CommuneRepository;
import com.meczy.dao.DepartementRepository;
import com.meczy.dao.RegionRepository;
import com.meczy.entites.Agence;
import com.meczy.entites.Commune;
import com.meczy.entites.Departement;
import com.meczy.entites.Pays;
import com.meczy.entites.Quartier;
import com.meczy.entites.Region;
import com.meczy.service.CommuneService;
import com.meczy.service.DepartementService;

@Service
@Transactional
public class DepartementServiceImpl  implements DepartementService{

	@Autowired
	private DepartementRepository departementRepository;
	@Autowired
	private RegionRepository regionRepository;
	@Autowired
	private CommuneService communeService;
	@Autowired
	private CommuneRepository communeRepository;

	@Override
	public boolean ajouterDepartement(Departement departement) {
		try {

			System.out.println("creation  Departement service...");
			String  idDepartement=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idDepartement);
			departement.setIddepartement(id);
			Departement departementrecherche = departementRepository.findDepatementRegion(departement.getNomdepartement(),departement.getRegion().getNomregion());
			if (departementrecherche==null) {
				departementRepository.save(departement);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Departement rechercheNomDepartement(String nomdepartement) {
		try {
			System.out.println("recherche  Departement service...");
			Departement departement = departementRepository.findByNomdepartement(nomdepartement);
			if(departement==null) throw new RuntimeException("Departement introuvable ");
			return departement;		  
			}catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
	}

	@Override
	public Departement modifierDepartement(int iddepartement, Departement departement) {
		try {
			System.out.println("modifier   Departement  service...");
			Departement monDepartement = departementRepository.findByIddepartement(iddepartement);
			monDepartement.setNomdepartement(departement.getNomdepartement() );
			monDepartement.setRegion(departement.getRegion());
			monDepartement.setEtat(departement.getEtat());
			return departementRepository.save(monDepartement);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerDepartement(int iddepartement) {
		try {
			Departement departement =departementRepository.findByIddepartement(iddepartement);

			if (departement!=null) {
				//liste des service
				System.out.println("modification des commune  supression   departement .....");
				List<Commune>  mesCommuneliste = communeRepository.findByDepartement(departement);
				Iterator<Commune> it=mesCommuneliste.iterator();
				while(it.hasNext())
				{
					Commune decommune=it.next();
					communeService.supprimerCommune(decommune.getIdcommune());
				}

				departementRepository.deleteById(iddepartement);
				return  true;

			}else {
				return  false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public List<Departement> listedesDepartements() {
		try {
			System.out.println("liste   Departement  service...");
			List<Departement> listDepartement = new ArrayList<Departement>();
			List<Departement> listereturnDepartement=new ArrayList<Departement>();
			departementRepository.mesdepartementparordre().forEach(listDepartement::add);
			Iterator<Departement> it=listDepartement.iterator();
			while(it.hasNext())
			{
				Departement patDepartement=it.next();
				Departement monDepartement=new Departement();
				monDepartement.setIddepartement(patDepartement.getIddepartement());
				monDepartement.setNomdepartement(patDepartement.getNomdepartement());
				monDepartement.setEtat(patDepartement.getEtat());
				monDepartement.setRegion(patDepartement.getRegion());
				monDepartement.setCommunes(null);
				
				Region regionPays   = regionRepository.findByIdregion(patDepartement.getRegion().getIdregion());
				if (regionPays!=null) {
					regionPays.setDepartements(null);
					//regionPays.setPays(null);
					monDepartement.setRegion(regionPays);
				}
			
				listereturnDepartement.add(monDepartement);
			}

			return listereturnDepartement;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Departement> listedesDepartementsActive() {
		try {
			System.out.println("liste   Departement  service...");
			List<Departement> listDepartement = new ArrayList<Departement>();
			List<Departement> listereturnDepartement=new ArrayList<Departement>();
			departementRepository.mesdepartementparordreActive().forEach(listDepartement::add);
			Iterator<Departement> it=listDepartement.iterator();
			while(it.hasNext())
			{
				Departement patDepartement=it.next();
				Departement monDepartement=new Departement();
				monDepartement.setIddepartement(patDepartement.getIddepartement());
				monDepartement.setNomdepartement(patDepartement.getNomdepartement());
				monDepartement.setEtat(patDepartement.getEtat());
				//monDepartement.setRegion(patDepartement.getRegion());
				monDepartement.setCommunes(null);
				//monDepartement.setRegion(patDepartement.getRegion());
				/*
				 * Region departementRegion =
				 * regionRepository.findByIdregion(patDepartement.getRegion().getIdregion()); if
				 * (departementRegion!=null) { departementRegion.setDepartements(null); Pays
				 * pyPays = patDepartement.getRegion().getPays(); pyPays.setRegions(null);
				 * departementRegion.setPays(pyPays); }
				 */
				listereturnDepartement.add(monDepartement);
			}

			return listereturnDepartement;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
