package com.meczy.serviceImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.AgenceRepository;
import com.meczy.dao.PlanificateurRepository;
import com.meczy.dao.RendezvousRepository;
import com.meczy.entites.Agence;
import com.meczy.entites.Planification;
import com.meczy.entites.Rendezvous;
import com.meczy.service.PlanificationService;

@Service
@Transactional
public class PlanificationServiceImpl implements PlanificationService  {
	
	@Autowired
	private PlanificateurRepository planificateurRepository;
	@Autowired
	private RendezvousRepository rendezvousRepository;
	@Autowired
	private AgenceRepository agenceRepository;
	
	@Override
	public boolean ajouterPlanification(Planification planification) {
		try {

			System.out.println("creation  Planification service...");
			String  idPlanification=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idPlanification);
			planification.setIdplanification(id);

			Planification planificationrecherche = planificateurRepository.findByDateplanificationAndAgence(planification.getDateplanification(),planification.getAgence());			
			if (planificationrecherche==null) {
				planificateurRepository.save(planification);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Planification recherchedatePlanification(String dateplanification ,String codeagence) {
		try {
			System.out.println("recherche  Planification service...");
			Agence agence = agenceRepository.findByCodeagence(codeagence);
			   
			   DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			   //String cunvertCurrentDate="06/09/2015";
			   Date date1 = new Date();
			   date1 = df.parse(dateplanification);
			   System.out.println(date1);
			  
			Planification planificationrecherche = planificateurRepository.findByDateplanificationAndAgence(date1 ,agence);			
			//Planification planification = planificateurRepository.findByCodeagence(codeagence);
			if(planificationrecherche==null) throw new RuntimeException("planification introuvable ");
			return planificationrecherche;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	

	@Override
	public Planification modifierplanification(int idplanification, Planification planification) {
		try {
			System.out.println("modifier   Planification  service...");
			Planification editplanification = planificateurRepository.findByIdplanification(idplanification);
			if (editplanification==null) 
				return null;
			editplanification.setAgence(planification.getAgence());
			editplanification.setDateplanification(planification.getDateplanification());
			editplanification.setDateFinaleplanification(planification.getDateFinaleplanification());
			editplanification.setMaxdossierplanification(planification.getMaxdossierplanification());
			editplanification.setNombredossierplanification(planification.getNombredossierplanification());	
			editplanification.setEtat(planification.getEtat());
			return planificateurRepository.save(editplanification);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerPlanification(int idPlanification) {
		try {
			Planification supplanification = planificateurRepository.findByIdplanification(idPlanification);


			if (supplanification!=null) {
				//liste des rendez vous 
				System.out.println("modification des rendez vous  supression   planification .....");
				List<Rendezvous>  mesrRendezvouse = rendezvousRepository.findByPlanification(supplanification);
				Iterator<Rendezvous> it=mesrRendezvouse.iterator();
				while(it.hasNext())
				{
					Rendezvous delRendezvous=it.next();
					rendezvousRepository.delete(delRendezvous);
				}
				planificateurRepository.deleteById(idPlanification);
				
				return  true;

			}else {
				return  false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Planification> listedesPlanifications() {
		try {
			System.out.println("liste   Planification  service...");
			List<Planification> listPlanification= new ArrayList<Planification>();
			List<Planification> listereturnlistPlanification=new ArrayList<Planification>();
			planificateurRepository.findAll().forEach(listPlanification::add);
			Iterator<Planification> it=listPlanification.iterator();
			while(it.hasNext())
			{
				Planification patPlanification=it.next();
				//Planification monPlanification= patPlanification;
				patPlanification.setRendezvous(null);
			
				listereturnlistPlanification.add(patPlanification);
			}

			return listereturnlistPlanification;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	public List<Planification> listedesPlanificationsActive() {
		try {
			System.out.println("liste   Planification Active  service...");
			List<Planification> listPlanification= new ArrayList<Planification>();
			List<Planification> listereturnlistPlanification=new ArrayList<Planification>();
			planificateurRepository.listPlanificationsActive().forEach(listPlanification::add);
			Iterator<Planification> it=listPlanification.iterator();
			while(it.hasNext())
			{
				Planification patPlanification=it.next();
				//Planification monPlanification= patPlanification;
				patPlanification.setRendezvous(null);
			
				listereturnlistPlanification.add(patPlanification);
			}

			return listereturnlistPlanification;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	

		@Override
		public List<Planification> listedesPlanificationAgence(String codeagence) {
			try {
				System.out.println("liste   Planification  service...");
				List<Planification> listPlanification= new ArrayList<Planification>();
				List<Planification> listereturnlistPlanification=new ArrayList<Planification>();
				planificateurRepository.listPlanificationsActiveAgence(codeagence).forEach(listPlanification::add);
				Iterator<Planification> it=listPlanification.iterator();
				while(it.hasNext())
				{
					Planification patPlanification=it.next();
					//Planification monPlanification= patPlanification;
					patPlanification.setRendezvous(null);
				
					listereturnlistPlanification.add(patPlanification);
				}

				return listereturnlistPlanification;
			} catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}

	@Override
	public List<Planification> recherchedatePlaning(String dateplanification) {
		try {

			System.out.println("liste   Planification  par date  service...");

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			//String cunvertCurrentDate="06/09/2015";
			Date date1 = new Date();
			date1 = df.parse(dateplanification);
			System.out.println(date1);
			List<Planification> listPlanification= new ArrayList<Planification>();
			List<Planification> listereturnlistPlanification=new ArrayList<Planification>();
			planificateurRepository.findByDateplanification(date1).forEach(listPlanification::add);
			Iterator<Planification> it=listPlanification.iterator();
			while(it.hasNext())
			{
				Planification patPlanification=it.next();
				patPlanification.setRendezvous(null);
				listereturnlistPlanification.add(patPlanification);
			}

			return listereturnlistPlanification;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Planification> listedesPlanificationAgenceActive(String codeagence) {
		// TODO Auto-generated method stub
		return null;
	}

}
