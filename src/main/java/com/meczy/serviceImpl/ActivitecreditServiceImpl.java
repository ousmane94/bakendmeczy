package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.ActivitecreditRepository;
import com.meczy.dao.SecteurcreditRepository;
import com.meczy.entites.Activitecredit;
import com.meczy.entites.Secteurcredit;
import com.meczy.service.ActivitecreditService;

@Service
@Transactional
public class ActivitecreditServiceImpl implements ActivitecreditService {
	

	@Autowired
	private SecteurcreditRepository secteurcreditRepository;

	@Autowired
	private ActivitecreditRepository activitecreditRepository;
	
	@Override
	public boolean ajouterActivitecredit(Activitecredit activitecredit) {
		try {

			System.out.println("creation  activitecredit service...");
			String  idcomite=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idcomite);
			activitecredit.setIdactivitecredit(id);
			Activitecredit monobjectcredit = activitecreditRepository.findByDescriptionactivitecredit(activitecredit.getDescriptionactivitecredit());
			if (monobjectcredit==null) {
				activitecreditRepository.save(activitecredit);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}


	

	@Override
	public Activitecredit rechercheActivitecredit(String descriptionactivitecredit) {
		try {
			System.out.println("recherche  Description Secteurcredit service...");
			Activitecredit comite = activitecreditRepository.findByDescriptionactivitecredit(descriptionactivitecredit);
			
			if(comite==null) throw new RuntimeException("Activitecredit introuvable ");
			return comite;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Activitecredit modifierActivitecredit(int idactivitecredit, Activitecredit activitecredit) {
		try {
			System.out.println("modifier   Activitecredit  service...");


			Secteurcredit monobjectcredit = secteurcreditRepository.findByIdsecteurcredit(activitecredit.getSecteurcredit().getIdsecteurcredit());
			if(monobjectcredit==null) 
				return null;
			Activitecredit activitecredit2 = activitecreditRepository.findByIdactivitecredit(idactivitecredit);
			activitecredit2.setDescriptionactivitecredit(activitecredit.getDescriptionactivitecredit());
			activitecredit2.setSecteurcredit(monobjectcredit);
			activitecredit2.setEtat(activitecredit.getEtat());
			return activitecreditRepository.save(activitecredit2);


		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerActivitecredit(int idactivitecredit) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Activitecredit> listedesActivitecredits() {
		try {
			System.out.println("liste   Activitecredit  service...");
			List<Activitecredit> listComite= new ArrayList<Activitecredit>();
			List<Activitecredit> listereturnlistComite=new ArrayList<Activitecredit>();
			activitecreditRepository.findAll().forEach(listComite::add);
			Iterator<Activitecredit> it=listComite.iterator();
			while(it.hasNext())
			{
				Activitecredit patComite=it.next();
				patComite.setSousactivite(null);
				listereturnlistComite.add(patComite);
			}

			return listereturnlistComite;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Activitecredit> listedesActivitecreditcredit() {
		try {
			System.out.println("liste   Activitecredit  service...");
			List<Activitecredit> listComite= new ArrayList<Activitecredit>();
			List<Activitecredit> listereturnlistComite=new ArrayList<Activitecredit>();
			activitecreditRepository.getActivitecreditActive().forEach(listComite::add);
			Iterator<Activitecredit> it=listComite.iterator();
			while(it.hasNext())
			{
				Activitecredit patComite=it.next();
				patComite.setSousactivite(null);
				listereturnlistComite.add(patComite);
			}

			return listereturnlistComite;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
