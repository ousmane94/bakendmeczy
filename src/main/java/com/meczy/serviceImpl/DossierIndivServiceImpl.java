package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.AgenceRepository;
import com.meczy.dao.DossierIndivRepository;
import com.meczy.entites.Agence;
import com.meczy.entites.Dossierindividuel;
import com.meczy.entites.Utilisateur;
import com.meczy.service.CompteService;
import com.meczy.service.DossierIndivService;

@Service
@Transactional
public class DossierIndivServiceImpl implements DossierIndivService {



	@Autowired
	private DossierIndivRepository dossierIndivRepository;
	@Autowired
	private AgenceRepository agenceRepository;
	@Autowired
	private CompteService compteService;

	@Override
	public boolean ajouterDossierindividuel(Dossierindividuel dossierindividuel) {
		
		List<Dossierindividuel> deDossierindividuels = (List<Dossierindividuel>) dossierIndivRepository.findAll();
		if(deDossierindividuels.size()<=0) {
			
			Dossierindividuel user1 =dossierIndivRepository.findByNumerodemande(1);
			if(user1!=null) throw new RuntimeException("Dossierindividuel existe deja");
			Dossierindividuel user2 = new Dossierindividuel();
			String  iduser1=""+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)));
			//Utilisateur uerUtilisateur = compteService.monUtilisateur("145676H");
			//if(uerUtilisateur==null) throw new RuntimeException("uerUtilisateur  nhexiste deja");
			Integer uss1=java.lang.Integer.valueOf(iduser1);
			user2.setNatureentrepreneur(dossierindividuel.getNatureentrepreneur());
			user2.setAgence(dossierindividuel.getAgence());
			user2.setNumerodemande(1);
			user2.setDatedemande(dossierindividuel.getDatedemande());
			user2.setComptemembre(dossierindividuel.getComptemembre());
			user2.setRaisonsociale(dossierindividuel.getRaisonsociale());
			user2.setDateadhesion(dossierindividuel.getDateadhesion());
			user2.setDatepayementfrais(dossierindividuel.getDatepayementfrais());
			user2.setMontant(dossierindividuel.getMontant());
			user2.setNatureidentification(dossierindividuel.getNatureidentification());
			user2.setNumeropiece(dossierindividuel.getNumeropiece());
			user2.setDatenaissance(dossierindividuel.getDatenaissance());
			user2.setLieudenaissance(dossierindividuel.getLieudenaissance());
			user2.setNompere(dossierindividuel.getNompere());
			user2.setNommere(dossierindividuel.getNommere());
			user2.setGenre(dossierindividuel.getGenre());
			user2.setAdresse(dossierindividuel.getAdresse());
			user2.setAdresseentreprise(dossierindividuel.getAdresseentreprise());
			user2.setEnaffaire(dossierindividuel.getEnaffaire());
			user2.setTeldomicile(dossierindividuel.getTeldomicile());
			user2.setTelportable(dossierindividuel.getTelportable());
			user2.setBureau(dossierindividuel.getBureau());
			user2.setSituationfamilial(dossierindividuel.getSituationfamilial());
			user2.setEnfantencharge(dossierindividuel.getEnfantencharge());
			user2.setConjoint(dossierindividuel.getConjoint());
			user2.setMontantdemande(dossierindividuel.getMontantdemande());
			user2.setMontantaccorde(dossierindividuel.getMontantaccorde());
			user2.setTauxinteret(dossierindividuel.getTauxinteret());
			user2.setDuree(dossierindividuel.getDuree());
			user2.setNombreecheance(dossierindividuel.getNombreecheance());
			user2.setMontantremboursement(dossierindividuel.getMontantremboursement());
			user2.setPeriodicite(dossierindividuel.getPeriodicite());
			user2.setDatedeblocage(dossierindividuel.getDatedeblocage());
			user2.setDateecheance(dossierindividuel.getDateecheance());
			user2.setDateecheancefinale(dossierindividuel.getDateecheancefinale());
			user2.setButcredit(dossierindividuel.getButcredit());
			user2.setActivite(dossierindividuel.getActivite());
			user2.setObjectcredit(dossierindividuel.getObjectcredit());
			user2.setSousactivite(dossierindividuel.getSousactivite());
			user2.setUtilisateur(dossierindividuel.getUtilisateur());
			user2.setIddossier(uss1);
			user2.setEmprunteur(dossierindividuel.getEmprunteur());
			//user2.se(dossierindividuel.ge);
			//user2.se(dossierindividuel.ge);
			System.out.println("Debut Enregidtrement reusi");
			dossierIndivRepository.save(user2);
			System.out.println(" Enregidtrement reusi");
			return true;
			
		}else {
		Dossierindividuel user1 =dossierIndivRepository.findByNumerodemande(deDossierindividuels.size()+1);
		if(user1!=null) throw new RuntimeException("Dossierindividuel existe deja");
		Dossierindividuel user2 = new Dossierindividuel();
		String  iduser1=""+((int) (Math.random()*(9-0)))
				+((int) (Math.random()*(9-0)))
				+((int) (Math.random()*(9-0)));
		//Utilisateur uerUtilisateur = compteService.monUtilisateur("145676H");
		//if(uerUtilisateur==null) throw new RuntimeException("uerUtilisateur  nhexiste deja");
		Integer uss1=java.lang.Integer.valueOf(iduser1);
		user2.setNatureentrepreneur(dossierindividuel.getNatureentrepreneur());
		user2.setAgence(dossierindividuel.getAgence());
		user2.setNumerodemande(deDossierindividuels.size()+1);
		user2.setDatedemande(dossierindividuel.getDatedemande());
		user2.setComptemembre(dossierindividuel.getComptemembre());
		user2.setRaisonsociale(dossierindividuel.getRaisonsociale());
		user2.setDateadhesion(dossierindividuel.getDateadhesion());
		user2.setDatepayementfrais(dossierindividuel.getDatepayementfrais());
		user2.setMontant(dossierindividuel.getMontant());
		user2.setNatureidentification(dossierindividuel.getNatureidentification());
		user2.setNumeropiece(dossierindividuel.getNumeropiece());
		user2.setDatenaissance(dossierindividuel.getDatenaissance());
		user2.setLieudenaissance(dossierindividuel.getLieudenaissance());
		user2.setNompere(dossierindividuel.getNompere());
		user2.setNommere(dossierindividuel.getNommere());
		user2.setGenre(dossierindividuel.getGenre());
		user2.setAdresse(dossierindividuel.getAdresse());
		user2.setAdresseentreprise(dossierindividuel.getAdresseentreprise());
		user2.setEnaffaire(dossierindividuel.getEnaffaire());
		user2.setTeldomicile(dossierindividuel.getTeldomicile());
		user2.setTelportable(dossierindividuel.getTelportable());
		user2.setBureau(dossierindividuel.getBureau());
		user2.setSituationfamilial(dossierindividuel.getSituationfamilial());
		user2.setEnfantencharge(dossierindividuel.getEnfantencharge());
		user2.setConjoint(dossierindividuel.getConjoint());
		user2.setMontantdemande(dossierindividuel.getMontantdemande());
		user2.setMontantaccorde(dossierindividuel.getMontantaccorde());
		user2.setTauxinteret(dossierindividuel.getTauxinteret());
		user2.setDuree(dossierindividuel.getDuree());
		user2.setNombreecheance(dossierindividuel.getNombreecheance());
		user2.setMontantremboursement(dossierindividuel.getMontantremboursement());
		user2.setPeriodicite(dossierindividuel.getPeriodicite());
		user2.setDatedeblocage(dossierindividuel.getDatedeblocage());
		user2.setDateecheance(dossierindividuel.getDateecheance());
		user2.setDateecheancefinale(dossierindividuel.getDateecheancefinale());
		user2.setButcredit(dossierindividuel.getButcredit());
		user2.setActivite(dossierindividuel.getActivite());
		user2.setUtilisateur(dossierindividuel.getUtilisateur());
		user2.setObjectcredit(dossierindividuel.getObjectcredit());
		user2.setSousactivite(dossierindividuel.getSousactivite());
		user2.setEmprunteur(dossierindividuel.getEmprunteur());
		user2.setIddossier(uss1);
		//user2.se(dossierindividuel.ge);
		//user2.se(dossierindividuel.ge);
		System.out.println("Debut Enregidtrement reusi");
		dossierIndivRepository.save(user2);
		System.out.println(" Enregidtrement reusi");
		return true;
		}
	}

	@Override
	public Dossierindividuel lectureparNumeroDossier(long numerodossier) {
		try {
			System.out.println("recherche numerodossier  Dossierindividuel  service...");			  
			Dossierindividuel dossierindividuel = dossierIndivRepository.findByNumerodemande(numerodossier);			
			if(dossierindividuel==null) throw new RuntimeException("dossierindividuel introuvable ");
			return dossierindividuel;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Dossierindividuel lectureparNumeroDossierAndagence(long numerodossier,String codeagence) {
		try {
			System.out.println("recherche numerodossier  Dossierindividuel agence  service...");
			Agence agence = agenceRepository.findByCodeagence(codeagence);

			Dossierindividuel dossierindividuel = dossierIndivRepository.findByNumerodemandeAndAgence(numerodossier,agence);			
			if(dossierindividuel==null) throw new RuntimeException("dossierindividuel introuvable ");
			return dossierindividuel;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Dossierindividuel> listDossierindividuel() {
		try {
			System.out.println("liste   Dossierindividuel  service...");
			List<Dossierindividuel> listPlanification= new ArrayList<Dossierindividuel>();
			List<Dossierindividuel> listereturnlistPlanification=new ArrayList<Dossierindividuel>();
			dossierIndivRepository.findAll().forEach(listPlanification::add);
			Iterator<Dossierindividuel> it=listPlanification.iterator();
			while(it.hasNext())
			{
				Dossierindividuel patPlanification=it.next();
				//Planification monPlanification= patPlanification;
				patPlanification.setBilanpersonnels(null);
				patPlanification.setGaranties(null);
				patPlanification.setBudgets(null);

				listereturnlistPlanification.add(patPlanification);
			}

			return listereturnlistPlanification;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public Dossierindividuel updeateDossierindividuel(int iddossierindividuel, Dossierindividuel dossierindividuel) {
		try {
			System.out.println("modifier   Dossierindividuel  service...");
			Dossierindividuel monDossierindividuel = dossierIndivRepository.findByIddossier(iddossierindividuel);
			if(monDossierindividuel==null)
				return null;
			dossierindividuel.setBilanpersonnels(monDossierindividuel.getBilanpersonnels());
			dossierindividuel.setGaranties(monDossierindividuel.getGaranties());
			dossierindividuel.setBudgets(monDossierindividuel.getBudgets());
			dossierindividuel.setRecommandations(monDossierindividuel.getRecommandations());	
			return dossierIndivRepository.save(dossierindividuel);


		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public boolean deleteDossierindividuel(int iddossierindividuel) {
		// TODO Auto-generated method stub
		return false;

	}

	@Override
	public Page<Dossierindividuel> findPaginated(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public  List<Dossierindividuel> lectureparNumeroDossierAndutilisateur(long numerodossier, String matricule) {
		try {
			System.out.println("recherche numerodossier  Dossierindividuel matricule  service...");
			Utilisateur utilisateur = compteService.monUtilisateur(matricule);

			 List<Dossierindividuel> dossierindividuel = dossierIndivRepository.findByNumerodemandeAndUtilisateur(numerodossier, utilisateur);			
			if(dossierindividuel==null) throw new RuntimeException("dossierindividuel introuvable ");
			return dossierindividuel;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Dossierindividuel> listDossierindividuelparAgent(String matricule) {
		try {
			System.out.println("liste   Dossierindividuel par agent  service...");
			List<Dossierindividuel> listPlanification= new ArrayList<Dossierindividuel>();
			List<Dossierindividuel> listereturnlistPlanification=new ArrayList<Dossierindividuel>();
			dossierIndivRepository.finddossierAgentcredit(matricule).forEach(listPlanification::add);
			Iterator<Dossierindividuel> it=listPlanification.iterator();
			while(it.hasNext())
			{
				Dossierindividuel patPlanification=it.next();
				//Planification monPlanification= patPlanification;
				patPlanification.setBilanpersonnels(null);
				patPlanification.setGaranties(null);
				patPlanification.setBudgets(null);
				patPlanification.setRecommandations(null);

				listereturnlistPlanification.add(patPlanification);
			}

			return listereturnlistPlanification;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
