package com.meczy.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meczy.dao.AgenceRepository;
import com.meczy.dao.ComiteRepository;
import com.meczy.dao.PlanificateurRepository;
import com.meczy.dao.QuartierRepository;
import com.meczy.dao.UtilisateurRepository;
import com.meczy.entites.Agence;
import com.meczy.entites.Comite;
import com.meczy.entites.Planification;
import com.meczy.entites.Quartier;
import com.meczy.entites.Utilisateur;
import com.meczy.service.AgenceService;
import com.meczy.service.PlanificationService;

@Service
@Transactional
public class AgenceServiceImpl implements AgenceService {
	
	@Autowired
	private AgenceRepository agenceRepository;
	@Autowired
	private PlanificateurRepository planificateurRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private PlanificationService planificationService;
	@Autowired
	private ComiteRepository comiteRepository;
	@Autowired
	private QuartierRepository  quartierRepository;

	@Override
	public boolean ajouterAgence(Agence agence) {
		try {

			System.out.println("creation  Agence service...");
			
			String  idAgence=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idAgence);
			agence.setIdagence(id);
			Agence agencerecherche = agenceRepository.findByCodeagence(agence.getCodeagence());
			
			if (agencerecherche==null) {
				agenceRepository.save(agence);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Agence findByCodeAgence(String codeagence) {
		try {
			System.out.println("recherche  Agence service...");
			Agence agence = agenceRepository.findByCodeagence(codeagence);
			if(agence==null) throw new RuntimeException("agence introuvable ");
			return agence;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Agence modificationAgence(int idagence, Agence agence) {
		try {
			System.out.println("modifier   Agence  service...");
			Agence monAgence = agenceRepository.findByIdagence(idagence);
			if(monAgence==null)
				return null;
			monAgence.setAdresseagence(agence.getAdresseagence());
			monAgence.setCodeagence(agence.getCodeagence());
			monAgence.setComite(agence.getComite());
			monAgence.setQuartier(agence.getQuartier());
			monAgence.setEtat(agence.getEtat());
			return agenceRepository.save(monAgence);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerAgence(int idagence) {
		try {
			Agence agence =agenceRepository.findByIdagence(idagence);

			if (agence!=null) {
				//liste des service
				System.out.println("modification des user  supression   agences .....");
				List<Utilisateur>  mesUtilisateurliste = utilisateurRepository.findByAgence(agence);
				Iterator<Utilisateur> it=mesUtilisateurliste.iterator();
				while(it.hasNext())
				{
					Utilisateur deuUtilisateur=it.next();
					deuUtilisateur.setAgence(agence);
					utilisateurRepository.save(deuUtilisateur);
				}
				List<Planification>  mesPlanifications = planificateurRepository.findByAgence(agence);
				Iterator<Planification> itp =mesPlanifications.iterator();
				while(it.hasNext())
				{
					Planification dePlanification=itp.next();
					planificationService.supprimerPlanification(dePlanification.getIdplanification());
				}


				agenceRepository.deleteById(idagence);
				return  true;

			}else {
				return  false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Agence> listedesAgences() {
		try {
			System.out.println("liste   Agence  service...");
			List<Agence> listAgence= new ArrayList<Agence>();
			List<Agence> listereturnlistAgence=new ArrayList<Agence>();
			agenceRepository.mesAgenceparordre().forEach(listAgence::add);
			Iterator<Agence> it=listAgence.iterator();
			while(it.hasNext())
			{
				Agence patAgence=it.next();
				Agence monAgence=new Agence();
				monAgence.setAdresseagence(patAgence.getAdresseagence());
				monAgence.setIdagence(patAgence.getIdagence());
				monAgence.setCodeagence(patAgence.getCodeagence());
				monAgence.setUtilisateurs(null);
				monAgence.setPlanifications(null);
				monAgence.setEtat(patAgence.getEtat());
				monAgence.setQuartier(patAgence.getQuartier());
				monAgence.setComite(patAgence.getComite());
				/*
				 * Comite monComite =
				 * comiteRepository.findByIdcomite(patAgence.getComite().getIdcomite());
				 * 
				 * if (monComite!=null) { monComite.setUtilisateurs(null);
				 * monComite.setAgences(null); monAgence.setComite(monComite);
				 * 
				 * } Quartier monquartier =
				 * quartierRepository.findByIdquartier(patAgence.getQuartier().getIdquartier());
				 * if(monquartier !=null) {
				 * 
				 * monquartier.setAgences(null); monquartier.setCommune(null);
				 * monAgence.setQuartier(monquartier); }
				 */
				
				listereturnlistAgence.add(monAgence);
			}

			return listereturnlistAgence;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public List<Agence> listedesAgencesActive() {
		try {
			System.out.println("liste   Agence active  service...");
			List<Agence> listAgence= new ArrayList<Agence>();
			List<Agence> listereturnlistAgence=new ArrayList<Agence>();
			agenceRepository.mesAgenceparordreActive().forEach(listAgence::add);
			Iterator<Agence> it=listAgence.iterator();
			while(it.hasNext())
			{
				Agence patAgence=it.next();
				Agence monAgence=new Agence();
				monAgence.setAdresseagence(patAgence.getAdresseagence());
				monAgence.setIdagence(patAgence.getIdagence());
				monAgence.setCodeagence(patAgence.getCodeagence());
				monAgence.setUtilisateurs(null);
				monAgence.setPlanifications(null);
				monAgence.setQuartier(patAgence.getQuartier());
				monAgence.setComite(patAgence.getComite());
			
				listereturnlistAgence.add(monAgence);
			}

			return listereturnlistAgence;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
