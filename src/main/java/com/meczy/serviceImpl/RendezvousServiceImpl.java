package com.meczy.serviceImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.meczy.dao.PlanificateurRepository;
import com.meczy.dao.RendezvousRepository;
import com.meczy.entites.Planification;
import com.meczy.entites.Rendezvous;
import com.meczy.service.RendezvousService;

@Service
@Transactional
public class RendezvousServiceImpl  implements RendezvousService{

	@Autowired
	private RendezvousRepository rendezvousRepository;
	@Autowired
	private PlanificateurRepository planificateurRepository;

	@Override
	public boolean ajouterRendezvous(Rendezvous rendezvous) {
		try {

			System.out.println("creation  Agence service...");

			String  idrendezvous=""+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)))
					+((int) (Math.random()*(9-1)));
			Integer id=java.lang.Integer.valueOf(idrendezvous);
			rendezvous.setIdrendezvous(id); 
			rendezvous.setEtat(1);
			Rendezvous rendezvousrecherche = rendezvousRepository.findByNumeromembreAndPlanification(rendezvous.getNumeromembre(), rendezvous.getPlanification());
			Planification planingPlanification = planificateurRepository.findByIdplanification(rendezvous.getPlanification().getIdplanification());

			if (rendezvousrecherche==null) {
				if(planingPlanification.getNombredossierplanification()<=0) {
					planingPlanification.setNombredossierplanification(1);
					rendezvous.setNumerorendezvous(1);

				}
				// int po= planingPlanification.getNombredossierplanification() +1;
				//List<Rendezvous> lgList =rendezvousRepository.findByPlanification(planingPlanification);
				rendezvous.setNumerorendezvous(planingPlanification.getNombredossierplanification()+1);
				planingPlanification.setNombredossierplanification(rendezvousRepository.findByPlanification(planingPlanification).size()+1);
				// rendezvous.setNumerorendezvous(rendezvous.getNumerorendezvous()+1);

				planificateurRepository.save(planingPlanification);
				rendezvousRepository.save(rendezvous);
				return true;
			}
			return false;

		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Rendezvous findByNumerorendezvous(long numerorendezvous) {
		try {
			System.out.println("recherche  Rendezvous service...");
			Rendezvous rendezvous = rendezvousRepository.findByNumerorendezvous(numerorendezvous);
			if(rendezvous==null) throw new RuntimeException("rendezvous introuvable ");
			return rendezvous;		  
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public Rendezvous modificationRendezvous(int idrendezvous, Rendezvous rendezvous) {
		try {
			System.out.println("modifier   Rendezvous  service...");
			Rendezvous monrendezvous = rendezvousRepository.findByIdrendezvous(idrendezvous);

			if(monrendezvous==null)
				return null;
			monrendezvous.setDescriptionrendezvous(rendezvous.getDescriptionrendezvous());
			monrendezvous.setHeurerendezvous(rendezvous.getHeurerendezvous());
			monrendezvous.setNumeromembre(rendezvous.getNumeromembre());
			monrendezvous.setNumerorendezvous(rendezvous.getNumerorendezvous());
			monrendezvous.setPlanification(rendezvous.getPlanification());
			monrendezvous.setFraisRendezvous(rendezvous.getFraisRendezvous());
			monrendezvous.setMontant(rendezvous.getMontant());
			monrendezvous.setEtat(rendezvous.getEtat());
			System.out.println("modifier   Rendezvous  service..."+rendezvous.getUtilisateur().getIdutilisateur());

			monrendezvous.setUtilisateur(rendezvous.getUtilisateur());
			return rendezvousRepository.save(monrendezvous);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean supprimerRendezvous(int idrendezvous) {
		try {
			Rendezvous monrendezvous = rendezvousRepository.findByIdrendezvous(idrendezvous);

			if (monrendezvous==null) 
				return false;
			rendezvousRepository.deleteById(idrendezvous);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Rendezvous> listedesRendezvous() {
		try {
			System.out.println("liste   Rendezvous  service...");
			List<Rendezvous> listRendezvous= new ArrayList<Rendezvous>();
			List<Rendezvous> listereturnlistRendezvous=new ArrayList<Rendezvous>();
			rendezvousRepository.findAll().forEach(listRendezvous::add);

			Iterator<Rendezvous> it=listRendezvous.iterator();
			while(it.hasNext())
			{
				Rendezvous patRendezvous=it.next();
				//Rendezvous monendezvous = patRendezvous;
				listereturnlistRendezvous.add(patRendezvous);
			}

			return listereturnlistRendezvous;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	// listeRendezvousAgentCredit
	public List<Rendezvous> listeRendezvousAgence(String codeagence) {
		try {
			List<Rendezvous> mesRendezvous = rendezvousRepository.mesrendezparAgence(codeagence);
			if(mesRendezvous==null)
				return null;
			System.out.println("liste   Rendezvous  service...");
			List<Rendezvous> listRendezvous= new ArrayList<Rendezvous>();
			List<Rendezvous> listereturnlistRendezvous=new ArrayList<Rendezvous>();
			rendezvousRepository.mesrendezparAgence(codeagence).forEach(listRendezvous::add);
			Iterator<Rendezvous> it=listRendezvous.iterator();
				while(it.hasNext())
				{
					Rendezvous patRendezvous=it.next();
					//Rendezvous monendezvous = patRendezvous;
					listereturnlistRendezvous.add(patRendezvous);
				}

				return listereturnlistRendezvous;
			} catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
	@Override

public List<Rendezvous> listeRendezvousAgentCreditindividuel(String matricule) {
		try {
			List<Rendezvous> mesRendezvous = rendezvousRepository.mesrendezparAgentCreditindividuel(matricule);
			if(mesRendezvous==null)
				return null;
			System.out.println("liste   Rendezvous  service...");
			List<Rendezvous> listRendezvous= new ArrayList<Rendezvous>();
			List<Rendezvous> listereturnlistRendezvous=new ArrayList<Rendezvous>();
			mesRendezvous.forEach(listRendezvous::add);
			Iterator<Rendezvous> it=listRendezvous.iterator();
				while(it.hasNext())
				{
					Rendezvous patRendezvous=it.next();
					//Rendezvous monendezvous = patRendezvous;
					listereturnlistRendezvous.add(patRendezvous);
				}

				return listereturnlistRendezvous;
			} catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}

	// listeRendezvousAgentCredit individuel
		public List<Rendezvous> listeRendezvousAgentCreditgroupement(String matricule) {
			try {
				List<Rendezvous> mesRendezvous = rendezvousRepository.mesrendezparAgentCreditgroupement(matricule);
				if(mesRendezvous==null)
					return null;
				System.out.println("liste   Rendezvous  service...");
				List<Rendezvous> listRendezvous= new ArrayList<Rendezvous>();
				List<Rendezvous> listereturnlistRendezvous=new ArrayList<Rendezvous>();
						
				mesRendezvous.forEach(listRendezvous::add);
				Iterator<Rendezvous> it=listRendezvous.iterator();
					while(it.hasNext())
					{
						Rendezvous patRendezvous=it.next();
						//Rendezvous monendezvous = patRendezvous;
						listereturnlistRendezvous.add(patRendezvous);
					}

					return listereturnlistRendezvous;
				} catch(Exception e)
				{
					e.printStackTrace();
					return null;
				}
			}

	@Override
	public List<Rendezvous> listeRendezvousparPlanification(int idplanification) {
		try { 
			List<Rendezvous> mesRendezvous = rendezvousRepository.mesrendezparplanification(idplanification);
			if(mesRendezvous==null)
				return null;
			//List<Rendezvous> listereturnlistRendezvous=new ArrayList<Rendezvous>();
			//Iterator<Rendezvous> it=mesRendezvous.iterator();
			return mesRendezvous;

		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public List<Rendezvous> listeRendezvousdate(String daterendezvous , String codeagence) {
		try {

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			//String cunvertCurrentDate="06/09/2015";
			Date date1 = new Date();
			date1 = df.parse(daterendezvous);
			System.out.println(date1);
			List<Rendezvous> mesRendezvous = rendezvousRepository.mesrendezpardate(codeagence, date1);
			if(mesRendezvous==null)
				return null;
			//List<Rendezvous> listereturnlistRendezvous=new ArrayList<Rendezvous>();
			//Iterator<Rendezvous> it=mesRendezvous.iterator();

			return mesRendezvous;
		} catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}

