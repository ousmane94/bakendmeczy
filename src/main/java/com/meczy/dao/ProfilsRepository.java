package com.meczy.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.meczy.entites.Profils;
import com.meczy.entites.Utilisateur;

	
	
	@CrossOrigin("*") 
	@RepositoryRestResource
	public interface ProfilsRepository extends CrudRepository<Profils, Integer> {
		
		public Profils findByIdprofils(Integer idprofils);

		public Profils findByNomprofils(String nomprofils);


}
