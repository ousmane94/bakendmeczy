package com.meczy.dao;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.meczy.entites.Departement;
import com.meczy.entites.Region;

@CrossOrigin("*") 
@RepositoryRestResource
public interface DepartementRepository extends CrudRepository<Departement, Integer> {
	
	public  Departement findByNomdepartement(String nomdepartement);
	public  Departement findByIddepartement(int iddepartement);
	@Query("SELECT r FROM Departement r ORDER BY r.region.nomregion ASC")
	public  List<Departement> mesdepartementparordre();
	// departement et region
	@Query("SELECT d FROM  Departement d  where d.nomdepartement =:x and d.region.nomregion =:y ")
	public  Departement findDepatementRegion(@Param("x")String x, @Param("y")String y);
	public List<Departement> findByRegion(Region region);
	
	@Query("SELECT r FROM Departement r  where r.etat ='1' ORDER BY r.region.nomregion ASC")
	public  List<Departement> mesdepartementparordreActive();
		


}
