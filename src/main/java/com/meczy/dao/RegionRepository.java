package com.meczy.dao;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.meczy.entites.Pays;
import com.meczy.entites.Region;


@CrossOrigin("*") 
@RepositoryRestResource
public interface RegionRepository extends CrudRepository<Region, Integer> {
	
	public  Region findByNomregion(String nomregion);
	public  Region findByIdregion(int idregion);
	
	@Query("SELECT r FROM Region r ORDER BY r.pays.nompays ASC")
	public  List<Region> mesregionparordre();
	public List<Region> findByPays(Pays pays);
	@Query("select r from Region r where  r.etat='1'")
	public  List<Region> findlisteActive();





}
