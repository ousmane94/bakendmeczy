package com.meczy.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.meczy.entites.Planification;
import com.meczy.entites.Rendezvous;

@CrossOrigin("*") 
@RepositoryRestResource
public interface RendezvousRepository extends CrudRepository<Rendezvous, Integer>{

	List<Rendezvous> findByPlanification(Planification supplanification);
	public  Rendezvous findByNumeromembreAndPlanification(String numeromembre, Planification planification);
	Rendezvous findByNumerorendezvous(long numerorendezvous);
	Rendezvous findByIdrendezvous(int idrendezvous);
	@Query("select r from Rendezvous r where r.planification.agence.codeagence =:x  and r.etat='1' ")
	public  List<Rendezvous>  mesrendezparAgence(@Param("x") String x);
	@Query("select r from Rendezvous r where r.utilisateur.matricule =:x  and r.etat='1' and r.naturedossier='0' or r.etat='2'  or r.etat='3'  ")
	public  List<Rendezvous>  mesrendezparAgentCreditindividuel(@Param("x") String x);
	
	//@Query("select r from Rendezvous r where r.naturedossier='2' and r.utilisateur.matricule =:x   and r.etat='1'   or r.etat='2'  or r.etat='3'  ")
	//public  List<Rendezvous>  mesrendezparAgentCreditgroupement(@Param("x") String x);
	
	@Query("select r from Rendezvous r where r.utilisateur.matricule =:x  and r.naturedossier='2' and  r.etat >='1'   ")
	public  List<Rendezvous>  mesrendezparAgentCreditgroupement(@Param("x") String x);
	
	
	@Query("select r from Rendezvous r where r.planification.idplanification =:x")
	public  List<Rendezvous>  mesrendezparplanification(@Param("x") int x);

	@Query("select r from Rendezvous r where r.planification.agence.codeagence =:x and r.planification.dateplanification =:y ")
	public  List<Rendezvous>  mesrendezpardate(@Param("x") String x,@Param("y") Date y);
	



}
