package com.meczy.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.meczy.entites.Agence;
import com.meczy.entites.Commune;
import com.meczy.entites.Quartier;


@CrossOrigin("*") 
@RepositoryRestResource
public interface QuartierRepository extends CrudRepository<Quartier, Integer> {
	
	public  Quartier findByNomquartier(String nomquartier);
	public  Quartier findByIdquartier(int idquartier);
	@Query("SELECT r FROM Quartier r ORDER BY r.commune.nomcommune ASC")
	public  List<Quartier> mesQuartierparordre();
	// departement et commune
	@Query("SELECT d FROM  Quartier d  where d.nomquartier =:x and d.commune.nomcommune =:y ")
	public  Quartier findCommuneQuartier(@Param("x")String x, @Param("y")String y);
	public  List<Quartier> findByCommune(Commune y);
	
	@Query("SELECT r FROM Quartier r where r.etat='1' ORDER BY r.commune.nomcommune ASC")
	public  List<Quartier> mesQuartierparordreActive();

		

}
