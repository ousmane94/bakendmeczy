package com.meczy.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.meczy.entites.Secteurcredit;

public interface SecteurcreditRepository extends CrudRepository<Secteurcredit, Integer>{

	Secteurcredit findByIdsecteurcredit(int idsecteurcredit);

	Secteurcredit findByDescriptionsecteur(String descriptionsecteur);
	@Query("select c from Secteurcredit c where  c.etat='1'")
	List<Secteurcredit> getSecteurcreditActive();

}
