package com.meczy.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.meczy.entites.Activitecredit;

public interface ActivitecreditRepository extends CrudRepository<Activitecredit, Integer>{

	Activitecredit findByIdactivitecredit(int idactivitecredit);

	Activitecredit findByDescriptionactivitecredit(String descriptionactivitecredit);
	@Query("select c from Activitecredit c where  c.etat='1'")
	List<Activitecredit> getActivitecreditActive();

}
