package com.meczy.dao;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.meczy.entites.Agence;
import com.meczy.entites.Dossierindividuel;
import com.meczy.entites.Utilisateur;

@CrossOrigin("*") 
@RepositoryRestResource
public interface DossierIndivRepository extends CrudRepository<Dossierindividuel,Integer> {

	public Dossierindividuel findByNumerodemande(long numerodemande);
	public Dossierindividuel findByNumerodemandeAndAgence(long numerodemande,Agence agence);
	public  List<Dossierindividuel> findByNumerodemandeAndUtilisateur(long numerodemande,Utilisateur utilisateur);
	public List<Dossierindividuel> findByUtilisateur(Utilisateur utilisateur);

	public Dossierindividuel findByIddossier(int iddossier);
	
    @Query("select d from Dossierindividuel d where d.etatdossier <='3' and d.utilisateur.matricule =:x")
	public  List<Dossierindividuel>  finddossierAgentcredit(@Param("x") String x);




	

	
	
}
