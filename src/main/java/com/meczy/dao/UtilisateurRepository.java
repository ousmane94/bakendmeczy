package com.meczy.dao;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.meczy.entites.Agence;
import com.meczy.entites.Utilisateur;


@CrossOrigin("*") 
@RepositoryRestResource
public interface UtilisateurRepository extends CrudRepository<Utilisateur, Integer> {
//@Query("SELECT u FROM Utilisateur u where u.etat='1' and u.matricule =:x")
public Utilisateur findByMatricule(String x);

public Utilisateur findByIdutilisateur(int idutilisateur);
public List<Utilisateur> findByAgence(Agence agence);
@Query("SELECT r FROM Utilisateur r ORDER BY r.nom ASC")
public  List<Utilisateur> mesUtilisateurparordre();
public Utilisateur findByMatriculeAndPassword(String matricule, String password);
@Query("SELECT u FROM Utilisateur u where u.agence.codeagence =:x and u.profils.idprofils='1' and u.etat='1' ORDER BY u.nom ASC")
public  List<Utilisateur> listetilisateurAgentcreditAgence(@Param("x") String x);
@Query("SELECT u FROM Utilisateur u where u.agence.codeagence =:x and u.etat='1' and u.profils.idprofils='2' ORDER BY u.nom ASC")
public  List<Utilisateur> listetilisateurSuperviseurAgence(@Param("x") String x);
//@Query("select r from Rendezvous r where r.planification.agence.codeagence =:x and r.planification.dateplanification =:y ")
//public  List<Rendezvous>  mesrendezpardate(@Param("x") String x,@Param("y") Date y);

@Query("SELECT u FROM Utilisateur u where u.etat ='1' and u.profils.idprofils='1' or u.profils.idprofils='4' ORDER BY u.nom ASC")
public  List<Utilisateur> listetilisateurAgentcredit();


	
	

}