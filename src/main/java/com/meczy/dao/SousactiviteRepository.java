package com.meczy.dao;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.meczy.entites.Sousactivite;

public interface SousactiviteRepository extends CrudRepository<Sousactivite, Integer>{

	Sousactivite findByIdsousactivitecredit(int idactivitecredit);
	Sousactivite findByDescriptionactivitecredit(String descriptionactivitecredit);
	@Query("select c from Sousactivite c where  c.etat='1'")
	List<Sousactivite> getSousactiviteActive();
	
  // @Query("select r from Sousactivite r where r.etat='1' and r.activitecredit.secteurcredit   join secteurcredit_objectcredits ue on ue.secteurcredit_idsecteurcredit = e.idsecteurcredit  where ue.objectcredits_idobjectcredit=:x)  ")
	
	//@Query("SELECT  `secteurcredit_idsecteurcredit` FROM `@` WHERE `objectcredits_idobjectcredit`='37'  ")
   
  // SELECT `secteurcredit_idsecteurcredit` FROM `secteurcredit_objectcredits` WHERE `objectcredits_idobjectcredit`='37'
   
	/*
	 * select e from Event e join user_event ue on ue.event_id = e.id where
	 * ue.user_id = :userId and e.startDate > CURRENT_TIMESTAMP"
	 */	
   

  // public  List<Sousactivite>  listesousactiviteparObjet(@Param("x") int x);

}
