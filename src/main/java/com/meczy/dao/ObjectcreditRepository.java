package com.meczy.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.meczy.entites.Objectcredit;

public interface ObjectcreditRepository extends CrudRepository<Objectcredit, Integer>{

	Objectcredit findByIdobjectcredit(int idobjectcredit);

	Objectcredit findByDescription(String description);
	@Query("select c from Objectcredit c where  c.etat='1'")
	List<Objectcredit> getObjectcreditActive();

}
