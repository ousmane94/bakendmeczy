package com.meczy.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.meczy.entites.Agence;
import com.meczy.entites.Comite;
import com.meczy.entites.Quartier;
import com.meczy.entites.Region;



@CrossOrigin("*") 
@RepositoryRestResource
public interface AgenceRepository extends CrudRepository<Agence, Integer> {
	
	public  List<Agence> findByQuartier(Quartier y);
	public Agence findByCodeagence(String codeagence);
	public Agence findByIdagence(int idagence);
	
	@Query("SELECT r FROM Agence r where r.etat='1'  ORDER BY r.quartier.commune.departement.region.pays.nompays ASC")
	public  List<Agence> mesAgenceparordreActive();
	@Query("SELECT r FROM Agence r ORDER BY r.quartier.commune.departement.region.pays.nompays ASC")
	public  List<Agence> mesAgenceparordre();
	public List<Agence> findByComite(Comite comite);


}
