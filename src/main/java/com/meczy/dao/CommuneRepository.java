package com.meczy.dao;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.meczy.entites.Commune;
import com.meczy.entites.Departement;


@CrossOrigin("*") 
@RepositoryRestResource
public interface CommuneRepository extends CrudRepository<Commune, Integer> {
	
	public  Commune findByNomcommune(String nomcommune);
	public  Commune findByIdcommune(int iddepartement);
	@Query("SELECT r FROM Commune r ORDER BY r.departement.nomdepartement ASC")
	public  List<Commune> mesCommuneparordre();
	// departement et commune
	@Query("SELECT d FROM  Commune d  where d.nomcommune =:x and d.departement.nomdepartement =:y ")
	public  Commune findCommuneDepatement(@Param("x")String x, @Param("y")String y);
	public List<Commune> findByDepartement(Departement departement);
	@Query("SELECT r FROM Commune r where r.etat='1'  ORDER BY r.nomcommune ASC")
	public  List<Commune> mesCommuneparordreActive();
		


}
