package com.meczy.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.meczy.entites.Comite;

@CrossOrigin("*") 
@RepositoryRestResource
public interface ComiteRepository  extends CrudRepository<Comite, Integer>{

	Comite findByIdcomite(int idcomite);

	Comite findByNomcomite(String nomcomite);
	@Query("select c from Comite c where  c.etat='1'")
	List<Comite> getcomiteActive();

}
