package com.meczy.dao;

import java.util.Date;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.meczy.entites.Agence;
import com.meczy.entites.Planification;

@CrossOrigin("*") 
@RepositoryRestResource
public interface PlanificateurRepository extends CrudRepository<Planification , Integer> {

	List<Planification> findByAgence(Agence agence);
	public Planification  findByDateplanificationAndAgence(Date dateplanification, Agence agence );
	@Query("SELECT p FROM  Planification p  ORDER BY p.agence.codeagence ASC ")
	public List<Planification>   findByDateplanification(Date dateplanification);
	public Planification  findByIdplanification(int idplanification );
	@Query("select r from Planification r where  r.etat='1'")
	public List<Planification> listPlanificationsActive();
	
	@Query("SELECT p FROM  Planification p  where p.agence.codeagence =:y and p.etat='1' ORDER BY p.dateplanification ASC  ")
	public  List<Planification> listPlanificationsActiveAgence(@Param("y")String y);
	
	




	

}
