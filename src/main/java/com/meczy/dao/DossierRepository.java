package com.meczy.dao;

import org.springframework.data.repository.CrudRepository;

import com.meczy.entites.Dossier;

public interface DossierRepository extends CrudRepository<Dossier, Integer>{

}
