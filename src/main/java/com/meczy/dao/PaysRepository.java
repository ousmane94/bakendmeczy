package com.meczy.dao;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.meczy.entites.*;
@CrossOrigin("*") 
@RepositoryRestResource
public interface PaysRepository extends CrudRepository<Pays, Integer>{
	public  Pays findByNompays(String nompays);
	public  Pays findByIdpays(int idpays);
	@Query("select p from Pays p where  p.etat='1'")
	public  List<Pays> findlisteActive();


}
